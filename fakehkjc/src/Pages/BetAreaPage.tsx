import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { oddTypeMap } from "../redux/odd/reducer";
import { RootState } from "../store";
import { Button } from 'reactstrap';
import { deleteDraftBet, selectAllDraftBets } from "../redux/cart/action";
import { fetchMatches } from "../redux/match/action";
import { push } from "connected-react-router";
// import { Route, Switch } from "react-router";
// import { stat } from "fs";
import { DraftBet } from "../redux/cart/reducer";
// import { BetRecord } from "../Components/ProfileContent";
import "./BetAreaPage.css"
import { Card, CardBody } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import { Divider } from 'antd';
// import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';
import ErrorIcon from '@material-ui/icons/Error';

export function BetAreaPage() {
    const matches = useSelector((state: RootState) => state.match.matches)
    const user = useSelector((state: RootState) => state.auth)
    const amount = useSelector((state: RootState) => state.user.coin)
    const dispatch = useDispatch()
    const cart = useSelector((state: RootState) => state.cart.draftBets)
    // const [draft, setDraft] = useState(cart)
    console.log(cart)

    // const [amountMsgTitle, setAmountMsgTitle] = useState("")
    const [amountMsg, setAmountMsg] = useState('')

    const [dateMsgTitle, setDateMsgTitle] = useState("")
    const [dateFailedDraftBet, setDateFailedDraftBet] = useState([] as DraftBet[])

    const [oddsMsgTitle, setOddsMsgTitle] = useState("")
    const [oddsFailedDraftBet, setOddsFailedDraftBet] = useState([] as DraftBet[])

    const [betTypeContentMsgTitle, setBetTypeContentMsgTitle] = useState("")
    const [betTypeContentFailedDraftBet, setBetTypeContentFailedDraftBet] = useState([] as DraftBet[])

    // const [serverMatch, setServerMatch] = useState([] as DraftBet[])

    // const [click, setClick] = useState(true)

    useEffect(() => {
        console.log("1")
        dispatch(fetchMatches())

    }, [dispatch])

    useEffect(() => {

        dispatch(selectAllDraftBets(user.user_id!))
        console.log("3")
    }, [dispatch, user.user_id])

    return (
        <Container className="betArea-con">
            <Row>
                <Col >


                    <div>
                        <Card>
                            <CardBody>
                                <div>

                                    你的戶口：{new Intl.NumberFormat().format(amount!)}金幣    {amountMsg}
                                </div>
                                <Divider>投注區</Divider>


                                {cart.length === 0 && <div>未有暫存注項。</div>}
                                {cart.length >= 1 && cart.filter(bet => bet.id).map(bet => {
                                    const match = matches.find(match => match.jc_match_id === bet.match_id)
                                    return (
                                        <div key={bet.id!} className={"margin-draft-bet"}>
                                            <Col>
                                                <div>
                                                    <div>注項編號：{bet.id!}</div>
                                                    <h5>
                                                        {match?.match_type} {match?.home_team} vs {match?.away_team}
                                                    </h5>
                                                    <div>{bet.bet_type}：{bet.bet_content}</div>
                                                    {bet.bet_type_content &&
                                                        <div>盤口：{bet.bet_type_content}</div>}
                                                    <div>賠率：{bet.odds}</div>
                                                    <div>投注金額：$ {new Intl.NumberFormat().format(bet.spent_coin)}</div>
                                                </div>
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <DeleteOutlineIcon fontSize='large' onClick={() => {
                                                            dispatch(deleteDraftBet(bet.id!, user.user_id!))
                                                        }} />

                                                        {oddsFailedDraftBet.find(draftBet => draftBet.id === bet.id) &&
                                                            <EditIcon fontSize='large'/>}
                                                        {betTypeContentFailedDraftBet.find(draftBet => draftBet.id === bet.id) &&
                                                            <EditIcon fontSize='large'/>}
                                                    </div>



                                                    <p className="mb-0 ">預計贏得金額：<span className="bet-gain"><strong>$ {new Intl.NumberFormat().format(bet.spent_coin * bet.odds)}</strong></span></p>
                                                </div>

                                                {dateFailedDraftBet.find(draftBet => draftBet.id === bet.id) &&
                                                
                                                    <div className={"dateFailed"}>
                                                        <ErrorIcon/> {dateMsgTitle}
                                                    </div>
                                                }

                                                {oddsFailedDraftBet.find(draftBet => draftBet.id === bet.id) &&
                                                    <div className={"oddsFailed"}>
                                                        <ErrorIcon/> {oddsMsgTitle}
                                                    </div>
                                                }

                                                {betTypeContentFailedDraftBet.find(draftBet => draftBet.id ===bet.id) && 
                                                    <div className={"betTypeContentFailed"}>
                                                        <ErrorIcon/> {betTypeContentMsgTitle}
                                                    </div>}
                                                <Divider />
                                            </Col>
                                        </div>

                                    )
                                })}



                                {cart.length > 0 && <Button color="danger" onClick={async () => {
                                    console.log("xxx")


                                    let totalAmount: number = 0;
                                    for (let bet of cart) {
                                        totalAmount += bet.spent_coin
                                    }

                                    if (amount! > totalAmount) {
                                        setAmountMsg("")
                                        let res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/validating-odds/${user.user_id}`, {
                                            headers: {
                                                "Authorization": `Bearer ${localStorage.getItem('token')}`
                                            }
                                        })
                                        let json = await res.json()
                                        console.log(json)

                                        if (json["dateFail"].length > 0 && json["oddFail"].length > 0 && json["betTypeContentFail"].length > 0) {
                                            setDateMsgTitle("比賽已經開始/完結，請刪除注項。")
                                            setDateFailedDraftBet(json.dateFail)
                                            setOddsMsgTitle('賠率有變，請更新賠率，或刪除並重新落注。')
                                            setOddsFailedDraftBet(json.oddFail)
                                            setBetTypeContentMsgTitle("盤口有變，請更新盤口，或刪除並重新落注")
                                            setBetTypeContentFailedDraftBet(json.betTypeContentFail)
                                        } else if (json["dateFail"].length > 0 && json["oddFail"].length > 0) {
                                            setDateMsgTitle("比賽已經開始/完結，請刪除注項。")
                                            setDateFailedDraftBet(json.dateFail)
                                            setOddsMsgTitle('賠率有變，請更新賠率，或刪除並重新落注。')
                                            setOddsFailedDraftBet(json.oddFail)
                                        } else if (json["dateFail"].length > 0 && json["betTypeContentFail"].length > 0) {
                                            setDateMsgTitle("比賽已經開始/完結，請刪除注項。")
                                            setDateFailedDraftBet(json.dateFail)
                                            setBetTypeContentMsgTitle("盤口有變，請更新盤口，或刪除並重新落注")
                                            setBetTypeContentFailedDraftBet(json.betTypeContentFail)
                                        } else if (json['oddFail'].length > 0 && json["betTypeContentFail"].length > 0) {
                                            setOddsMsgTitle('賠率有變，請更新賠率，或刪除並重新落注。')
                                            setOddsFailedDraftBet(json.oddFail)
                                            setBetTypeContentMsgTitle("盤口有變，請更新盤口，或刪除並重新落注")
                                            setBetTypeContentFailedDraftBet(json.betTypeContentFail)
                                        } else if (json["oddFail"].length > 0) {
                                            setOddsMsgTitle('賠率有變，請更新賠率，或刪除並重新落注。')
                                            setOddsFailedDraftBet(json.oddFail)
                                        } else if (json["dateFail"].length > 0) {
                                            setDateMsgTitle( "比賽已經開始/完結，請刪除注項。")
                                            setDateFailedDraftBet(json.dateFail)
                                        } else if (json["betTypeContentFail"].length > 0) {
                                            setBetTypeContentMsgTitle("盤口有變，請更新盤口，或刪除並重新落注")
                                            setBetTypeContentFailedDraftBet(json.betTypeContentFail)
                                        } else {
                                            setDateMsgTitle("")
                                            setDateFailedDraftBet([] as DraftBet[])
                                            setOddsMsgTitle("")
                                            setOddsFailedDraftBet([] as DraftBet[])
                                            dispatch(push('/profile/record'))
                                        }
                                    } else if (amount! < totalAmount) {
                                        setAmountMsg("戶口金幣不足。")
                                    }
                                    console.log("yyy")
                                }}>落注</Button>}


                                {/* {dateMsgTitle.length > 0 && dateFailedDraftBet.map(draftBet => {

            })} */}



                                {/* {validationMessage.length > 0 && validationMessage.map(msg => {
                const match = matches.find(match => match.jc_match_id === msg.match_id)
                return (
                    <div key={validationMessage.indexOf(msg)}>
                        {msg.id!}
                        {match?.match_type} {match?.home_team} vs {match?.away_team}
                        <div>
                            {msg.bet_type}: {msg.bet_content}
                        </div>
                        <div>

                        </div>
                        <div>
                            {msg.bet_type_content}
                        </div>
                        <div>
                            {msg.odds}
                        </div>
                        <div>
                            ${msg.spent_coin}
                        </div>
                    </div>
                )
            })} */}

                            </CardBody>
                        </Card>

                    </div>

                </Col>
            </Row>


        </Container>
    )
}