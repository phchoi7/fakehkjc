import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { login, loginFacebook } from "../redux/auth/action"
import { RootState } from "../store"
import {
  Box,
  Button,
  Grommet,
  Form,
  FormField,
  Text,
  TextInput,
} from 'grommet';
// import { grommet } from 'grommet/themes';
import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
// import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

// import Typography from '@material-ui/core/Typography';
import ReactFacebookLogin, { ReactFacebookLoginInfo } from "react-facebook-login";
import { FormGroup } from "reactstrap";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export function LoginPage() {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const dispatch = useDispatch()
  const loginMsg = useSelector((state: RootState) => state.auth.message)
  const [value, setValue] = React.useState({ name: '', email: '' });

  const classes = useStyles();
  // const bull = <span className={classes.bullet}>•</span>;

  // /*rest of the code*/
  const fBOnCLick = () => {
    return null;
  }
  const fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
    if (userInfo.accessToken) {
      console.log("5")
      dispatch(loginFacebook(userInfo.accessToken));
      console.log("6")
    }
    return null;
  }

  return (
    <div>
      <div>立即登入，XXXXXXXXXX</div>
      <Grommet>
        <Box fill align="center" justify="center">
          <Box width="medium">
            <Card className={classes.root} variant="outlined">
              <CardContent>

                <Form
                  value={value}
                  onChange={nextValue => setValue(nextValue as any)}
                  onSubmit={(event) => {
                    event.preventDefault()
                    dispatch(login(email, password))
                  }}>
                  <FormField name="email" required >
                    <TextInput name="email" type="email" placeholder="電郵" value={email} onChange={event => setEmail(event.currentTarget.value)} />
                  </FormField>

                  <FormField name="password" required >
                    <TextInput name="password" type="password" placeholder="密碼" value={password} onChange={event => setPassword(event.currentTarget.value)} />
                  </FormField>

                  {loginMsg && (
                    <Box pad={{ horizontal: 'small' }}>
                      <Text color="status-error">{loginMsg}</Text>
                    </Box>
                  )}

                  <Box direction="row" justify="between" margin={{ top: 'medium' }}>
                    <Link to="/register" >
                      <Button label="註冊" />
                    </Link>
                    <Button className="login-button " type="submit" label="登入" primary />
                  </Box>
                </Form>


              </CardContent>

              <div>或</div>
              <br></br>

              <FormGroup>
                <div className="fb-button">
                  <ReactFacebookLogin
                    appId={process.env.REACT_APP_FACEBOOK_APP_ID || ''}
                    autoLoad={false}
                    fields="name,email,picture"
                    onClick={fBOnCLick}
                    callback={fBCallback}
                  />
                </div>
              </FormGroup>
            </Card>
          </Box>
        </Box>
      </Grommet>
    </div >
  )
}
