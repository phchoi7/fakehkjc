import { push } from "connected-react-router"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { ProfileContainer } from "../Components/ProfileContainer"
import { selectAllDraftBets } from "../redux/cart/action"
import { fetchFolloweeInfo } from "../redux/follow/action"
import { fetchBetRecord } from "../redux/user/action"
import { RootState } from "../store"
import './ProfilePage.css'

export function ProfilePage() {
    const dispatch = useDispatch()
    const info = useSelector((state: RootState) => state.auth)


    useEffect(() => {
        // console.log("here is profile page")
        // console.log(info.id)
        if (info.user_id !== null) {
            dispatch(fetchBetRecord(info.user_id + ''))
            dispatch(fetchFolloweeInfo(info.user_id + ''))
            dispatch(selectAllDraftBets(info.user_id!))
            // console.log("dispatched")
        } else {
            return
        }
    }, [dispatch, info.user_id])

    return (
        <div>
            <ProfileContainer user_id={info.user_id!}/>
        </div>
    )
}

export function ProfileFailPage() {
    const dispatch = useDispatch()

    return (
        <div>
            請先登入帳戶，如沒有帳戶，可<span className={"redirect"} onClick={() => dispatch(push('/login'))}>按此</span>申請。
        </div>
    )
}