import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
// import { login } from "../redux/auth/action"
import { RootState } from "../store"
import {
  Box,
  Button,
  Grommet,
  Form,
  FormField,
  Text,
  TextInput,
} from 'grommet';
// import { grommet } from 'grommet/themes';
import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
// import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import Typography from '@material-ui/core/Typography';
import { registerAc } from "../redux/user/action";


const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

// var actionCodeSettings = {
//   url: 'https://www.example.com/finishSignUp?cartId=1234',
//   handleCodeInApp: true,
//   iOS: {
//     bundleId: 'com.example.ios'
//   },
//   android: {
//     packageName: 'com.example.android',
//     installApp: true,
//     minimumVersion: '12'
//   },
//   dynamicLinkDomain: 'example.page.link'
// };

export function RegisterPage() {
  const [email, setEmail] = useState("")
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [passwordMessage, setPasswordMessage] = useState("")


  const dispatch = useDispatch()
  const registerMsg = useSelector((state: RootState) => state.user.message)

  const [value, setValue] = React.useState({ name: '', email: '' });
  const classes = useStyles();
  // const bull = <span className={classes.bullet}>•</span>;

  return (
    <div>
      <div>註冊</div>
      <Grommet>
        <Box fill align="center" justify="center">
          <Box width="medium">
            <Card className={classes.root} variant="outlined">
              <CardContent>
                <Form
                  value={value}
                  onChange={nextValue => setValue(nextValue as any)}

                  onSubmit={(event) => {
                    event.preventDefault()
                    if (confirmPassword !== password) {
                      setPasswordMessage("兩個密碼不一致。")
                    } else {
                      setPasswordMessage("")
                      dispatch(registerAc(email, username, password))
                    }
                  }}>
                  <FormField name="email" required >
                    <TextInput name="email" type="email" placeholder="電郵" value={email} onChange={event => setEmail(event.currentTarget.value)} />
                  </FormField>

                  <FormField name="username" required >
                    <TextInput name="username" type="text" placeholder="名稱" value={username} onChange={event => setUsername(event.currentTarget.value)} />
                  </FormField>

                  <FormField name="password" required >
                    <TextInput name="password" type="password" placeholder="密碼" value={password} onChange={event => setPassword(event.currentTarget.value)} />
                  </FormField>

                  <FormField name="confirm-password" required >
                    <TextInput name="confirm-password" type="password" placeholder="再次輸入密碼" value={confirmPassword} onChange={(event) => {
                      setConfirmPassword(event.currentTarget.value);
                    }} />
                  </FormField>

                  {passwordMessage && (
                    <Box pad={{ horizontal: 'small' }}>
                      <Text color="status-error">{passwordMessage}</Text>
                    </Box>)}

                  {registerMsg && !passwordMessage && (
                    <Box pad={{ horizontal: 'small' }}>
                      <Text color="status-error">{registerMsg}</Text>
                    </Box>)}

                  <Box direction="row" justify="between" margin={{ top: 'medium' }}>
                    <Link to="/login" >
                      <Button label="前往登入" />
                    </Link>
                    <Button type="submit" label="註冊" primary
                      disabled={!(/^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,})$/.test(email))} />
                  </Box>
                </Form>
              </CardContent>
            </Card>
          </Box>
        </Box>
      </Grommet>
      {/* <div id="firebaseui-auth-container"></div> */}
      {/* <div>登入</div>
            <div>{loginMsg}</div>
            <form onSubmit={(event) => {
                event.preventDefault()
                dispatch(login(email, password))
            }}>
                <input type="email" placeholder="電郵" value={email} onChange={event => setEmail(event.currentTarget.value)}></input>

                <input type="password" placeholder="密碼" value={password} onChange={event => setPassword(event.currentTarget.value)}></input>
                <input type="submit" />
            </form> */}
    </div >
  )
}


// import React, { Component } from "react";
// import Form from "react-bootstrap/Form";
// import Container from "react-bootstrap/Container";
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";
// import PrimaryButton from "./shared/button";
// import firebase from "../firebase";
// import GoogleLogin from "./google-login";

// export default class PhoneLogin extends Component {
// constructor() {
//   super();
//   this.state = {
//     form: true,
//     alert: false,
//   };
// }

// onChangeHandler = (event) => {
//   const { name, value } = event.target;
//   this.setState({
//     [name]: value,
//   });
// };

// setUpRecaptcha = () => {
//   window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
//     "recaptcha-container",
//     {
//       size: "invisible",
//       callback: function (response) {
//         console.log("Captcha Resolved");
//         this.onSignInSubmit();
//       },
//       defaultCountry: "IN",
//     }
//   );
// };

// onSignInSubmit = (e) => {
//   e.preventDefault();
//   this.setUpRecaptcha();
//   let phoneNumber = "+91" + this.state.mobile;
//   console.log(phoneNumber);
//   let appVerifier = window.recaptchaVerifier;
//   firebase
//     .auth()
//     .signInWithPhoneNumber(phoneNumber, appVerifier)
//     .then(function (confirmationResult) {
//       // SMS sent. Prompt user to type the code from the message, then sign the
//       // user in with confirmationResult.confirm(code).
//       window.confirmationResult = confirmationResult;
//       // console.log(confirmationResult);
//       console.log("OTP is sent");
//     })
//     .catch(function (error) {
//       console.log(error);
//     });
// };

// onSubmitOtp = (e) => {
//   e.preventDefault();
//   let otpInput = this.state.otp;
//   let optConfirm = window.confirmationResult;
//   // console.log(codee);
//   optConfirm
//     .confirm(otpInput)
//     .then(function (result) {
//       // User signed in successfully.
//       // console.log("Result" + result.verificationID);
//       let user = result.user;
//     })
//     .catch(function (error) {
//       console.log(error);
//       alert("Incorrect OTP");
//     });
// };

// render() {
//   return (
//     <div>
//       <Container fluid="sm" className="mt-3">
//         <Row className="justify-content-center">
//           <Col xs={12} md={6} lg={5}>
//             <h2 className="mb-3">Login</h2>
//             <Form className="form" onSubmit={this.onSignInSubmit}>
//               <div id="recaptcha-container"></div>
//               <Form.Group>
//                 <Form.Control
//                   type="number"
//                   name="mobile"
//                   placeholder="Mobile Number"
//                   onChange={this.onChangeHandler}
//                   required
//                 />
//               </Form.Group>
//               <PrimaryButton button="Submit" type="submit" />
//             </Form>
//           </Col>
//         </Row>
//         <Row className="justify-content-center">
//           <Col xs={12} md={6} lg={5}>
//             <h2 className="mb-3">Enter OTP</h2>
//             <Form className="form" onSubmit={this.onSubmitOtp}>
//               <Form.Group>
//                 <Form.Control
//                   id="otp"
//                   type="number"
//                   name="otp"
//                   placeholder="OTP"
//                   onChange={this.onChangeHandler}
//                 />
//               </Form.Group>
//               <PrimaryButton button="Submit" type="submit" />
//             </Form>
//           </Col>
//         </Row>
//         <GoogleLogin />
//       </Container>
//     </div>
//   );
// }
// }