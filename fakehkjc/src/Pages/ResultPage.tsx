// export const matchTypeName = {
//   PremierLeague: "英格蘭超級聯賽",
//   LaLiga: "西班牙甲組聯賽",
//   SerieA: '意大利甲組聯賽',
//   Bundesliga: "德國甲組聯賽",
//   Ligue: "法國甲組聯賽"
// }

import { push } from "connected-react-router";
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Switch, Route } from "react-router-dom";
import { ResultPanel } from "../Components/ResultPanel";

import { RootState } from "../store";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { fetchMatchesResult } from "../redux/match-result/action";

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        padding: '10px',
    },
});

export function Results() {
    const matches = useSelector((state: RootState) => state.matchResult.matchesResult)
    const dispatch = useDispatch();
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    useEffect(() => {
        dispatch(fetchMatchesResult())
        // eslint-disable-next-line
    }, [])



    return (
        <div>
            <div>賽果</div>
            <Paper className={classes.root}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    scrollButtons="auto"
                    centered
                >
                    <Tab label="所有" onClick={() => {
                        dispatch(push('/match-result/'))
                    }} />
                    <Tab label="英超" onClick={() => {

                        dispatch(push(`/match-result/premierLeague`))
                    }} />
                    <Tab label="西甲" onClick={() => {

                        dispatch(push('/match-result/laLiga'))
                    }} />
                    <Tab label="意甲" onClick={() => {

                        dispatch(push('/match-result/serieA'))
                    }} />
                    <Tab label="德甲" onClick={() => {

                        dispatch(push('/match-result/bundesliga'))
                    }} />
                    <Tab label="法甲" onClick={() => {

                        dispatch(push('/match-result/ligue'))
                    }} />
                </Tabs>
                <Switch>
                    <Route path="/match-result/" exact>
                        <ResultPanel matches={matches} type={""} />
                    </Route>
                    <Route path="/match-result/premierLeague">
                        <ResultPanel matches={matches} type={"英格蘭超級聯賽"} />
                    </Route>
                    <Route path="/match-result/laLiga">
                        <ResultPanel matches={matches} type={"西班牙甲組聯賽"} />
                    </Route>
                    <Route path="/match-result/serieA">
                        <ResultPanel matches={matches} type={"意大利甲組聯賽"} />
                    </Route>
                    <Route path="/match-result/bundesliga">
                        <ResultPanel matches={matches} type={"德國甲組聯賽"} />
                    </Route>
                    <Route path="/match-result/ligue">
                        <ResultPanel matches={matches} type={"法國甲組聯賽"} />
                    </Route>
                </Switch>
            </Paper>

        </div>
    )
}



// export function Results() {
//     const matches = useSelector((state: RootState) => state.match.matches)
//     //const [display, setDisplay] = useState(matches)
// 
//     const dispatch = useDispatch()
// 
//     useEffect(() => {
//         dispatch(fetchMatches())
//         // eslint-disable-next-line
//     }, [])
//     
//     //useEffect(() => {
//     //    setDisplay(matches)
//     //    // eslint-disable-next-line
//     //}, [matches])
// 
//     return (
//         <div>
//             <div>賽果</div>
//             <button onClick={() => {
//                 dispatch(push('/match-result/'))
//             }}>所有</button>
//             <button onClick={() => {
//                 dispatch(push(`/match-result/premier-league`))
//             }}>英超</button>
//             <button onClick={() => {
//                 dispatch(push('/match-result/la-liga'))
//             }}>西甲</button>
//             <button onClick={() => {
//                 dispatch(push('/match-result/serie-a'))
//             }}>意甲</button>
//             <button onClick={() => {
//                 dispatch(push('/match-result/bundesliga'))
//             }}>德甲</button>
//             <button onClick={() => {
//                 dispatch(push('/match-result/ligue'))
//             }}>法甲</button>
//             <div>
//                 <Switch>
//                     <Route path="/match-result/" exact>
//                         <ResultPanel match={matches} type={""} />
//                     </Route>
//                     <Route path="/match-result/premier-league">
//                         <ResultPanel match={matches} type={"英格蘭超級聯賽"} />
//                     </Route>
//                     <Route path="/match-result/la-liga">
//                         <ResultPanel match={matches} type={"西班牙甲組聯賽"} />
//                     </Route>
//                     <Route path="/match-result/serie-a">
//                         <ResultPanel match={matches} type={"意大利甲組聯賽"} />
//                     </Route>
//                     <Route path="/match-result/bundesliga">
//                         <ResultPanel match={matches}type={"德國甲組聯賽"} />
//                     </Route>
//                     <Route path="/match-result/ligue">
//                         <ResultPanel match={matches} type={"法國甲組聯賽"} />
//                     </Route>
//                 </Switch>
//             </div>
//         </div>
//     )
// }




// export function Results() {
//    const matches = useSelector((state: RootState) => state.match.matches)
//    const [display, setDisplay] = useState(matches)
//    const dispatch = useDispatch();

//     useEffect(() => {
//         dispatch(fetchMatches())
//         // eslint-disable-next-line
//     }, [])

//    useEffect(() => {
//        setDisplay(matches)
//        // eslint-disable-next-line
//    }, [matches])

//    return (
//        <div>
//            <div>賽果</div>
//            <button onClick={() => {
//                setDisplay(matches)
//                dispatch(push('/match-result/'))
//            }}>所有</button>
//            <button onClick={() => {

//                dispatch(push(`/match-result/premier-league`))
//            }}>英超</button>
//            <button onClick={() => {
//                let leagues = matches.filter(match => match.match_type === "西班牙甲組聯賽");
//                setDisplay(leagues);
//                dispatch(push('/match-result/la-liga'))
//            }}>西甲</button>
//            <button onClick={() => {
//                let leagues = matches.filter(match => match.match_type === "意大利甲組聯賽");
//                setDisplay(leagues)
//                dispatch(push('/match-result/serie-a'))
//            }}>意甲</button>
//            <button onClick={() => {
//                let leagues = matches.filter(match => match.match_type === "德國甲組聯賽");
//                setDisplay(leagues)
//                dispatch(push('/match-result/bundesliga'))
//            }}>德甲</button>
//            <button onClick={() => {
//                let leagues = matches.filter(match => match.match_type === "法國甲組聯賽");
//                setDisplay(leagues)
//                dispatch(push('/match-result/ligue'))
//            }}>法甲</button>
//            <div>
//                <Switch>
//                    <Route path="/match-result/" exact>
//                        <ResultPanel matches={display} type={""} />
//                    </Route>
//                    <Route path="/match-result/premier-league">
//                        <ResultPanel matches={display} type={"英格蘭超級聯賽"} />
//                    </Route>
//                    <Route path="/match-result/la-liga">
//                        <ResultPanel matches={display} type={"西班牙甲組聯賽"} />
//                    </Route>
//                    <Route path="/match-result/serie-a">
//                        <ResultPanel matches={display} type={"意大利甲組聯賽"} />
//                    </Route>
//                    <Route path="/match-result/bundesliga">
//                        <ResultPanel matches={display} type={"德國甲組聯賽"} />
//                    </Route>
//                    <Route path="/match-result/ligue">
//                        <ResultPanel matches={display} type={"法國甲組聯賽"} />
//                    </Route>
//                </Switch>
//            </div>
//        </div>
//    )
// }