import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { MatchRow } from "../Components/MatchRow";
import React, { useEffect, useState } from "react";
import { fetchMatches } from "../redux/match/action";
// import { push } from "connected-react-router";
// import { Route, Switch } from "react-router-dom";
// import { OddPanel } from "../Components/OddPanel";
// import { ResultPanel } from "../Components/ResultPanel";
// import { grommet, Box, Button, Grommet } from 'grommet';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import moment from "moment";


const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    padding: '10px',
  },
});




export function BetPage() {
  const matches = useSelector((state: RootState) => state.match.matches)
  const dispatch = useDispatch()

  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };


  const [showMatches, setShowMatches] = useState("")


  // console.log(matches)
  useEffect(() => {

    dispatch(fetchMatches())
    // eslint-disable-next-line
  }, [])

  // const showOddPanel = function () {

  // }

  return (
    <div>

      <div>受注賽事</div>
      <Paper className={classes.root}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
          variant="scrollable"
          scrollButtons="auto"
        >
          <Tab label="所有" onClick={() => {
            setShowMatches("")
          }} />
          <Tab label="英超" onClick={() => {
            setShowMatches("英超")
          }} />
          <Tab label="西甲" onClick={() => {
            setShowMatches("西甲")
          }} />
          <Tab label="意甲" onClick={() => {
            setShowMatches("意甲")
          }} />
          <Tab label="德甲" onClick={() => {
            setShowMatches("德甲")
          }} />
          <Tab label="法甲" onClick={() => {
            setShowMatches("法甲")
          }} />
        </Tabs>

        {showMatches === "" && matches.map(eachMatch => {

          // 呢段comment code是正確的!!!!!!!!!!!!!!! 但要delete === Defined!
          // if (eachMatch.matchStatus === "Defined" && moment(eachMatch.match_date).format("YYYY-MM-DD HH:mm") > moment(new Date()).format("YYYY-MM-DD HH:mm")) {
          //   return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
          // } else {
          //   return <></>
          // }

          return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />

        })}

        {showMatches === "英超" && matches.map(eachMatch => {
          if (eachMatch.match_type === "英格蘭超級聯賽" && moment(eachMatch.match_date).format("YYYY-MM-DD HH:mm") > moment(new Date()).format("YYYY-MM-DD HH:mm")) {
            return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
          } else {
            return <></>
          }
        })}

        {showMatches === "西甲" && matches.map(eachMatch => {
          if (eachMatch.match_type === "西班牙甲組聯賽" && moment(eachMatch.match_date).format("YYYY-MM-DD HH:mm") > moment(new Date()).format("YYYY-MM-DD HH:mm")) {
            return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
          } else {
            return <></>
          }
        })}

        {showMatches === "意甲" && matches.map(eachMatch => {
          if (eachMatch.match_type === "意大利國甲組聯賽" && moment(eachMatch.match_date).format("YYYY-MM-DD HH:mm") > moment(new Date()).format("YYYY-MM-DD HH:mm")) {
            return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
          } else {
            return <></>
          }
        })}

        {showMatches === "德甲" && matches.map(eachMatch => {
          if (eachMatch.match_type === "德國甲組聯賽" && moment(eachMatch.match_date).format("YYYY-MM-DD HH:mm") > moment(new Date()).format("YYYY-MM-DD HH:mm")) {
            return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
          } else {
            return <></>
          }
        })}

        {showMatches === "法甲" && matches.map(eachMatch => {
          if (eachMatch.match_type === "法國甲組聯賽" && moment(eachMatch.match_date).format("YYYY-MM-DD HH:mm") > moment(new Date()).format("YYYY-MM-DD HH:mm")) {
            return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
          } else {
            return <></>
          }
        })}
      </Paper>
      {/* {matches.map(eachMatch => {
        if (eachMatch.matchStatus === "Defined") {
          return <MatchRow match={eachMatch} key={matches.indexOf(eachMatch)} />
        }
      })} */}


    </div>
  );
}

