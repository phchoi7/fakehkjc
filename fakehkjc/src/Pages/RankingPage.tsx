import React, { useEffect, useState } from "react";
import { Card, CardBody, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap"
import { Button } from 'reactstrap';
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { fetchFolloweeInfo, insertFollowee, deleteFollowee } from "../redux/follow/action";
import { Followee } from "../redux/follow/reducer";
import "./RankingPage.css";
import { Divider } from 'antd';
import { Container, Row, Col } from "reactstrap";

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        padding: '10px',
    },
});
export function RankingPage() {
    const userId = useSelector((state: RootState) => state.auth.user_id)
    const followee = useSelector((state: RootState) => state.followee.followee)
    const [ranking, setRanking] = useState<Followee[]>([])
    const classes = useStyles();
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };
    const [modal, setModal] = useState(false);
    const [value, setValue] = React.useState(0);
    const [showRanking, setShowRanking] = useState("Top 1 - 10")

    const toggle = () => setModal(!modal);
    const dispatch = useDispatch()
    // console.log(matches)

    function calculateWinPercentage (no_of_win_game: number | null, no_of_game: number | null) {
        if (no_of_win_game == null || no_of_game == null) {
            return 0
        } else {
            return Math.floor((no_of_win_game/no_of_game) * 100)
        }
    }
    useEffect(() => {
        console.log("ranking page")
        async function loadRanking() {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/ranking`)
            const json = await res.json()
            console.log(json)
            setRanking(json)
        }
        loadRanking()

    }, [])

    useEffect(() => {
        dispatch(fetchFolloweeInfo((userId + "")))
        // console.log(followee)
    }, [userId])

    return (
        <Container>
            <Row>
            <Col>
        <div>
            <div>高手排名</div>
            <Paper className={classes.root}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    scrollButtons="auto"
                    centered
                >
                    <Tab label="Top 1 - 10" onClick={() => {
                        setShowRanking("Top 1 - 10")
                    }} />
                    <Tab label="Top 11 - 20" onClick={() => {
                        setShowRanking("Top 11 - 20")
                    }} />
                    <Tab label="Top 21 - 30" onClick={() => {
                        setShowRanking("Top 21 - 30")
                    }} />
                    <Tab label="Top 31 - 40" onClick={() => {
                        setShowRanking("Top 31 - 40")
                    }} />
                    <Tab label="Top 41 - 50" onClick={() => {
                        setShowRanking("Top 41 - 50")
                    }} />
                </Tabs>
                <Card>
                            <CardBody>
                            <Col >
                {showRanking === "Top 1 - 10" && ranking.slice(0, 10).map(rank =>
                   
                    <div className="ranking-container">
                        <Divider>高手檔案</Divider>
                                <div className="ranking">
                                    <div className="ranking-user-info">
                                        <div>高手大名：{rank.username}</div>
                                        <div>總投注場數：{rank.no_of_game}</div>
                                        <div>勝出場數：{rank.no_of_win_game}</div>
                                        <div>勝率：{calculateWinPercentage(rank.no_of_win_game,rank.no_of_game)}%</div>
                                        <div>金幣：${rank.coin}</div>
                                    </div>

                                    <div className="follow-button">
                                        {!(followee.find(followee => followee.username === rank.username)) &&
                                            <Button onClick={() => {
                                                if (userId) {
                                                    dispatch(insertFollowee({
                                                        id: userId,
                                                        username: rank.username,
                                                        no_of_game: rank.no_of_game,
                                                        no_of_win_game: rank.no_of_win_game,
                                                        no_of_game_CRS: rank.no_of_game_CRS,
                                                        no_of_win_game_CRS: rank.no_of_win_game_CRS,
                                                        no_of_game_HIL: rank.no_of_game_HIL,
                                                        no_of_win_game_HIL: rank.no_of_win_game_HIL,
                                                        no_of_game_COR: rank.no_of_game_COR,
                                                        no_of_win_game_COR: rank.no_of_win_game_COR,
                                                        no_of_game_HDC: rank.no_of_game_HDC,
                                                        no_of_win_game_HDC: rank.no_of_win_game_HDC,
                                                        no_of_game_HAD: rank.no_of_game_HAD,
                                                        no_of_win_game_HAD: rank.no_of_win_game_HAD,
                                                        coin: rank.coin
                                                    }))
                                                } else {
                                                    toggle()
                                                }
                                            }
                                            }>跟隨</Button>
                                        }
                                        {followee.find(followee => followee.username === rank.username) &&
                                            <Button onClick={() =>
                                                dispatch(deleteFollowee(
                                                    userId, rank.username
                                                ))}>取消跟隨</Button>
                                        }
                                    </div>
                                </div>
                            
                    </div>
                    
                )} </Col></CardBody>
                </Card>

                {showRanking === "Top 11 - 20" && ranking.slice(10, 20).map(rank =>
                    <div className="ranking-container">
                        <Card>
                            <CardBody>
                                <div className="ranking">
                                    <div className="ranking-user-info">
                                        <div>高手大名：{rank.username}</div>
                                        <div>總投注場數：{rank.no_of_game}</div>
                                        <div>勝出場數：{rank.no_of_win_game}</div>
                                        <div>主客和勝出場數：{rank.no_of_win_game_HAD}</div>
                                        <div>波膽勝出場數：{rank.no_of_win_game_CRS}</div>
                                        <div>入球大細勝出場數：{rank.no_of_win_game_HIL}</div>
                                        <div>角球大細勝出場數：{rank.no_of_win_game_COR}</div>
                                        <div>讓球勝出場數：{rank.no_of_win_game_HDC}</div>
                                        <div>${rank.coin}</div>
                                    </div>
                                    <div className="follow-button">
                                        {!(followee.find(followee => followee.username === rank.username)) &&
                                            <Button onClick={() => {
                                                if (userId) {
                                                    dispatch(insertFollowee({
                                                        id: userId,
                                                        username: rank.username,
                                                        no_of_game: rank.no_of_game,
                                                        no_of_win_game: rank.no_of_win_game,
                                                        no_of_game_CRS: rank.no_of_game_CRS,
                                                        no_of_win_game_CRS: rank.no_of_win_game_CRS,
                                                        no_of_game_HIL: rank.no_of_game_HIL,
                                                        no_of_win_game_HIL: rank.no_of_win_game_HIL,
                                                        no_of_game_COR: rank.no_of_game_COR,
                                                        no_of_win_game_COR: rank.no_of_win_game_COR,
                                                        no_of_game_HDC: rank.no_of_game_HDC,
                                                        no_of_win_game_HDC: rank.no_of_win_game_HDC,
                                                        no_of_game_HAD: rank.no_of_game_HAD,
                                                        no_of_win_game_HAD: rank.no_of_win_game_HAD,
                                                        coin: rank.coin
                                                    }))
                                                } else {
                                                    toggle()
                                                }
                                            }
                                            }>跟隨</Button>
                                        }
                                        {followee.find(followee => followee.username === rank.username) &&
                                            <Button onClick={() =>
                                                dispatch(deleteFollowee(
                                                    userId, rank.username
                                                ))}>取消跟隨</Button>
                                        }
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                )}

                {showRanking === "Top 21 - 30" && ranking.slice(20, 30).map(rank =>
                    <div className="ranking-container">
                        <Card>
                            <CardBody>
                                <div className="ranking">
                                    <div className="ranking-user-info">
                                        <div>高手大名：{rank.username}</div>
                                        <div>總投注場數：{rank.no_of_game}</div>
                                        <div>勝出場數：{rank.no_of_win_game}</div>
                                        <div>主客和勝出場數：{rank.no_of_win_game_HAD}</div>
                                        <div>波膽勝出場數：{rank.no_of_win_game_CRS}</div>
                                        <div>入球大細勝出場數：{rank.no_of_win_game_HIL}</div>
                                        <div>角球大細勝出場數：{rank.no_of_win_game_COR}</div>
                                        <div>讓球勝出場數：{rank.no_of_win_game_HDC}</div>
                                        <div>${rank.coin}</div>
                                    </div>
                                    <div className="follow-button">
                                        {!(followee.find(followee => followee.username === rank.username)) &&
                                            <Button onClick={() => {
                                                if (userId) {
                                                    dispatch(insertFollowee({
                                                        id: userId,
                                                        username: rank.username,
                                                        no_of_game: rank.no_of_game,
                                                        no_of_win_game: rank.no_of_win_game,
                                                        no_of_game_CRS: rank.no_of_game_CRS,
                                                        no_of_win_game_CRS: rank.no_of_win_game_CRS,
                                                        no_of_game_HIL: rank.no_of_game_HIL,
                                                        no_of_win_game_HIL: rank.no_of_win_game_HIL,
                                                        no_of_game_COR: rank.no_of_game_COR,
                                                        no_of_win_game_COR: rank.no_of_win_game_COR,
                                                        no_of_game_HDC: rank.no_of_game_HDC,
                                                        no_of_win_game_HDC: rank.no_of_win_game_HDC,
                                                        no_of_game_HAD: rank.no_of_game_HAD,
                                                        no_of_win_game_HAD: rank.no_of_win_game_HAD,
                                                        coin: rank.coin
                                                    }))
                                                } else {
                                                    toggle()
                                                }
                                            }
                                            }>跟隨</Button>
                                        }
                                        {followee.find(followee => followee.username === rank.username) &&
                                            <Button onClick={() =>
                                                dispatch(deleteFollowee(
                                                    userId, rank.username
                                                ))}>取消跟隨</Button>
                                        }
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                )}

                {showRanking === "Top 31 - 40" && ranking.slice(30, 40).map(rank =>
                    <div className="ranking-container">
                        <Card>
                            <CardBody>
                                <div className="ranking">
                                    <div className="ranking-user-info">
                                        <div>高手大名：{rank.username}</div>
                                        <div>總投注場數：{rank.no_of_game}</div>
                                        <div>勝出場數：{rank.no_of_win_game}</div>
                                        <div>主客和勝出場數：{rank.no_of_win_game_HAD}</div>
                                        <div>波膽勝出場數：{rank.no_of_win_game_CRS}</div>
                                        <div>入球大細勝出場數：{rank.no_of_win_game_HIL}</div>
                                        <div>角球大細勝出場數：{rank.no_of_win_game_COR}</div>
                                        <div>讓球勝出場數：{rank.no_of_win_game_HDC}</div>
                                        <div>${rank.coin}</div>
                                    </div>
                                    <div className="follow-button">
                                        {!(followee.find(followee => followee.username === rank.username)) &&
                                            <Button onClick={() => {
                                                if (userId) {
                                                    dispatch(insertFollowee({
                                                        id: userId,
                                                        username: rank.username,
                                                        no_of_game: rank.no_of_game,
                                                        no_of_win_game: rank.no_of_win_game,
                                                        no_of_game_CRS: rank.no_of_game_CRS,
                                                        no_of_win_game_CRS: rank.no_of_win_game_CRS,
                                                        no_of_game_HIL: rank.no_of_game_HIL,
                                                        no_of_win_game_HIL: rank.no_of_win_game_HIL,
                                                        no_of_game_COR: rank.no_of_game_COR,
                                                        no_of_win_game_COR: rank.no_of_win_game_COR,
                                                        no_of_game_HDC: rank.no_of_game_HDC,
                                                        no_of_win_game_HDC: rank.no_of_win_game_HDC,
                                                        no_of_game_HAD: rank.no_of_game_HAD,
                                                        no_of_win_game_HAD: rank.no_of_win_game_HAD,
                                                        coin: rank.coin
                                                    }))
                                                } else {
                                                    toggle()
                                                }
                                            }
                                            }>跟隨</Button>
                                        }
                                        {followee.find(followee => followee.username === rank.username) &&
                                            <Button onClick={() =>
                                                dispatch(deleteFollowee(
                                                    userId, rank.username
                                                ))}>取消跟隨</Button>
                                        }
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                )}

                {showRanking === "Top 41 - 50" && ranking.slice(40, 50).map(rank =>
                    <div className="ranking-container">
                        <Card>
                            <CardBody>
                                <div className="ranking">
                                    <div className="ranking-user-info">
                                        <div>高手大名：{rank.username}</div>
                                        <div>總投注場數：{rank.no_of_game}</div>
                                        <div>勝出場數：{rank.no_of_win_game}</div>
                                        <div>主客和勝出場數：{rank.no_of_win_game_HAD}</div>
                                        <div>波膽勝出場數：{rank.no_of_win_game_CRS}</div>
                                        <div>入球大細勝出場數：{rank.no_of_win_game_HIL}</div>
                                        <div>角球大細勝出場數：{rank.no_of_win_game_COR}</div>
                                        <div>讓球勝出場數：{rank.no_of_win_game_HDC}</div>
                                        <div>${rank.coin}</div>
                                    </div>
                                    <div className="follow-button">
                                        {!(followee.find(followee => followee.username === rank.username)) &&
                                            <Button onClick={() => {
                                                if (userId) {
                                                    dispatch(insertFollowee({
                                                        id: userId,
                                                        username: rank.username,
                                                        no_of_game: rank.no_of_game,
                                                        no_of_win_game: rank.no_of_win_game,
                                                        no_of_game_CRS: rank.no_of_game_CRS,
                                                        no_of_win_game_CRS: rank.no_of_win_game_CRS,
                                                        no_of_game_HIL: rank.no_of_game_HIL,
                                                        no_of_win_game_HIL: rank.no_of_win_game_HIL,
                                                        no_of_game_COR: rank.no_of_game_COR,
                                                        no_of_win_game_COR: rank.no_of_win_game_COR,
                                                        no_of_game_HDC: rank.no_of_game_HDC,
                                                        no_of_win_game_HDC: rank.no_of_win_game_HDC,
                                                        no_of_game_HAD: rank.no_of_game_HAD,
                                                        no_of_win_game_HAD: rank.no_of_win_game_HAD,
                                                        coin: rank.coin
                                                    }))
                                                } else {
                                                    toggle()
                                                }
                                            }
                                            }>跟隨</Button>
                                        }
                                        {followee.find(followee => followee.username === rank.username) &&
                                            <Button onClick={() =>
                                                dispatch(deleteFollowee(
                                                    userId, rank.username
                                                ))}>取消跟隨</Button>
                                        }
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                )}
            </Paper>

            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader>請先登入</ModalHeader>
                <ModalBody>
                    登入咗先Follow到呀！未有Account？去呢度<a href="/login">註冊</a>先啦！
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>
                        確定
                    </Button>
                    <Button color="secondary" onClick={toggle}>
                        取消
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
        </Col>
        </Row>
        </Container>
    );
}
