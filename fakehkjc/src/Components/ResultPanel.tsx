import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import { leagueEngToChi } from "../App";
import { fetchMatches } from "../redux/match/action";
import { Match } from "../redux/match/reducer";
import { RootState } from "../store";
import { ResultRow } from "./MatchRow";


export function ResultPanel(props: { matches: Match[] } & { type: string }): JSX.Element {
    const [display, setDisplay] = useState(props.matches)
    const routeMatch = useRouteMatch<{ id?: string }>()
    const matches = useSelector((state: RootState) => state.match.matches)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchMatches())
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        if (props.type === "") {
            setDisplay(props.matches)
        } else if (routeMatch.params.id) {
            setDisplay(matches.filter(match => match.match_type === leagueEngToChi[routeMatch.params.id!]))
        } else {
            setDisplay(props.matches.filter(match => match.match_type === props.type))
        }
        // eslint-disable-next-line
    }, [props.type, routeMatch])
    // console.log(display)

    return (
        <>
            {display.map((eachMatch, index) => {
                if (eachMatch.matchStatus === "ResultIn" && eachMatch.match_type === props.type && eachMatch.away_full_match_score !== null) {
                    return <ResultRow match={eachMatch} key={index} />
                } else if (eachMatch.matchStatus === "ResultIn") {
                    return <ResultRow match={eachMatch} key={index} />
                } else {
                    return <></>
                }
            })}
        </>
    )

}


// export function ResultPanel(props: {match: Match[]} & { type: string }): JSX.Element {
//     //const matches = useSelector((state: RootState) => state.match.matches)
//     const [display, setDisplay] = useState(props.match)
//     console.log(props.match)
//     const dispatch = useDispatch()

//     useEffect(() => {
//       dispatch(fetchMatches())
//       // eslint-disable-next-line
//     }, [])

//     useEffect(() => {
//         if (props.type === "") {
//             console.log(props.type)
//             setDisplay(props.match)
//             console.log(display)
//         } else {
//             setDisplay(props.match.filter(match => match.match_type === props.type))
//         }
//         // eslint-disable-next-line
//     }, [props.type])

//     return (
//         <>

//             {display.map((eachMatch, index) => {
//                 if (eachMatch.matchStatus === "ResultIn" && eachMatch.match_type === props.type) {
//                     return <ResultRow match={eachMatch} key={index} />
//                 } else {
//                     return <ResultRow match={eachMatch} key={index} />
//                 }
//             })}
//         </>
//     )

// }


// export function ResultPanel(props: { matches: Match[] } & { type: string }): JSX.Element {
//     return (
//         <>
//             {props.matches.map((eachMatch, index) => {
//                 if (eachMatch.matchStatus === "ResultIn" && eachMatch.match_type === props.type) {
//                     return <ResultRow match={eachMatch} key={index} />
//                 }
//                 return <ResultRow match={eachMatch} key={index} />
//             })}
//         </>
//     )

// }