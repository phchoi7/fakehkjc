import React, { useState } from 'react'
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js"
import * as axios from 'axios'
import { Button } from 'reactstrap'
import "./Stripe.css"
import { sendTips } from '../redux/tips/action'
import { useRadioGroup } from '@material-ui/core'

const CARD_OPTIONS = {
    iconStyle: "solid",
    style: {
        base: {
            iconColor: "#c4f0ff",
            color:"fff",
            fontWeight: 500,
            fontSize: "16px",
            fontSmoothing:"antialiased",
        },
        invaild: {
            iconColor: "#ffc7ee",
            color: "ffc7ee"
        }
    }
}

export default function PaymentForm(props) {
    const [success, setSuccess] = useState(false)
    const stripe = useStripe()
    const elements = useElements()

    const handleSubmit = async (e) => {
        e.preventDefault()
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: "card",
            card: elements.getElement(CardElement)
        })


        if (!error) {
            try {
                const { id } = paymentMethod
                const response = await axios.post(`${process.env.REACT_APP_BACKEND_URL}/checkout`, {
                    amount: 100,
                    id
                })

                if (response.data.success) {
                    console.log('Successful payment')
                    setSuccess(true)
                    props.transaction()
                    
                }   
            } catch (error) {
                console.log('Error', error)
            }
        } else {
            console.log(error.message)
        }
    }

    return (
        <div>
            {!success ?
                <form onSubmit={handleSubmit}>
                    <fieldset className="FormGroup">
                        <div className="FormRow">
                            <CardElement options={CARD_OPTIONS} />
                        </div>
                    </fieldset>
                    <Button className="payButton">付款$100港元</Button>
                </form >
                :
                <div>
                    <h2 className="successfulMessage">成功付款！</h2>
                </div>
            }
        </div>
    )
}