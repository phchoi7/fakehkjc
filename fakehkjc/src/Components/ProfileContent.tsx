import moment from "moment"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
// import { fetchBetRecord } from "../redux/user/action"
import { weekday } from "../App";
import { Card, Icon } from 'semantic-ui-react'
// import { deleteFollowee } from "../redux/follow/action"
import { fetchPaidTips } from "../redux/tips/action"
import './ProfileContent.css'
import { RootState } from "../store"
import "moment-timezone"
import { betResultType } from "../App"
// import { Tips } from "../redux/tips/reducer";


export interface IfolloweeMatch {
    id: number | null,
    username: string | null,
    match_id: string | null,
    home_team: string | null,
    away_team: string | null,
    match_date: string | null,
    match_day: string | null,
    match_type: string | null
}
export function BetRecord() {


    const betRecords = useSelector((state: RootState) => state.user_bet_record.bets)
    // console.log(betRecords)


    return (
        <div className="bet_record">
            {betRecords && betRecords.map(record => {
                return (
                    <Card className = "bet-record-card"key={betRecords.indexOf(record)}>
                         <Card.Content header='投注記錄' />
                        <Card.Content description>
                            <div className='bet-match-info'>
                                <div>
                                    {record.match_type}
                                </div>
                                <div>
                                    {moment(record.match_date).tz("Asia/Hong_Kong").format("YYYY-MM-DD")}
                                </div>
                                <div className="">
                                    {record.home_team} ({record.home_full_match_score}：{record.away_full_match_score}) {record.away_team}
                                </div>
                            </div>

                            <div className="bet-record-info">
                                <div>
                                    落注日期：{moment(record.created_at).tz("Asia/Hong_Kong").format("YYYY-MM-DD")}
                                </div>
                                <div>
                                    {record.bet_type}：{record.bet_content}
                                </div>
                                <div>
                                    賠率：{record.odds}
                                </div>
                                <div>
                                    落注： {new Intl.NumberFormat().format(record.spent_coin)} 金幣
                                </div>
                                <div>
                                    結果：{betResultType[record.bet_result_type]}
                                </div>
                            </div>
                        </Card.Content>
                        <Card.Content extra>
                                <div className="gain-coins">
                                    <strong>存入：{new Intl.NumberFormat().format(record.gain_coin)} 金幣</strong>
                                </div>
                                </Card.Content>
                    </Card>
                )
            })}
        </div>
    )
}
