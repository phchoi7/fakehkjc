import moment from "moment"
import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
// import { fetchBetRecord } from "../redux/user/action"
import { weekday } from "../App";
import { Card, CardBody } from "reactstrap";
// import { deleteFollowee } from "../redux/follow/action"
import { fetchPaidTips } from "../redux/tips/action"
import "./TipsRow.css"
import { RootState } from "../store"
import "moment-timezone"
import { betResultType } from "../App"
import { List, ListItem } from "semantic-ui-react";
import { Divider, ListItemText } from "@material-ui/core";
import ThumbUpIcon from '@material-ui/icons/ThumbUp';

// import { Tips } from "../redux/tips/reducer";


export function BoughtTips() {
    const userId = useSelector((state: RootState) => state.auth.user_id)
    // const coin = useSelector((state: RootState) => state.user.coin)
    const paidTips = useSelector((state: RootState) => state.paidTips.paidTips)
    const [paidTipsUser, setPaidTipsUser] = useState<string[]>([])
    // const [paidTipsMatch, setPaidTipsMatch] = useState<string[]>([])

    const [showTips, setShowTips] = useState<string>()
    const [showBets, setShowBets] = useState<string>()

    const dispatch = useDispatch()

    const [userArray, setUserArray] = useState<string[]>([])

    // const userArray: string[] = []

    // let matchArray: string[] = []

    useEffect(() => {
        if (userId) {
            dispatch(fetchPaidTips(userId))
        }


    }, [dispatch, userId])

    // why need not setUserArray
    useEffect(() => {
        for (let tip of paidTips) {
            if (!(userArray.find(user => user === tip.username))) {
                userArray.push(tip.username)
            }
        }
        console.log(userArray)
    }, [paidTips])

    // something went wrong, but why wrong
    // useEffect(() => {
    //     for (let tips of paidTips) {
    //         if (!(userArray.find(user => user === tips.username))) {
    //             userArray.push(tips.username)
    //         }
    //     }
    //     setPaidTipsUser(userArray)
    // }, [userArray, paidTips])

    return (
        <div className="TipsRow">
            {
                userArray.length > 0 && userArray.map(username => (
                    <>

                        <div className="tip-provider-info">
                            <div className="tip-provider-name">高手大名：{username}</div>
                        </div>

                        <div className="tips-row" key={userArray.indexOf(username)}>
                            <div onClick={() => setShowTips(username)}>
                                <Card type="button"
                                    className="card-frame card-edit">


                                    <CardBody className="text">
                                        {/* <div className="tips-detail"> */}
                                        <div className="tip-provider-info">
                                            <div className="tip-provider-name">高手大名：{username}</div>
                                        </div>
                                        {/* </div> */}
                                    </CardBody>

                                    {paidTips.filter(tips => tips.username === username)
                                        .reduce((acc, tips) => {
                                            const previousMatch = acc.find(match => match.match_id === tips.match_id);
                                            if (previousMatch) {
                                                previousMatch.tips.push(tips)
                                                return acc
                                            } else {
                                                return acc.concat([{
                                                    match_id: tips.match_id,
                                                    home_team: tips.home_team,
                                                    away_team: tips.away_team,
                                                    match_type: tips.match_type,
                                                    match_date: tips.match_date,
                                                    match_day: tips.match_day,
                                                    tips: [tips]
                                                }])
                                            }
                                        }, [] as {
                                            match_id: string,
                                            home_team: string,
                                            away_team: string,
                                            match_type: string,
                                            match_date: string,
                                            match_day: string,
                                            tips: {
                                                bet_type: string,
                                                bet_type_content: string,
                                                bet_content: string,
                                                odds: number
                                            }[]
                                        }[])
                                        .map(tip => (
                                            <div>
                                                <div onClick={() => setShowBets(tip.match_id)}>
                                                    <Card className="match-content" >
                                                        <div >{tip.home_team} 對 {tip.away_team}</div>
                                                        <div >{tip.match_type}</div>
                                                        <div >{tip.match_day}</div>
                                         星期{
                                                            weekday[
                                                            moment(tip.match_date)
                                                                .format("ddd")
                                                                .toUpperCase()
                                                            ]
                                                        }{" "}

                                                    </Card>
                                                </div>

                                                {showBets === tip.match_id && tip.tips.map(bet => (
                                                    <div className="tips-content">
                                                        <Card>
                                                            <div>{bet.bet_type}</div>
                                                            <div>{bet.bet_type_content}</div>
                                                            <div>{bet.bet_content}</div>
                                                            <div>{bet.odds}</div>
                                                        </Card>
                                                    </div>
                                                ))}
                                            </div>
                                        ))}

                                </Card>
                            </div>

                        </div>
                    </>
                ))
            }
        </div>

        //         <div>
        //             {showTips === username && paidTips.filter(tips => showTips === tips.username)
        //                 .reduce((acc, tips) => {
        //                     const previousMatch = acc.find(match => match.match_id === tips.match_id);
        //                     if (previousMatch) {
        //                         previousMatch.tips.push(tips)
        //                         return acc
        //                     } else {
        //                         return acc.concat([{
        //                             match_id: tips.match_id,
        //                             home_team: tips.home_team,
        //                             away_team: tips.away_team,
        //                             match_type: tips.match_type,
        //                             match_date: tips.match_date,
        //                             match_day: tips.match_day,
        //                             tips: [tips]
        //                         }])
        //                     }
        //                 }, [] as {
        //                     match_id: string,
        //                     home_team: string,
        //                     away_team: string,
        //                     match_type: string,
        //                     match_date: string,
        //                     match_day: string,
        //                     tips: {
        //                         bet_type: string,
        //                         bet_type_content: string,
        //                         bet_content: string,
        //                         odds: number
        //                     }[]
        //                 }[])
        //                 .map(tip => (
        //                     <div>
        //                         <div onClick={() => setShowBets(tip.match_id)}>
        //                             <Card className="match-content" >
        //                                 <div >{tip.home_team} 對 {tip.away_team}</div>
        //                                 <div >{tip.match_type}</div>
        //                                 <div >{tip.match_day}</div>
        //                                 星期{
        //                                     weekday[
        //                                     moment(tip.match_date)
        //                                         .format("ddd")
        //                                         .toUpperCase()
        //                                     ]
        //                                 }{" "}

        //                             </Card>
        //                         </div>

        //                         {showBets === tip.match_id && tip.tips.map(bet => (
        //                             <div className="tips-content">
        //                                 <Card>
        //                                     <div>{bet.bet_type}</div>
        //                                     <div>{bet.bet_type_content}</div>
        //                                     <div>{bet.bet_content}</div>
        //                                     <div>{bet.odds}</div>
        //                                 </Card>
        //                             </div>
        //                         ))}
        //                     </div>
        //                 ))
        //             }
        //         </div>
        //     </div>
        // )} */
        // // {/* </div> */}
    )
}


