import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { OddColumn } from "./OddColumn";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { insertDraftBet, selectAllDraftBets } from "../redux/cart/action";
import { oddTypeMap } from "../redux/match/reducer";
import { betTypeMap } from "../redux/match/reducer";
import { Match } from "../redux/match/reducer";
import { Container, Row, Col } from "reactstrap";
import { push } from "connected-react-router";

import { Box } from 'grommet';
import { Link } from "react-router-dom";
export function OddPanel(props: { match: Match }) {
  // const odds = useSelector((state: RootState) => state.odd.odds[props.match])
  const [selected, setSelected] = useState<any[]>([]);
  const [modal, setModal] = useState(false);
  const [formValues, setFormValues] = useState<{ [T: string]: number }>({});
  const toggle = () => setModal(!modal);
  const dispatch = useDispatch();
  const userID = useSelector((state: RootState) => state.user.id)
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const [validationMsg, setValidationMsg] = useState('')

  const [alertModal, setAlertModal] = useState(false)
  const toggleAlert = () => {
    console.log("alert modal")
    console.log(alertModal)
    setAlertModal(!alertModal)
  }


  // if (odds == null) {
  //     return <div>no odds</div>
  // }

  function onOddClick(oddType: string) {
    if (selected.indexOf(oddType) > -1) {
      setSelected(selected.filter((s) => s !== oddType));
    } else {
      setSelected(selected.concat([oddType]));
    }
  }
  return (
    <Container className="OddPanel">
      <div className="odd-panel">
        {/*------主客和------*/}

        <div className="odd-row">
          <Row className="odd-bet-title">
            <Col className="odd-col-4">
              <div className="odd-type"><strong>主客和</strong></div>
            </Col>
          </Row>
          <Row className="odd-bet-title">
            <div className="odd-detail">
              <Col sm="4" className="odd-col-4">
                <OddColumn
                  oddType="had_h_odds"
                  oddLabel={oddTypeMap["had_h_odds"]}
                  betType={"主客和"}
                  betContent={""}
                  odd={props.match}
                  selected={selected}
                  onClick={onOddClick}
                />
              </Col>
              <Col sm="4" className="odd-col-4">
                <OddColumn
                  oddType="had_d_odds"
                  oddLabel={oddTypeMap["had_d_odds"]}
                  betType={"主客和"}
                  betContent={""}
                  odd={props.match}
                  selected={selected}
                  onClick={onOddClick}
                />
              </Col>
              <Col sm="4" className="odd-col-4 scores-right">
                <OddColumn
                  oddType="had_a_odds"
                  oddLabel={oddTypeMap["had_a_odds"]}
                  betType={"主客和"}
                  betContent={""}
                  odd={props.match}
                  selected={selected}
                  onClick={onOddClick}
                />
              </Col>
            </div>
          </Row>
        </div>

        {/*------讓球盤------*/}
        {props.match.hdc_hg && (
          <div className="odd-row">
            <Row className="odd-bet-title">
              <Col className="odd-col-4">
                <div className="odd-type"><strong>讓球盤</strong></div>
              </Col>
            </Row>
            <Row className="odd-bet-title">
              <div className="odd-detail">
                <Col xs="6" className="odd-col-4">
                  <OddColumn
                    oddType="hdc_h_odds"
                    oddLabel={oddTypeMap["hdc_h_odds"]}
                    betType={"讓球"}
                    betContent={props.match.hdc_hg}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </Col>

                <Col xs="6" className="odd-col-4 scores-right">
                  <OddColumn
                    oddType="hdc_a_odds"
                    oddLabel={oddTypeMap["hdc_a_odds"]}
                    betType={"讓球"}
                    betContent={props.match.hdc_ag}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </Col>
              </div>
            </Row>
          </div>
        )}

        {/*------波膽------*/}
        <div className="odd-row">
          <Row className="odd-bet-title">
            <Col className="odd-col-4">
              <div className="odd-type"><strong>波膽</strong></div>
            </Col>
          </Row>
          <Row className="odd-bet-title">
            <div className="odd-detail">
              <Col sm="4" className="odd-col-4">
                <div className="odd">
                  <div>主</div>
                </div>

                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0100"
                    oddLabel={oddTypeMap["crs_odds_s0100"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0200"
                    oddLabel={oddTypeMap["crs_odds_s0200"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0201"
                    oddLabel={oddTypeMap["crs_odds_s0201"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0300"
                    oddLabel={oddTypeMap["crs_odds_s0300"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0301"
                    oddLabel={oddTypeMap["crs_odds_s0301"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0302"
                    oddLabel={oddTypeMap["crs_odds_s0302"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0400"
                    oddLabel={oddTypeMap["crs_odds_s0400"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0401"
                    oddLabel={oddTypeMap["crs_odds_s0401"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0402"
                    oddLabel={oddTypeMap["crs_odds_s0402"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0500"
                    oddLabel={oddTypeMap["crs_odds_s0500"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0501"
                    oddLabel={oddTypeMap["crs_odds_s0501"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0502"
                    oddLabel={oddTypeMap["crs_odds_s0502"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_sm1mh"
                    oddLabel={oddTypeMap["crs_odds_sm1mh"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
              </Col>
              <Col sm="4" className="odd-col-4">
                <div className="odd odd-line">
                  <div>和</div>
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0000"
                    oddLabel={oddTypeMap["crs_odds_s0000"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0101"
                    oddLabel={oddTypeMap["crs_odds_s0101"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0202"
                    oddLabel={oddTypeMap["crs_odds_s0202"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_s0303"
                    oddLabel={oddTypeMap["crs_odds_s0303"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores">
                  <OddColumn
                    oddType="crs_odds_sm1md"
                    oddLabel={oddTypeMap["crs_odds_sm1md"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
              </Col>
              <Col sm="4" className="odd-col-4">
                <div className="odd">
                  <div>客</div>
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0001"
                    oddLabel={oddTypeMap["crs_odds_s0001"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0002"
                    oddLabel={oddTypeMap["crs_odds_s0002"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0102"
                    oddLabel={oddTypeMap["crs_odds_s0102"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0003"
                    oddLabel={oddTypeMap["crs_odds_s0003"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0103"
                    oddLabel={oddTypeMap["crs_odds_s0103"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0203"
                    oddLabel={oddTypeMap["crs_odds_s0203"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0004"
                    oddLabel={oddTypeMap["crs_odds_s0004"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0104"
                    oddLabel={oddTypeMap["crs_odds_s0104"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0204"
                    oddLabel={oddTypeMap["crs_odds_s0204"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0005"
                    oddLabel={oddTypeMap["crs_odds_s0005"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0105"
                    oddLabel={oddTypeMap["crs_odds_s0105"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_s0205"
                    oddLabel={oddTypeMap["crs_odds_s0205"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
                <div className="scores scores-right">
                  <OddColumn
                    oddType="crs_odds_sm1ma"
                    oddLabel={oddTypeMap["crs_odds_sm1ma"]}
                    betType={"波膽"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
              </Col>
            </div>
          </Row>
        </div>

        {/*------入球大細------*/}
        <div className="odd-row">
          <Row className="odd-bet-title">
            <Col className="odd-col-4">
              <div className="odd-type"><strong>入球大細</strong></div>
            </Col>
          </Row>
          <Row className="odd-bet-title">
            <div className="odd-detail">
              <Col sm="4" className="odd-col-4">
                <div className="odd scores-right">
                  <OddColumn
                    oddType="hil_odds_h"
                    oddLabel={oddTypeMap["hil_odds_h"]}
                    betType={"入球大細"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
              </Col>
              <Col sm="4" className="odd-col-4">
                <div className="odd odd-line">
                  <div>球數</div>
                  <div>{props.match.hil_odds_line}</div>
                </div>
              </Col>
              <Col sm="4" className="hil-odds-l">
                <div className="odd scores-right">
                  <OddColumn
                    oddType="hil_odds_l"
                    oddLabel={oddTypeMap["hil_odds_l"]}
                    betType={"入球大細"}
                    betContent={""}
                    odd={props.match}
                    selected={selected}
                    onClick={onOddClick}
                  />
                </div>
              </Col>
            </div>
          </Row>
        </div>

        {/*------角球大細------*/}
        {props.match.chl_odds_line && (
          <div className="odd-row">
            <Row className="odd-bet-title">
              <Col className="odd-col-4">
                <div className="odd-type"><strong>角球大細</strong></div>
              </Col>
            </Row>
            <Row className="odd-bet-title">
              <div className="odd-detail">
                <Col sm="4" className="odd-col-4">
                  <div className="odd scores-right">
                    <OddColumn
                      oddType="chl_odds_h"
                      oddLabel={oddTypeMap["chl_odds_h"]}
                      betType={"角球大細"}
                      betContent={""}
                      odd={props.match}
                      selected={selected}
                      onClick={onOddClick}
                    />
                  </div>
                </Col>
                <Col sm="4" className="odd-col-4">
                  <div className="odd odd-line">
                    <div>球數</div>
                    <div>{props.match.chl_odds_line}</div>
                  </div>
                </Col>
                <Col sm="4" className="odd-col-4">
                  <div className="odd scores-right">
                    <OddColumn
                      oddType="chl_odds_l"
                      oddLabel={oddTypeMap["chl_odds_l"]}
                      betType={"角球大細"}
                      betContent={""}
                      odd={props.match}
                      selected={selected}
                      onClick={onOddClick}
                    />
                  </div>
                </Col>
              </div>
            </Row>
          </div>
        )}
      </div>

      <div>
        <Box align="center" pad="medium">
          <Button color="primary" className="button-color" onClick={() => {
            if (isAuthenticated) {
              toggle()
            } else {
              toggleAlert()
              // dispatch(push('/auth'))
            }
          }} >加入注項</Button>

        </Box>


        {/* modal start*/}
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>加入注項（最低投注額為100金幣）</ModalHeader>
          <ModalBody>
            {selected.map((oddType) => (
              <div key={selected.indexOf(oddType)}>
                {betTypeMap[oddType]} {oddTypeMap[oddType]}
                {oddType === "hdc_h_odds"
                  ? `[ ${props.match.hdc_hg} ]`
                  : ""}{" "}
                {oddType === "hdc_a_odds" ? `[ ${props.match.hdc_ag} ]` : ""}{" "}
                {oddType === "hil_odds_h" || oddType === "hil_odds_l"
                  ? `[ ${props.match.hil_odds_line} ]`
                  : ""}{" "}
                {oddType === "chl_odds_h" || oddType === "chl_odds_l"
                  ? `[ ${props.match.chl_odds_line} ]`
                  : ""}
                : $
                <input
                  type="number"
                  value={formValues[oddType]}
                  onChange={(event) => {
                    let value = event.currentTarget.valueAsNumber
                    if (!(Number.isInteger(value))) {
                      setValidationMsg("請輸入正數。")
                    } else if (Number.isNaN(value)) {
                      setValidationMsg("請輸入數字。")
                    } else if (value < 100) {
                      setValidationMsg("最低投注額為100金幣。")
                    } else {
                      setValidationMsg("")
                      setFormValues({
                        ...formValues,
                        [oddType]: event.currentTarget.value,
                      })
                    }

                    // let value = event.currentTarget.valueAsNumber
                    // setFormValues({
                    //   ...formValues,
                    //   [oddType]: value,
                    // })
                  }
                    // let value = event.currentTarget.valueAsNumber
                    // if (!(Number.isInteger(value))) {
                    //   setValidationMsg("請輸入正數。")
                    // } else if (Number.isNaN(value)) {
                    //   setValidationMsg("請輸入數字。")
                    // } else if (value < 100) {
                    //   setValidationMsg("最低投注額為100金幣。")
                    // } else {
                    //   setValidationMsg("")
                    //   setFormValues({
                    //     ...formValues,
                    //     [oddType]: event.currentTarget.value,
                    //   })
                    // }
                  } />



              </div>
            ))}
          </ModalBody>

          <ModalFooter>
            <div>{validationMsg}</div>
            <Button
              color="primary"
              onClick={() => {
                // console.log(selected);

                if (validationMsg === "") {
                  for (let oddType of selected) {
                    dispatch(insertDraftBet({
                      id: null,
                      end_user_id: userID!,
                      match_id: props.match.jc_match_id,
                      odds: (props.match as any)[oddType],
                      bet_type: betTypeMap[oddType],
                      bet_type_content:
                        oddType === "hdc_h_odds"
                          ? props.match.hdc_hg
                          : "" || oddType === "hdc_a_odds"
                            ? props.match.hdc_ag
                            : "" ||
                              oddType === "hil_odds_h" ||
                              oddType === "hil_odds_l"
                              ? props.match.hil_odds_line
                              : "" ||
                                oddType === "chl_odds_h" ||
                                oddType === "chl_odds_l"
                                ? props.match.chl_odds_line
                                : "",
                      bet_content: oddType,
                      spent_coin: formValues[oddType],
                    }))
                  }
                } else {
                  return
                }

                toggle();
                dispatch(selectAllDraftBets(userID!))
                dispatch(push('/bet-area'))
              }}>
              確定
            </Button>
            <Button color="secondary" onClick={toggle}>
              取消
            </Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={alertModal} toggle={toggleAlert}>
            <ModalHeader>請先登入</ModalHeader>
            <ModalBody>
              登入咗先可以買波~未有Account？去呢度<Link to="/login">註冊</Link>啦！
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={toggleAlert}>
                確定
              </Button>
            </ModalFooter>
          </Modal>

      </div>
    </Container>
  );
}

