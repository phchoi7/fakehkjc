import React, { useEffect, useState } from "react"
import { Card, CardBody, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap"
import { Match } from "../redux/match/reducer"
import { Button } from 'reactstrap';
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../store"
import { TipsProvider } from '../redux/tips/reducer'
import './TipsRow.css';
import { fetchPaidTips, sendTips } from "../redux/tips/action";
// import { Bet } from "../redux/bet/reducer";
import { Row, Col } from "reactstrap";
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import FaceIcon from '@material-ui/icons/Face';
import StarIcon from '@material-ui/icons/Star';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Stripe from './Stripe.js'
import { Link, useHistory } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
        },
        badge: {
            '& > *': {
                margin: theme.spacing(1),
            },
            margin: '3px',
        },
    }),
);

// import { Bet } from "../redux/bet/reducer";

export function TipsRow(props: { match: Match }) {
    const matchId = props.match.jc_match_id
    const [tipsProvider, setTipsProvider] = useState<TipsProvider[]>()
    const [buyTips, setBuyTips] = useState(false)
    const userId = useSelector((state: RootState) => state.auth.user_id)
    // const coin = useSelector((state: RootState) => state.user.coin)
    const paidTips = useSelector((state: RootState) => state.paidTips.paidTips)
    //for modal
    const [modal, setModal] = useState(false);
    const [modalPurchase, setModalPurchase] = useState(false);
    const toggle = () => setModal(!modal);
    const togglePurchase = () => setModalPurchase(!modalPurchase);
    //for modal
    const transaction = () => setBuyTips(true)


    const dispatch = useDispatch()
    const classes = useStyles();
    const history = useHistory();

    const routeChange = () => {
        let path = `/profile/tips`;
        history.push(path);
    }

    // const [selected, setSelected] = useState<string[]>([])
    useEffect(() => {
        async function loadTipsProvider(matchId: string) {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/tips/${matchId}`)
            const json = await res.json()
            let jsonWithoutSelf = []
            for (const tip of json) {
                if (!(tip.id === userId)) {
                    jsonWithoutSelf.push(tip)
                }
            }
            console.log(jsonWithoutSelf)
            
            setTipsProvider(jsonWithoutSelf)
        }
        loadTipsProvider(matchId)
    }, [])

    useEffect(() => {
        if (userId) {
            dispatch(fetchPaidTips(userId))
        }
    }, [userId])



    return (
        <div className="TipsRow">
            <Row>
                <Col>
                    {tipsProvider?.map(tips =>

                        <div className="tips-row" key={tipsProvider.indexOf(tips)}>
                            <Card className="card-frame card-edit">
                                <CardBody className="text">
                                    <div className="mobile-tips">
                                        <List component="nav" className={classes.root} aria-label="mailbox folders">
                                            <ListItem button>
                                                <FaceIcon />
                                                <ListItemText primary={'高手大名：' + tips.username} />
                                            </ListItem>
                                            <Divider />
                                            <ListItem button divider>
                                                <MonetizationOnIcon />
                                                <ListItemText primary={'戶口資金：' + tips.coin} />
                                            </ListItem>
                                            <ListItem button>
                                                <StarIcon />
                                                <ListItemText primary={' 近十場戰績：'} />
                                            </ListItem>
                                            <Divider light />
                                            <ListItem button>
                                                <ListItemText primary={tips.betRecord.map(record => {
                                                    if (record === "win") {
                                                        return <span className={classes.badge}><Badge className="badge-color" badgeContent={'W'} color="primary">
                                                        </Badge> </span>
                                                    } else if (record === "draw") {
                                                        return <span className={classes.badge}><Badge badgeContent={'D'} color="secondary">
                                                        </Badge> </span>
                                                    } else if (record === "lose") {
                                                        return <span className={classes.badge}><Badge badgeContent={'L'} color="error">
                                                        </Badge> </span>
                                                    } else {
                                                        return <></>
                                                    }
                                                })
                                                } />
                                            </ListItem>
                                            <Divider light />
                                            <ListItem button>
                                                {paidTips.find(paidTip => paidTip.username == tips.username) && paidTips.find(paidTip => paidTip.match_id == matchId) && 
                                                <ListItemText primary={' 投注總注數：' + paidTips.length} />}
                                                
                                            </ListItem>
                                            <Divider light />
                                            <ListItem button>
                                                <div className="tip-button">
                                                    {paidTips.find(paidTip => paidTip.username === tips.username)
                                                        &&
                                                        paidTips.find(paidTip => paidTip.match_id === matchId)
                                                        &&
                                                        <Button onClick={routeChange}>已購買</Button>}

                                                    {!(paidTips.find(paidTip => paidTip.username === tips.username))
                                                        &&
                                                        <Button onClick={() => {
                                                            if (userId) {
                                                                togglePurchase()
                                                            } else {
                                                                toggle()
                                                            }
                                                        }
                                                        }>買貼士</Button>
                                                    }
                                                </div>
                                            </ListItem>
                                        </List>
                                    </div>
                                    <div className="tip-provider">
                                        <Col lg="6" xs="12">
                                            <div className="tips-detail">

                                                <div className="tip-provider-info">
                                                    <div className="tip-provider-name"> <FaceIcon />高手大名：{tips.username}</div>
                                                    <div><MonetizationOnIcon />戶口資金：{tips.coin}</div>
                                                </div>
                                                <Divider />
                                                <div className="tip-provider-bet">
                                                    <div className="tip-provider-result">
                                                        <StarIcon />近十場戰績：
                                                            {tips.betRecord.map(record => {
                                                            if (record === "win") {
                                                                return <span className={classes.badge}><Badge className="badge-color-win" badgeContent={'W'} color="primary">
                                                                </Badge> </span>
                                                            } else if (record === "draw") {
                                                                return <span className={classes.badge}><Badge className="badge-color-draw" badgeContent={'D'} color="primary">
                                                                </Badge> </span>
                                                            } else if (record === "lose") {
                                                                return <span className={classes.badge}><Badge badgeContent={'L'} color="error">
                                                                </Badge> </span>
                                                            } else {
                                                                return <></>
                                                            }
                                                        })}
                                                    </div>

                                                </div>

                                            </div>
                                        </Col>
                                        <Col xs="3">
                                            <div className="tip-bet-detail">
                                                <div className="tip-provider-bet-number">
                                                {paidTips.find(paidTip => paidTip.username == tips.username) && paidTips.find(paidTip => paidTip.match_id == matchId) && 
                                                <div>投注總注數：{paidTips.length} </div>
                                                }
                                                </div>
                                                <div className="tip-button tips-button">

                                                    {!(paidTips.find(paidTip => paidTip.username === tips.username))
                                                        &&
                                                        <div>
                                                            <Button onClick={() => {
                                                                if (userId) {
                                                                    togglePurchase()

                                                                } else {
                                                                    toggle()
                                                                }
                                                            }
                                                            }>買貼士</Button>

                                                            <Modal isOpen={modalPurchase} toggle={togglePurchase} backdrop={false}>
                                                                <ModalHeader>購買{tips.username}的貼士</ModalHeader>
                                                                <Stripe transaction={transaction} info={[{ userId: userId, username: tips.username, match_id: matchId }]} />
                                                                <ModalFooter>
                                                                    <Button onClick={() => {
                                                                        if (buyTips) {
                                                                            dispatch(sendTips({
                                                                                customer_id: userId,
                                                                                tips_provider: tips.username,
                                                                                match_id: matchId

                                                                            }))
                                                                            togglePurchase()
                                                                        } else {
                                                                            togglePurchase()
                                                                        }
                                                                    }}>確定</Button>
                                                                </ModalFooter>

                                                            </Modal>
                                                        </div>
                                                    }

                                                </div>
                                            </div>
                                        </Col>

                                    </div>

                                    <Modal isOpen={modal} toggle={toggle}>
                                        <ModalHeader>請先登入</ModalHeader>
                                        <ModalBody>
                                            登入咗先買到貼士呀~未有Account？去呢度<Link to="/login">註冊</Link>啦！
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button color="primary" onClick={toggle}>
                                                確定
                                            </Button>
                                        </ModalFooter>
                                    </Modal>
                                </CardBody>
                            </Card>
                            <div><div>推薦選擇</div>
                            {paidTips.find(paidTip => paidTip.username === tips.username)
                                &&
                                paidTips.find(paidTip => paidTip.match_id === matchId)
                                &&
                                paidTips.map(paidTip =>
                                    
                                    <List component="nav" className={classes.root + "tip-mobile-content"} aria-label="mailbox folders">    
                                        <ListItem button>
                                            <ThumbUpIcon />
                                            <ListItemText primary={paidTip.bet_type} />
                                            <ListItemText primary={paidTip.bet_type_content} />

                                            <ListItemText className="tips-choose" primary={'選擇：' + paidTip.bet_content} />
                                            <ListItemText primary={'賠率：' + paidTip.odds} />
                                        </ListItem>
                                        <Divider />
                                    </List>

                                )
                            }

                        </div></div>
                    )}
                </Col>
            </Row>
        </div >



    )
}

