// import { Odd } from "../redux/odd/reducer";
import { Match } from "../redux/match/reducer";

export function OddColumn(props: {
  oddType: string; // hostWin , draw ...
  oddLabel: string; // 主勝...
  odd: Match;
  // betType冇用??
  betType: string;
  betContent: any;
  selected: string[];
  onClick: (oddType: string) => void;
}) {


  // console.log(props.odd)
  return (
    <div
      className={(props.selected.indexOf(props.oddType) > -1 ? 'active' : '') + ' odd-col'}
      onClick={() => props.onClick(props.oddType)}>
      <div>
        {props.oddLabel}  {props.betContent}
      </div>
      <div>
        {(props.odd as any)[props.oddType]}
      </div>
    </div>
  )
}