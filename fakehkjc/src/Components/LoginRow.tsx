import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { login } from "../redux/auth/action"
import { RootState } from "../store"
// import firebaseConfig from '../firebaseConfig'
// import * as firebaseui from "firebaseui";
// import firebase from "firebase";
import {
    Box,
    Button,
    Grommet,
    Form,
    FormField,
    Text,
    TextInput,
  } from 'grommet';
  import { grommet } from 'grommet/themes';
import React from "react";


export function LoginRow() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const dispatch = useDispatch()
    const loginMsg = useSelector((state: RootState) => state.auth.message)
    const [value, setValue] = React.useState({ name: '', email: '' });
  const message =
    value.name && value.email && value.name[0] !== value.email[0]
      ? 'Mismatched first character'
      : undefined;


    // useEffect(() => {
    //     const fbase = firebase.initializeApp(firebaseConfig);
    //     const uiConfig = {
    //         signInSuccessUrl: "https://netflix-clone-ankur.herokuapp.com/", //This URL is used to return to that page when we got success response for phone authentication.
    //         signInOptions: [firebase.auth.PhoneAuthProvider.PROVIDER_ID],
    //         tosUrl: "https://netflix-clone-ankur.herokuapp.com/"
    //     };
    //     var ui = new firebaseui.auth.AuthUI(firebase.auth());
    //     ui.start("#firebaseui-auth-container", uiConfig);
    // })

    return (
        <div>

      <Box fill align="center" justify="center">
      <div>登入</div>
        <Box width="medium">
          <Form
            value={value}
            onChange={nextValue => setValue(nextValue as any)}
            onSubmit={(event) => {
                event.preventDefault()
                dispatch(login(email, password))
            }}>
            <FormField label="Email" name="email"  required >
              <TextInput name="email" type="email" placeholder="電郵" value={email} onChange={event => setEmail(event.currentTarget.value)}/>
            </FormField>

            <FormField label="Password" name="password"  required >
              <TextInput name="password" type="password" placeholder="密碼" value={password} onChange={event => setPassword(event.currentTarget.value)}/>
            </FormField>

            {message && (
              <Box pad={{ horizontal: 'small' }}>
                <Text color="status-error">{message} {loginMsg}</Text>
              </Box>
            )}

            <Box direction="row" justify="between" margin={{ top: 'medium' }}>
              <Button type="reset" label="註冊" />
              <Button type="submit" label="登入" primary />
            </Box>
          </Form>
        </Box>
      </Box>

            {/* <div id="firebaseui-auth-container"></div> */}
            {/* <div>登入</div>
            <div>{loginMsg}</div>
            <form onSubmit={(event) => {
                event.preventDefault()
                dispatch(login(email, password))
            }}>
                <input type="email" placeholder="電郵" value={email} onChange={event => setEmail(event.currentTarget.value)}></input>

                <input type="password" placeholder="密碼" value={password} onChange={event => setPassword(event.currentTarget.value)}></input>
                <input type="submit" />
            </form> */}
        </div >
    )
}

export function RegisterPage() {
    return (
        <div>
            <div>註冊</div>
            <input type="number" ></input>
            <input></input>
        </div>
    )
}
