import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab, { TabProps } from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Radar } from "react-chartjs-2";
import { useDispatch, useSelector } from "react-redux"
import { RootState } from '../store';
import { Divider } from 'antd';
import { Card } from "reactstrap";
import { Statistic } from 'semantic-ui-react'
import { Container, Row, Col } from "reactstrap";
import SportsSoccerIcon from '@material-ui/icons/SportsSoccer';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ThumbsUpDownIcon from '@material-ui/icons/ThumbsUpDown';
import ScoreIcon from '@material-ui/icons/Score';
import { SVG } from './SVG';
import { LinkProps, Route, Switch } from 'react-router-dom';
import { push } from 'connected-react-router';
import { BetRecord } from './ProfileContent';
import { BoughtTips } from './BoughtTips';
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}


function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;


  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {

  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,

  };

}

interface LinkTabProps {
  label?: string;
  href?: string;
}

function LinkTabStat(props: LinkTabProps) {
  const dispatch = useDispatch()
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        dispatch(push('/profile/stat'));
      }}
      {...props}
    />
  );
}
function LinkTabRecord(props: LinkTabProps) {
  const dispatch = useDispatch()
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        dispatch(push('/profile/record'));
      }}
      {...props}
    />
  );
}
function LinkTabTips(props: LinkTabProps) {
  const dispatch = useDispatch()
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        dispatch(push('/profile/tips'));
      }}
      {...props}
    />
  );
}

function LinkTabFollow(props: LinkTabProps) {
  const dispatch = useDispatch()
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        dispatch(push('/profile/follow'));
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function NavTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const dispatch = useDispatch()
  // const dispatch = useDispatch()
  // const userId = useSelector((state: RootState) => state.auth.user_id)
  const info = useSelector((state: RootState) => state.user)
  // const followingRecords = useSelector((state: RootState) => state.followee.followee)

  //const [compare, setCompare] = useState({
  //    username: "",
  //    no_of_game: 0,
  //    no_of_win_game: 0,
  //    no_of_game_COR: 0,
  //    no_of_win_game_COR: 0,
  //    no_of_game_CRS: 0,
  //    no_of_win_game_CRS: 0,
  //    no_of_game_HAD: 0,
  //    no_of_win_game_HAD: 0,
  //    no_of_game_HDC: 0,
  //    no_of_win_game_HDC: 0,
  //    no_of_game_HIL: 0,
  //    no_of_win_game_HIL: 0,
  //})

  const chartRef: any = React.createRef()

  const RadarData = {
    labels: ["主客和", "讓球盤", "入球大細", "波膽", "角球大細"],
    datasets: [
      {
        label: `${info.username}`,
        backgroundColor: "rgba(213, 34, 222, .2)",
        borderColor: "rgba(213, 34, 222, 1)",
        pointBackgroundColor: "rgba(213, 34, 222, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(213, 34, 222, 1)",
        data: [Math.round(info.no_of_win_game_HAD! / info.no_of_game_HAD! * 100),
        Math.round(info.no_of_win_game_HDC! / info.no_of_game_HDC! * 100),
        Math.round(info.no_of_win_game_HIL! / info.no_of_game_HIL! * 100),
        Math.round(info.no_of_win_game_CRS! / info.no_of_game_CRS! * 100),
        Math.round(info.no_of_win_game_COR! / info.no_of_game_COR! * 100)],
      }
    ]
  };

  // if (compare.username) {
  //     RadarData.datasets[1] = {
  //         label: `${compare.username}`,
  //         backgroundColor: "rgba(55, 0, 60, .2)",
  //         
  //         borderColor: "rgba(55, 0, 60, 1)",
  //         pointBackgroundColor: "rgba(55, 0, 60, 1)",
  //         poingBorderColor: "#fff",
  //         pointHoverBackgroundColor: "#fff",
  //         pointHoverBorderColor: "rgba(55, 0, 60, 1)",
  //         data: [(compare.no_of_win_game_HAD! / compare.no_of_game_HAD!) * 100,
  //         (compare.no_of_win_game_HDC! / compare.no_of_game_HDC!) * 100,
  //         (compare.no_of_win_game_HIL! / compare.no_of_game_HIL!) * 100,
  //         (compare.no_of_win_game_CRS! / compare.no_of_game_CRS!) * 100,
  //         (compare.no_of_win_game_COR! / compare.no_of_game_COR!) * 100],
  //     }
  //     // console.log(RadarData.datasets)
  // }

  const RadarOptions = {
    scale: {
      ticks: {
        min: 0,
        max: 100,
        stepSize: 25,
        showLabelBackdrop: false,
        backdropColor: "rgba(203, 197, 11, 1)"
      },
      angleLines: {
        color: "rgba(1, 1, 1, 0.3)",
        lineWidth: 1,
        display: true
      },
      gridLines: {
        color: "rgba(1, 1, 1, 0.3)",
        lineWidth: 1,
        circular: false,
        display: true
      }
    }
  };

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Container className="full-container" fluid={true}>


      <div className={classes.root}>
        <AppBar position="static" className="ProfileTab">
          <Tabs
            variant="fullWidth"
            value={value}
            onChange={handleChange}
            aria-label="profile-tab"
          >
            <LinkTabStat label="個人統計" {...a11yProps(0)} />
            <LinkTabRecord label="投注記錄"  {...a11yProps(1)} />
            <LinkTabTips label="己買貼士"  {...a11yProps(2)} />
            <LinkTabFollow label="跟隨紀錄"  {...a11yProps(3)} />
          </Tabs>
        </AppBar>

        <Switch>
          <Route path="/profile/stat">
            <TabPanel value={value} index={0}>
              <Row>
                <Col xs="12">
                  <Divider>戰神卡</Divider>
                  <Row className="profile-card">
                    <SVG />
                  </Row>

                  {/* <Divider>個人統計</Divider>
                  <Radar ref={chartRef} data={RadarData} options={RadarOptions}></Radar> */}
                  <Divider>你的數據</Divider>
                </Col>
              </Row>
              <Row>
                <Col sm="12" lg="3">
                  <Card className=" card-frame card-edit">
                    <Statistic color='red'>
                      <Statistic.Label><SportsSoccerIcon fontSize='default' />主客和</Statistic.Label>
                      <Statistic.Value>{info.no_of_game_HAD === 0 ? "0" : Math.round(info.no_of_win_game_HAD! / info.no_of_game_HAD! * 100)}% </Statistic.Value>
                      <Statistic.Label>{info.no_of_win_game_HAD}勝出/{info.no_of_game_HAD}場次</Statistic.Label>
                    </Statistic>
                  </Card>
                </Col>
                <Col sm="12" lg="3">
                  <Card className=" card-frame card-edit">
                    <Statistic color='orange'>
                      <Statistic.Label><ScoreIcon fontSize='default' />波膽</Statistic.Label>
                      <Statistic.Value>{info.no_of_game_CRS === 0 ? "0" : Math.round(info.no_of_win_game_CRS! / info.no_of_game_CRS! * 100)}% </Statistic.Value>
                      <Statistic.Label>{info.no_of_win_game_CRS}勝出/{info.no_of_game_CRS}場次</Statistic.Label>
                    </Statistic>
                  </Card>
                </Col>
                <Col sm="12" lg="3">
                  <Card className=" card-frame card-edit">
                    <Statistic color='yellow'>
                      <Statistic.Label><ThumbsUpDownIcon fontSize='default' />讓球盤</Statistic.Label>
                      <Statistic.Value>{info.no_of_game_CRS === 0 ? "0" : Math.round(info.no_of_win_game_CRS! / info.no_of_game_CRS! * 100)}% </Statistic.Value>
                      <Statistic.Label>{info.no_of_win_game_HDC}勝出/{info.no_of_game_HDC}場次</Statistic.Label>
                    </Statistic>
                  </Card>
                </Col>

                <Col lg="3" sm="12">
                  <Card className=" card-frame card-edit">
                    <Statistic color='yellow'>
                      <Statistic.Label><EqualizerIcon fontSize='default' />入球大細</Statistic.Label>
                      <Statistic.Value>{info.no_of_game_HIL === 0 ? "0" : Math.round(info.no_of_win_game_HIL! / info.no_of_game_HIL! * 100)}% </Statistic.Value>
                      <Statistic.Label>{info.no_of_win_game_HIL}勝出/{info.no_of_game_HIL}場次</Statistic.Label>
                    </Statistic>
                  </Card>
                </Col>

                <Col sm="12">
                  <Card className=" card-frame card-edit">
                    <Statistic color='yellow'>
                      <Statistic.Label><PlayArrowIcon fontSize='default' />角球大細</Statistic.Label>
                      <Statistic.Value>{info.no_of_game_COR === 0 ? "0" : Math.round(info.no_of_win_game_COR! / info.no_of_game_COR! * 100)}% </Statistic.Value>
                      <Statistic.Label>{info.no_of_win_game_COR}勝出/{info.no_of_game_COR}場次</Statistic.Label>
                      <Statistic.Label>總投注金幣：XXXXX</Statistic.Label>
                      <Statistic.Label>淨利潤/虧損：XXXXX</Statistic.Label>
                    </Statistic>
                  </Card>
                </Col>
              </Row>

              
              <Radar ref={chartRef} data={RadarData} options={RadarOptions}></Radar>

              {/* <Divider>戰神卡</Divider>
              <Row className="profile-card">
                <SVG />
              </Row> */}
              <Col>

              </Col>
            </TabPanel>
          </Route>

          <Route path="/profile/record">
            <TabPanel value={value} index={1}>
              <BetRecord />
            </TabPanel>
          </Route>
          <Route path="/profile/tips">
            <TabPanel value={value} index={2}>
              <BoughtTips />
            </TabPanel>
          </Route>

          <TabPanel value={value} index={3}>

          </TabPanel>

        </Switch>
      </div>
    </Container>
  );
}

