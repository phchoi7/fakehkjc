import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';
import '../Components/AppTopBar.css';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { Link } from 'react-router-dom';
import { logout } from '../redux/auth/action';
import { Icon } from 'semantic-ui-react'
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';
import coins from '../asset/coins.png';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      color:"white",
    },
    
  }),
);

function HomeIcon(props: SvgIconProps) {
    return (
      <SvgIcon {...props}>
        <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
      </SvgIcon>
    );
  }
  

export default function AppTopBar() {
    const classes = useStyles();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
    const dispatch = useDispatch()
    const userCoins = useSelector((state:RootState)=> state.user.coin)
    return (
      <div className="App-TopBar">
        <AppBar position="fixed">
          <Toolbar className="TopBar" >
          {isAuthenticated && 
               <Link to="/profile" className="nav-link app-bar-button"><img className="coins-img"src={coins} /> {userCoins }</Link>
            }
            <Typography variant="h6" className={classes.title}>
              貼士多
            </Typography>
            
            {!isAuthenticated && 
              <Link to="/login" className="nav-link app-bar-button" >註冊/登入</Link>
            }
           
            
            {isAuthenticated && 
              <Button onClick={() => { dispatch(logout()) }} className="nav-link app-bar-button" >登出</Button>
            }
            
                
           
            
            
          </Toolbar>
        </AppBar>
      </div>
    );
  }