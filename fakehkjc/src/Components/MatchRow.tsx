import { Match } from "../redux/match/reducer";
import { Card, CardBody } from "reactstrap";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
// import { Button } from "reactstrap";
import { push, replace } from "connected-react-router";
import React from "react";
import { OddPanel } from "./OddPanel";
import { TipsRow } from "./TipsRow";
// import homeLogo from "../Chelsea.png";
// import awayLogo from "../Luton.png";
import "moment-timezone";
import moment from "moment";
import "./MatchRow.css";
// import { RootState } from "../store";
import { CommentsRow } from "./CommentsRow";
import { weekday } from "../App";
import { Divider } from "antd";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Container, Row, Col } from "reactstrap";
// import { Box } from "grommet";
// import defaultLogo from '../Default.png'
import { Icon } from 'semantic-ui-react'

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

export function MatchRow(props: { match: Match }) {
  const routeMatch = useRouteMatch<{ id?: string }>();
  const match = props.match;
  const isOpen = (routeMatch.params.id || "") === match.jc_match_id;
  const dispatch = useDispatch();
  const classes = useStyles();
  const [value, setValue] = React.useState(0);


  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };
  return (
    <Container>
      <div className="match-row">
        <Row className="matched-row">
          <Col>
            <Card className=" card-frame card-edit">
              <CardBody>
                <div
                  className="match-details"
                  onClick={() => {
                    dispatch(
                      replace("/bet-page/match/" + props.match.jc_match_id)
                    );
                    if (isOpen === true) {
                      dispatch(replace("/bet-page/"));
                    }
                  }}
                >
                  <Row className="team-row">
                    <Col lg="3" xs="12">
                      <div className="">
                        <h5 className="fs16 mt-2 mb-1">
                          日期：
                          {moment(props.match.match_date).format("YYYY-MM-DD")}{" "}
                        </h5>
                        <h5 className="fs12 mb-2">
                          星期
                          {
                            weekday[
                            moment(props.match.match_date)
                              .format("ddd")
                              .toUpperCase()
                            ]
                          }{" "}
                        </h5>
                        <h6 className="fs12">{moment(props.match.match_date).format("YYYY-MM-DD HH:mm").slice(11)}</h6>
                        <div className="match-league">
                          {props.match.match_type}
                        </div>
                      </div>
                    </Col>
                    <Col lg="3" xs="3" className="home-team">
                      <div className="ft-right">

                        <img
                          src={props.match.home_team[props.match.home_team.length -1 ] === " " ? (`https://teamflag.tipstore.fun/${props.match.home_team}.png`).replace(" ", "") : `https://teamflag.tipstore.fun/${props.match.home_team}.png`}
                          alt="Team Flag"
                          className="HomeLogo w105"
                        />

                      </div>
                      <div className="ft-right mt-md-2 ">
                        <h6 className="home-hame">主隊</h6>

                        <h6 className="fs14 mb-0 text-black">
                          <span className="PC_only fs14">
                            <strong>{props.match.home_team}</strong>
                          </span>
                        </h6>
                      </div>
                    </Col>

                    <Col lg="1" xs="3" className="match-vs-col">
                      <div className="match-vs">V.S</div>
                    </Col>
                    <Col lg="3" xs="3" className="away-team text-md-left">
                      <div className="ft-left">

                        <img
                          src={props.match.away_team[props.match.away_team.length -1 ] === " " ? (`https://teamflag.tipstore.fun/${props.match.away_team}.png`).replace(" ", "") : `https://teamflag.tipstore.fun/${props.match.away_team}.png`}
                          alt="Team Flag"
                          className="HomeLogo w105"
                        />

                      </div>
                      <div className="ft-left mt-md-2 ">
                        <h6 className="fs12 mb-2 PC_only">客隊</h6>

                        <h6 className="fs14 mb-0 text-black">
                          <span className="PC_only fs14">
                            <strong>{props.match.away_team}</strong>
                          </span>
                        </h6>
                      </div>
                    </Col>

                    <Col className="arrow">
                      <Icon name='caret square down outline' />
                    </Col>
                  </Row>
                </div>

                {isOpen && (
                  <div className="match-content">
                    <Divider />
                    <Paper className={classes.root}>
                      <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered
                        
                      >
                        <Tab
                          label="賠率"
                          onClick={() =>
                            dispatch(
                              push("/bet-page/match/" + props.match.jc_match_id)
                            )
                          }
                        />
                        <Tab
                          label="高手tips"
                          onClick={() =>
                            dispatch(
                              push(
                                "/bet-page/match/" +
                                props.match.jc_match_id +
                                "/tips"
                              )
                            )
                          }
                        />
                        <Tab
                          label="留言區"
                          onClick={() =>
                            dispatch(
                              push(
                                "/bet-page/match/" +
                                props.match.jc_match_id +
                                "/comments"
                              )
                            )
                          }
                        />
                      </Tabs>
                      <Switch>
                        <Route
                          className="OddPanel"
                          path="/bet-page/match/:id"
                          exact
                        >
                          <OddPanel match={match} />
                        </Route>
                        <Route
                          className="TipsRow"
                          path="/bet-page/match/:id/tips"
                        >
                          <TipsRow match={match} />
                        </Route>
                        <Route
                          className="CommentsRow"
                          path="/bet-page/match/:id/comments"
                        >
                          <CommentsRow match={match} />
                        </Route>
                      </Switch>
                    </Paper>
                    {/* <div className="match-option">
                <Button color="info" onClick={() => dispatch(push('/bet-page/match/' + props.match.jc_match_id))}>賠率</Button>{' '}
                <Button color="info" onClick={() => dispatch(push('/bet-page/match/' + props.match.jc_match_id + '/tips'))}>高手tips</Button>{' '}
                <Button color="info" onClick={() => dispatch(push('/bet-page/match/' + props.match.jc_match_id + '/comments'))}>留言區</Button>{' '}
              </div> */}
                    <div></div>
                  </div>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </Container>
  );
}

export function ResultRow(props: { match: Match }) {
  // const matches = useSelector((state: RootState) => state.match.matches);
  // const [display, setDisplay] = useState(matches);

  return (
    <div className="match-row">
      <Row className="matched-row">
        <Col className="result-col">
          <Card className=" card-frame card-edit">
            <CardBody>
              <Row className="team-row">
                <Col lg="3" xs="12">
                  <div className="">
                    <h5 className="fs16 mt-2 mb-1">
                      日期：
                      {moment(props.match.match_date).format(
                      "YYYY-MM-DD"
                    )}{" "}
                    </h5>
                    <h5 className="fs12 mb-2">
                      星期
                      {
                        weekday[
                        moment(props.match.match_date)
                          .format("ddd")
                          .toUpperCase()
                        ]
                      }{" "}
                    </h5>
                    <h6 className="fs12">{moment(props.match.match_date).format("YYYY-MM-DD HH:mm").slice(11)}</h6>
                    <div className="match-league">{props.match.match_type}</div>
                  </div>
                </Col>
                <Col lg="3" xs="3" className="home-team">
                  <div className="ft-right">

                    <img
                      src={props.match.home_team[props.match.home_team.length -1 ] === " " ? (`https://teamflag.tipstore.fun/${props.match.home_team}.png`).replace(" ", "") : `https://teamflag.tipstore.fun/${props.match.home_team}.png`}
                      alt="Team Flag"
                      className="HomeLogo w105"
                    />


                  </div>
                  <div className="ft-right mt-md-2 ">
                    <h6 className="home-hame">主隊</h6>

                    <h6 className="fs14 mb-0 text-black">
                      <span className="PC_only fs14">
                        <strong>{props.match.home_team}</strong>
                      </span>
                    </h6>
                  </div>
                </Col>

                <Col lg="1" xs="3" className="match-vs-col">
                  <div className="match-vs">
                    {" "}
                    {props.match.home_full_match_score} :{" "}
                    {props.match.away_full_match_score}{" "}
                  </div>
                </Col>
                <Col lg="3" xs="3" className="away-team text-md-left">
                  <div className="ft-left">

                    <img
                      src={props.match.away_team[props.match.away_team.length -1 ] === " " ? (`https://teamflag.tipstore.fun/${props.match.away_team}.png`).replace(" ", "") : `https://teamflag.tipstore.fun/${props.match.away_team}.png`}
                      alt="Team Flag"
                      className="HomeLogo w105"
                    />


                  </div>
                  <div className="ft-left mt-md-2 ">
                    <h6 className="fs12 mb-2 PC_only">客隊</h6>

                    <h6 className="fs14 mb-0 text-black">
                      <span className="PC_only fs14">
                        <strong>{props.match.away_team}</strong>
                      </span>
                    </h6>
                  </div>
                </Col>

                <Col className="arrow">

                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
