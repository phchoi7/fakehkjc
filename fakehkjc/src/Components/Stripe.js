import React from 'react'
import { Elements } from "@stripe/react-stripe-js"
import { loadStripe } from '@stripe/stripe-js'
import "./Stripe.css"
import PaymentForm from './PaymentForm'


const PUBLIC_KEY = "pk_test_51IJZHlH76ZNxrR3AEAcMQj4weQBGXfASY0dbMNuqQjxuwwb30hCpkYcutOofoKalffVehKEMPAdboBHEq3JcXLDP00WN1pYOWD"

const stripeTestPromise = loadStripe(PUBLIC_KEY)

export default function Stripe(props) {
    return (
        <Elements stripe={stripeTestPromise}>
            <PaymentForm transaction={props.transaction} info={props.info}/>
        </Elements>
    )
};