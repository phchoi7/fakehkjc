import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../store"
import { BetRecord } from "./ProfileContent"
import { Card, CardBody, Button, Modal, ModalHeader, ModalFooter, ModalBody } from "reactstrap"
import "moment-timezone"
import moment from "moment"
import { weekday } from "../App";
import { deleteFollowee } from "../redux/follow/action"
import './ProfileContainer.css'
import { push } from "connected-react-router"
import { Route, Switch } from "react-router-dom"
import { Container, Row, Col } from "reactstrap";
import Default from "../Default.png"
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Divider } from 'antd';
import ProfileTab from "./ProfileTab"
import React, { useEffect, useState } from "react";
import { Radar } from "react-chartjs-2"
import Stripe from './Stripe.js'
import { sendTips } from "../redux/tips/action"

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& > *': {
                margin: theme.spacing(1),
            },
        },
        small: {
            width: theme.spacing(3),
            height: theme.spacing(3),
        },
        large: {
            width: theme.spacing(7),
            height: theme.spacing(7),
        },
    }),
);


// const useStyles = makeStyles((theme: Theme) =>
//   createStyles({
//     root: {
//       '& > *': {
//         margin: theme.spacing(1),
//       },
//     },
//   }),
// );

// let betMatches: [] = []

interface IfolloweeMatch {
    id: number | null,
    username: string | null,
    match_id: string | null,
    home_team: string | null,
    away_team: string | null,
    match_date: string | null,
    match_day: string | null,
    match_type: string | null
}
// let betMatches: [] = []

export function ProfileContainer(props: { user_id: number }) {
    const userId = useSelector((state: RootState) => state.auth.user_id)
    const paidTips = useSelector((state: RootState) => state.paidTips.paidTips)
    const dispatch = useDispatch()
    const [modalPurchase, setModalPurchase] = useState(false);
    const togglePurchase = () => setModalPurchase(!modalPurchase);
    const info = useSelector((state: RootState) => state.user)
    const [buyTips, setBuyTips] = useState(false)
    const transaction = () => setBuyTips(true)


    // const userId = useSelector((state: RootState) => state.auth.user_id)
    // const coin = useSelector((state: RootState) => state.user.coin)
    // const paidTips = useSelector((state: RootState) => state.paidTips.paidTips)

    const followingRecords = useSelector((state: RootState) => state.followee.followee)
    const betRecordLength = useSelector((state: RootState) => state.user_bet_record)
    // const classes = useStyles();

    const [followedBy, setFollowedBy] = useState(0)

    const [followeeMatch, setFolloweeMatch] = useState<IfolloweeMatch[]>()
    const [compare, setCompare] = useState({
        username: "",
        no_of_game: 0,
        no_of_win_game: 0,
        no_of_game_COR: 0,
        no_of_win_game_COR: 0,
        no_of_game_CRS: 0,
        no_of_win_game_CRS: 0,
        no_of_game_HAD: 0,
        no_of_win_game_HAD: 0,
        no_of_game_HDC: 0,
        no_of_win_game_HDC: 0,
        no_of_game_HIL: 0,
        no_of_win_game_HIL: 0,
    })

    const chartRef: any = React.createRef()

    const RadarData = {
        labels: ["主客和", "讓球盤", "入球大細", "波膽", "角球大細"],
        datasets: [
            {
                label: `${info.username}`,
                backgroundColor: "rgba(213, 34, 222, .2)",
                borderColor: "rgba(213, 34, 222, 1)",
                pointBackgroundColor: "rgba(213, 34, 222, 1)",
                poingBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "rgba(213, 34, 222, 1)",
                data: [Math.round(info.no_of_win_game_HAD! / info.no_of_game_HAD! * 100),
                Math.round(info.no_of_win_game_HDC! / info.no_of_game_HDC! * 100),
                Math.round(info.no_of_win_game_HIL! / info.no_of_game_HIL! * 100),
                Math.round(info.no_of_win_game_CRS! / info.no_of_game_CRS! * 100),
                Math.round(info.no_of_win_game_COR! / info.no_of_game_COR! * 100)],
            }
        ]
    };

    if (compare.username) {
        RadarData.datasets[1] = {
            label: `${compare.username}`,
            backgroundColor: "rgba(55, 0, 60, .2)",

            borderColor: "rgba(55, 0, 60, 1)",
            pointBackgroundColor: "rgba(55, 0, 60, 1)",
            poingBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(55, 0, 60, 1)",
            data: [(compare.no_of_win_game_HAD! / compare.no_of_game_HAD!) * 100,
            (compare.no_of_win_game_HDC! / compare.no_of_game_HDC!) * 100,
            (compare.no_of_win_game_HIL! / compare.no_of_game_HIL!) * 100,
            (compare.no_of_win_game_CRS! / compare.no_of_game_CRS!) * 100,
            (compare.no_of_win_game_COR! / compare.no_of_game_COR!) * 100],
        }
        // console.log(RadarData.datasets)
    }
    const RadarOptions = {
        scale: {
            ticks: {
                min: 0,
                max: 100,
                stepSize: 25,
                showLabelBackdrop: false,
                backdropColor: "rgba(203, 197, 11, 1)"
            },
            angleLines: {
                color: "rgba(1, 1, 1, 0.3)",
                lineWidth: 1,
                display: true
            },
            gridLines: {
                color: "rgba(1, 1, 1, 0.3)",
                lineWidth: 1,
                circular: false,
                display: true
            }
        }
    };

    useEffect(() => {
        async function loadFollowing(id: number) {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/following/${id}`, {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
            })
            const json = await res.json()
            console.log(json)
            if (json.result) {
                setFollowedBy(0)
            } else {
                setFollowedBy(json[0].count)
            }
        }
        console.log(info.id)
        loadFollowing(info.id)
    }, [info.id])

    return (
        <Container >
            <div>
                <Row>
                    <Col className="profile-col-12" lg="12">
                        <Card className=" card-frame card-edit">
                            <CardBody>
                                <Row className="user-row">
                                    <Col sm="6">
                                        <div className="ft-left mr-3">
                                            <img src={Default} alt={"username"} className=" w105-2" />
                                        </div>
                                        <div className="media-body">
                                            <h4 className="">用戶編號：{info.id}</h4>
                                            <div className="">用戶名：{info.username}</div>
                                            <div className="">金幣：{info.coin}</div>
                                        </div>
                                    </Col>
                                    <Col sm="6">
                                        {/* <Button variant="outlined" color="primary">
                                            Follow
                                        </Button> */}
                                    </Col>
                                    <Divider>個人檔案</Divider>
                                    <div className="user-panel">
                                        <div className="following">
                                            <div>{followingRecords.length}</div>
                                            關注中
                                        </div>
                                        <Divider type="vertical" />
                                        <div className="follower">
                                            <div>{followedBy}</div>
                                            粉絲
                                        </div>
                                        <Divider type="vertical" />
                                        <div className="new-bet">
                                            <div>{betRecordLength.bets? betRecordLength.bets.length : 0}</div>
                                            注數
                                        </div>
                                    </div>
                                </Row>
                            </CardBody>
                        </Card>
                        <ProfileTab />
                    </Col>
                </Row>
                {/* <div className="user-content">
                    <button onClick={() => dispatch(push('/profile/record'))}>投注紀錄</button>
                    <button onClick={() => dispatch(push('/profile/tips'))}>已買貼士</button>
                    <button onClick={() => dispatch(push('/profile/follow'))}>跟隨紀錄</button>
                </div> */}
                <div>
                    {/* all of the jsx elements below are in the ProfileContent.tsx*/}
                    <Switch>

                        <Route path="/profile/follow">

                            <div>
                                <Divider>你的數據</Divider>
                                <Radar ref={chartRef} data={RadarData} options={RadarOptions}></Radar>
                                {followingRecords && followingRecords.map(followee => {
                                    return (
                                        <div
                                            className={"name"}
                                            key={followingRecords.indexOf(followee)}>
                                            <Card>
                                                <CardBody>
                                                    <div>
                                                        {followee.username}
                                                    </div>
                                                    <div>
                                                        {new Intl.NumberFormat().format(followee.coin!)} 金幣
                                                    </div>
                                                    <div>
                                                        總投注賽事：{followee.no_of_game} 場
                                                    </div>
                                                    <div>
                                                        勝出投注：{followee.no_of_win_game} 次
                                                    </div>
                                                    <div>
                                                        勝出主客和投注：{followee.no_of_game_HAD} 次
                                                    </div>
                                                    <div>
                                                        勝出讓球盤投注：{followee.no_of_game_HDC} 次
                                                    </div>
                                                    <div>
                                                        勝出入球大細投注：{followee.no_of_game_CRS} 次
                                                    </div>
                                                    <div>
                                                        勝出角球大細投注：{followee.no_of_win_game_COR} 次
                                                    </div>
                                                    <div>
                                                        勝出波膽投注：{followee.no_of_game_HIL} 次
                                                    </div>
                                                    <button onClick={async () => {
                                                        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/bet/${followee.id}`)
                                                        const json = await res.json()
                                                        console.log(json)
                                                        if (json === "暫無投注紀錄。") {
                                                            setFolloweeMatch([
                                                                {
                                                                    id: followee.id,
                                                                    username: followee.username,
                                                                    match_id: null,
                                                                    home_team: null,
                                                                    away_team: null,
                                                                    match_date: null,
                                                                    match_day: null,
                                                                    match_type: null

                                                                }
                                                            ])
                                                            console.log(followeeMatch)
                                                        } else {
                                                            let matchArray: IfolloweeMatch[] = []
                                                            for (let duplicateMatch of json) {
                                                                if (!matchArray.find(insertMatch => insertMatch.match_id === duplicateMatch.match_id)) {
                                                                    matchArray.push(duplicateMatch)
                                                                }
                                                            }
                                                            setFolloweeMatch(matchArray)
                                                            console.log(matchArray)
                                                        }
                                                    }}>查看投注紀錄
                                                    </button>

                                                    {followeeMatch && followeeMatch.find(betMatch => betMatch.username === followee.username)
                                                        && followeeMatch.find(betMatch => betMatch.match_date !== null)
                                                        && followeeMatch.map(betMatch => (
                                                            <div>
                                                                <Card>
                                                                    <CardBody>
                                                                        <div>{betMatch.match_type}</div>
                                                                        <div>{betMatch.match_day}</div>
                                                                    星期{
                                                                            weekday[
                                                                            moment(betMatch.match_date)
                                                                                .format("ddd")
                                                                                .toUpperCase()
                                                                            ]
                                                                        }{" "}
                                                                        <div>
                                                                            {betMatch.home_team} 對 {betMatch.away_team}
                                                                        </div>
                                                                        {!buyTips &&
                                                                            <Button onClick={() => {
                                                                                togglePurchase()
                                                                            }
                                                                            }>買貼士</Button>
                                                                        }
                                                                    </CardBody>
                                                                </Card>

                                                                {paidTips.find(paidTip => paidTip.username === betMatch.username)
                                                                    &&
                                                                    paidTips.find(paidTip => paidTip.match_id === betMatch.match_id)
                                                                    &&
                                                                    paidTips.map(paidTip =>
                                                                        <List component="nav" className={"tip-mobile-content"} aria-label="mailbox folders">
                                                                            <ListItem button>
                                                                                <ThumbUpIcon />
                                                                                <ListItemText primary={paidTip.bet_type} />
                                                                                <ListItemText primary={paidTip.bet_type_content} />

                                                                                <ListItemText className="tips-choose" primary={'選擇：' + paidTip.bet_content} />
                                                                                <ListItemText primary={'賠率：' + paidTip.odds} />
                                                                            </ListItem>
                                                                            <Divider />
                                                                        </List>

                                                                    )
                                                                }

                                                                <Modal isOpen={modalPurchase} toggle={togglePurchase} backdrop={false}>
                                                                    <ModalHeader>購買{followee.username}的貼士</ModalHeader>
                                                                    <Stripe transaction={transaction} />
                                                                    <ModalFooter>
                                                                        <Button onClick={() => {
                                                                            if (buyTips) {
                                                                                dispatch(sendTips({
                                                                                    customer_id: userId,
                                                                                    tips_provider: followee.username,
                                                                                    match_id: betMatch.match_id

                                                                                }))
                                                                                togglePurchase()
                                                                            } else {
                                                                                togglePurchase()
                                                                            }
                                                                        }}>確定</Button>
                                                                    </ModalFooter>
                                                                </Modal>
                                                            </div>
                                                        )
                                                        )
                                                    }


                                                    {followeeMatch && followeeMatch.find(betMatch => betMatch.username === followee.username)
                                                        && followeeMatch.find(betMatch => betMatch.match_date == null) &&
                                                        <div>暫無投注紀錄。</div>}

                                                    <button onClick={() =>
                                                        dispatch(deleteFollowee(
                                                            info.id, followee.username
                                                        ))}>
                                                        取消跟蹤
                                                    </button>
                                                    <button onClick={() => {
                                                        setCompare({
                                                            username: followee.username!,
                                                            no_of_game: followee.no_of_game!,
                                                            no_of_win_game: followee.no_of_win_game!,
                                                            no_of_game_COR: followee.no_of_game_COR!,
                                                            no_of_win_game_COR: followee.no_of_win_game_COR!,
                                                            no_of_game_CRS: followee.no_of_game_CRS!,
                                                            no_of_win_game_CRS: followee.no_of_win_game_CRS!,
                                                            no_of_game_HAD: followee.no_of_game_HAD!,
                                                            no_of_win_game_HAD: followee.no_of_win_game_HAD!,
                                                            no_of_game_HDC: followee.no_of_game_HDC!,
                                                            no_of_win_game_HDC: followee.no_of_win_game_HDC!,
                                                            no_of_game_HIL: followee.no_of_game_HIL!,
                                                            no_of_win_game_HIL: followee.no_of_win_game_HIL!,
                                                        })
                                                    }}>比較
                                                    </button>
                                                </CardBody>
                                            </Card>
                                        </div>
                                    )
                                })}
                            </div>
                        </Route>

                    </Switch>
                </div>
            </div>
        </Container>
    )
}




// export function ProfileContainer(props: { user_id: number }) {
//     const dispatch = useDispatch()
//     const info = useSelector((state: RootState) => state.user)
//     const followingRecords = useSelector((state: RootState) => state.followee.followee)

//     const [compare, setCompare] = useState({
//         username: "",
//         no_of_game: 0,
//         no_of_win_game_COR: 0,
//         no_of_win_game_CRS: 0,
//         no_of_win_game_HAD: 0,
//         no_of_win_game_HDC: 0,
//         no_of_win_game_HIL: 0,
//     })

//     const data = [
//         {
//             subject: '總勝率',
//             A: (info.no_of_win_game_COR! +
//                 info.no_of_win_game_CRS! +
//                 info.no_of_win_game_HAD! +
//                 info.no_of_win_game_HDC! +
//                 info.no_of_win_game_HIL!) / (info.no_of_game!) * 100,
//             B: (compare.no_of_win_game_COR! +
//                 compare.no_of_win_game_CRS! +
//                 compare.no_of_win_game_HAD! +
//                 compare.no_of_win_game_HDC! +
//                 compare.no_of_win_game_HIL!) / (compare.no_of_game!) * 100,
//             fullMark: 100,
//         },
//         {
//             subject: '主客和',
//             A: (info.no_of_win_game_HAD! / info.no_of_game!) * 100,
//             B: (compare.no_of_win_game_HAD! / compare.no_of_game!) * 100,
//             fullMark: 100,
//         },
//         {
//             subject: '讓球盤',
//             A: (info.no_of_win_game_HDC! / info.no_of_game!) * 100,
//             B: (compare.no_of_win_game_HAD! / info.no_of_game!) * 100,
//             fullMark: 100,
//         },
//         {
//             subject: '入球大細',
//             A: (info.no_of_win_game_HIL! / info.no_of_game!) * 100,
//             B: (compare.no_of_win_game_HAD! / compare.no_of_game!) * 100,
//             fullMark: 100,
//         },
//         {
//             subject: '波膽',
//             A: (info.no_of_win_game_CRS! / info.no_of_game!) * 100,
//             B: (compare.no_of_win_game_HAD! / compare.no_of_game!) * 100,
//             fullMark: 100,
//         },
//         {
//             subject: '角球大細',
//             A: (info.no_of_win_game_COR! / info.no_of_game!) * 100,
//             B: (compare.no_of_win_game_HAD! / compare.no_of_game!) * 100,
//             fullMark: 100,
//         },
//     ];

//     return (
//         <div>
//             <div className="user-intro">
//                 <SVG />
//                 <RadarChart cx={200} cy={150} outerRadius={100} width={400} height={300} data={data}>
//                     <PolarGrid gridType='circle' />
//                     <PolarAngleAxis dataKey="subject" />
//                     <PolarRadiusAxis angle={30} domain={[0, 100]} />
//                     <Radar name={info.username!} dataKey="A" stroke="#8884d8" fill="#8884d8" fillOpacity={0.4} />
//                     {compare.username && <Radar name={compare.username!} dataKey="B" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.4} />}
//                     <Legend />
//                 </RadarChart>
//             </div>


//             <table className="user-detail-table">
//                 <thead className="user-detail-table-row">
//                     <tr>
//                         <th colSpan={3}>個人統計</th>
//                     </tr>
//                 </thead>
//                 <tbody>
//                     <tr>
//                         <td>投注總類</td>
//                         <td>勝數/注數</td>
//                         <td>勝率</td>
//                     </tr>
//                     <tr>
//                         <td>主客和：</td>
//                         <td>{info.no_of_win_game_HAD}/待加</td>
//                         <td>X%</td>
//                     </tr>
//                     <tr>
//                         <td>波膽：</td>
//                         <td>{info.no_of_win_game_CRS}/待加</td>
//                         <td>X%</td>
//                     </tr>
//                     <tr>
//                         <td>讓球盤：</td>
//                         <td>{info.no_of_win_game_HDC}/待加</td>
//                         <td>X%</td>
//                     </tr>
//                     <tr>
//                         <td>入球大細：</td>
//                         <td>{info.no_of_win_game_HIL}/待加</td>
//                         <td>X%</td>
//                     </tr>
//                     <tr>
//                         <td>角球大細：</td>
//                         <td>{info.no_of_win_game_COR}/待加</td>
//                         <td>X%</td>
//                     </tr>
//                 </tbody>
//                 <thead>
//                     <tr>
//                         <th colSpan={3}>總投注金幣：XXXXX</th>
//                     </tr>
//                 </thead>
//                 <thead>
//                     <tr>
//                         <th colSpan={3}>淨利潤/虧損：XXXXX</th>
//                     </tr>
//                 </thead>
//             </table>


//             {/*<Pentagon2 />*/}


//             <div className="user-content">
//                 <button onClick={() => dispatch(push('/profile/record'))}>投注紀錄</button>
//                 <button onClick={() => dispatch(push('/profile/tips'))}>已買貼士</button>
//                 <button onClick={() => dispatch(push('/profile/follow'))}>跟隨紀錄</button>
//             </div>
//             <div>
//                 {/* all of the jsx elements below are in the ProfileContent.tsx*/}

//                 <Switch>
//                     <Route path="/profile/record">
//                         <BetRecord />
//                     </Route>
//                     <Route path="/profile/tips">
//                         <BoughtTips />
//                     </Route>
//                     <Route path="/profile/follow">
//                         <div>
//                             {followingRecords && followingRecords.map(followee => {
//                                 return (
//                                     <div
//                                         className={"name"}
//                                         key={followingRecords.indexOf(followee)}
//                                         onClick={() => {
//                                             setCompare({
//                                                 username: followee.username!,
//                                                 no_of_game: followee.no_of_game!,
//                                                 no_of_win_game_COR: followee.no_of_win_game_COR!,
//                                                 no_of_win_game_CRS: followee.no_of_win_game_CRS!,
//                                                 no_of_win_game_HAD: followee.no_of_win_game_HAD!,
//                                                 no_of_win_game_HDC: followee.no_of_win_game_HDC!,
//                                                 no_of_win_game_HIL: followee.no_of_win_game_HIL!,
//                                             })
//                                         }}>
//                                         <div>
//                                             {followee.username}
//                                         </div>
//                                         <div>
//                                             {new Intl.NumberFormat().format(followee.coin!)} 金幣
//                                         </div>
//                                         <button>
//                                             取消跟蹤
//                                         </button>
//                                     </div>
//                                 )
//                             })}
//                         </div>
//                     </Route>
//                 </Switch>
//             </div>
//         </div>
//     )
// }

