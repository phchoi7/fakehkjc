import { useSelector } from 'react-redux'
import { RootState } from '../store'
import './SVG.scss'
// import Testing from "../testing.jpeg"
// import Chelsea from "../Chelsea.png"
import Default from "../Default.png"


export function SVG() {
    const info = useSelector((state: RootState) => state.user)

    return (
        <div>
            <div className={"fut-player-card"}>
                <div className={"player-card-top"}>
                    <div className={"player-master-info"}>
                        <div className={"player-rating"}>
                            <span>
                                {Math.round(info.no_of_win_game! / info.no_of_game! * 100)}%
                            </span>
                        </div>
                        <span className={"player-position"}>
                            <span>總勝率</span>
                        </span>
                        {/* <div className={"player-nation"}>
                            <img src="https://selimdoyranli.com/cdn/fut-player-card/img/argentina.svg" alt="Argentina" />
                        </div>
                        <div className={"player-club"}>
                            <img src="https://selimdoyranli.com/cdn/fut-player-card/img/barcelona.svg" alt="Barcelona" />
                        </div> */}
                    </div>
                    <div className={"player-picture"}>
                        <img src={Default} alt={"username"} />
                        {/* <div className={"player-extra"}>
                            <span>4*SM</span>
                            <span>4*WF</span>
                        </div> */}
                    </div>
                </div>
                <div className={"player-card-bottom"}>
                    <div className={"player-info"}>
                        <div className={"player-name"}>
                            <span>{info.username}</span>
                        </div>
                        <div className={"player-features"}>
                            <div className={"player-features-col"}>
                                <span>
                                    <div className={"player-feature-value"}>
                                        {info.no_of_win_game_HAD}
                                    </div>
                                    <div className={"player-feature-title"}>
                                        主客和
                                    </div>
                                </span>
                                <span>
                                    <div className={"player-feature-value"}>
                                        {info.no_of_win_game_HDC}
                                    </div>
                                    <div className={"player-feature-title"}>
                                        讓球盤
                                    </div>
                                </span>
                                <span>
                                    <div className={"player-feature-value"}>
                                        {info.no_of_win_game_CRS}
                                    </div>
                                    <div className={"player-feature-title"}>
                                        波膽
                                    </div>
                                </span>
                            </div>
                            <div className={"player-features-col"}>
                                <span>
                                    <div className={"player-feature-value"}>
                                        {info.no_of_win_game_HIL}
                                    </div>
                                    <div className={"player-feature-title"}>
                                        入球大細
                                    </div>
                                </span>
                                <span>
                                    <div className={"player-feature-value"}>
                                        {info.no_of_win_game_COR}
                                    </div>
                                    <div className={"player-feature-title"}>
                                        角球大細
                                    </div>
                                </span>
                                {/*<span>
                                    <div className={"player-feature-value"}>
                                        68
                                    </div>
                                    <div className={"player-feature-title"}>
                                        PHY
                                    </div>
                                </span>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
