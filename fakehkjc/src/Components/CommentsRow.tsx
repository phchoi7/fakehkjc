
// import { Card, CardBody } from "reactstrap"
import '../Components/CommentsRow.css';
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../store"

// import {useRouteMatch} from "react-router-dom"
import { addMessage } from "../redux/comments/action";

import Button from '@material-ui/core/Button';
import { Match } from "../redux/match/reducer"
import { Modal, ModalBody, ModalFooter, ModalHeader, Toast, ToastBody, ToastHeader } from 'reactstrap';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import "moment-timezone"
// import moment from "moment"
import { Divider } from 'antd';
import { Box } from 'grommet';
import { Container, Row, Col } from "reactstrap";
import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& > *': {

            },
        },
    }),
);

export function CommentsRow(props: { match: Match }) {
    const matchId = props.match.jc_match_id
    // const match = useRouteMatch<{id:string}>();
    const [commentsProvider, setCommentsProvider] = useState<{
        matchid: string;
        username: string | null;
        content: string;
        created_at: string;
    }[]>([])
    // const [commentWord, setcommentWord] = useState('')
    const dispatch = useDispatch();
    const userID = useSelector((state: RootState) => state.auth.user_id)
    const userName = useSelector((state: RootState) => state.user.username)
    // const CommentsmatchId = useSelector((state:RootState)=> state.comment.comments)
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);


    useEffect(() => {
        async function loadComments(matchId: string) {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/comments/${matchId}`)
            console.log(res)
            const json = await res.json()
            console.log(json)
            setCommentsProvider(json)
        }
        loadComments(matchId)
    }, [matchId])

    useEffect(() => {
        console.log('123')
        console.log(commentsProvider)
    }, [commentsProvider])

    const classes = useStyles();

    // if (chatroom == null) {
    //     return <div>這場賽事沒有留言</div>
    // }
    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <div className="CommentsRow">
                            <h3>留言區</h3>
                            <Divider />
                            {commentsProvider.map(message =>
                                <div key={commentsProvider.indexOf(message)}>
                                    <Toast className="message-card">
                                        <ToastHeader icon="primary" >
                                            <strong className="mr-auto">{message.username}</strong>
                                            <small>{message.created_at}</small>
                                        </ToastHeader>
                                        <ToastBody>
                                            {message.content}
                                        </ToastBody>
                                    </Toast>
                                </div>
                            )}
                            <Divider />
                            <div>
                                <form className={classes.root} noValidate autoComplete="off" onSubmit={
                                    event => {
                                        event.preventDefault();
                                        if (userID) {
                                            dispatch(addMessage(
                                                matchId,
                                                userID,
                                                event.currentTarget.content.value
                                            ))
                                            setCommentsProvider(
                                                commentsProvider.concat([{
                                                    matchid: matchId,
                                                    username: userName,
                                                    content: event.currentTarget.content.value,
                                                    created_at: event.currentTarget.created_at
                                                }])
                                            )
                                        } else {
                                            toggle()
                                        }
                                    }
                                }>
                                    <Row>
                                        <Col >
                                            <div className="text-button">
                                                <Box align="center" pad="medium">
                                                    <TextField className={classes.root + 'text'} id="outlined-required" label="write your comments" variant="outlined" name="content" placeholder="write your comments" required />
                                                </Box>
                                            </div>
                                        </Col>
                                    </Row>


                                    <Box align="center" pad="medium">
                                        <Button className={classes.root + 'button'} variant="contained" color="primary" type="submit" value="submit ">
                                            提交留言
                                    </Button>
                                    </Box>
                                </form>
                            </div>


                        </div>
                    </Col>
                </Row>
            </Container>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader>請先登入</ModalHeader>
                <ModalBody>
                    登入咗先留到言呀~未有Account？去呢度<Link to="/login">註冊</Link>啦！
                                        </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>
                        確定
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}