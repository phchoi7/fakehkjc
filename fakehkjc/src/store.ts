import { connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { createBrowserHistory } from "history";
import { MatchState, matchReducer } from "./redux/match/reducer";
import { oddReducer, OddState } from "./redux/odd/reducer";
// import { TipsState } from "./redux/tips/reducer";
import { cartReducer, CartState } from "./redux/cart/reducer";
import thunk from 'redux-thunk'
import { authReducer, AuthState } from "./redux/auth/reducer";
import { commentReducer, CommentState} from "./redux/comments/reducer";
import { paidTipState, paidTipsReducer } from "./redux/tips/reducer";
import { BetRecordState, UserBetReducer, UserReducer, UserState } from "./redux/user/reducer";

import { FolloweeReducer, FolloweeState } from "./redux/follow/reducer";
import { matchResultReducer, MatchResultState } from "./redux/match-result/reducer";

export const history = createBrowserHistory()
export interface RootState {
    router: RouterState
    match: MatchState
    odd: OddState
    paidTips: paidTipState
    cart: CartState
    auth: AuthState
    comment:CommentState
    user: UserState
    user_bet_record: BetRecordState
    followee: FolloweeState
    matchResult: MatchResultState
}

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const rootReducer = combineReducers({
    router: connectRouter(history), 
    match: matchReducer,
    odd: oddReducer,
    cart: cartReducer,
    auth: authReducer,
    comment: commentReducer,
    paidTips: paidTipsReducer,
    user: UserReducer,
    user_bet_record: UserBetReducer,
    followee: FolloweeReducer,
    matchResult: matchResultReducer
})


export const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
))

