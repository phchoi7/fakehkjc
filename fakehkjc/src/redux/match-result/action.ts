import { Dispatch } from "redux"
import { Match } from "../match/reducer"

export function loadMatchesForResult(matches: Match[]) {
    return {
        type: "@@match/LOAD_MATCHES_FOR_RESULT" as "@@match/LOAD_MATCHES_FOR_RESULT",
        matches
    }
}

export function fetchMatchesResult() {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/matches-result`)
        const json = await res.json()

        dispatch(loadMatchesForResult(json.map((match: any) => ({
            ...match,
        }))))
    }
}


export type MatchResultActions = ReturnType<typeof loadMatchesForResult>