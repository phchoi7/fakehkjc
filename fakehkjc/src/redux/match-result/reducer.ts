import { Match } from "../match/reducer";
import { MatchResultActions } from "./action";

export interface MatchResultState {
    matchesResult: Match[]
}

const initialState: MatchResultState = {
    matchesResult: []
}

export const matchResultReducer = (state: MatchResultState = initialState, action: MatchResultActions) => {
    if (action.type === "@@match/LOAD_MATCHES_FOR_RESULT") {
        return {
            ...state,
            matchesResult: action.matches
        }
    }
    return state
}