enum Bet_content {
    home,
    draw,
    away,
    big,
    small
}

enum Bet_result_type {
    win,
    draw,
    lose
}
export interface Bet {
    spent_coin: number,
    gain_coin: number,
    bet_type: string,
    bet_type_content: string,
    bet_content: Bet_content
    odds: number,
    bet_result_type: Bet_result_type
}