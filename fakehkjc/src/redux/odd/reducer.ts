
export const oddTypeMap:any = {
    had_h_odds:'主勝',
    had_d_odds:'和',
    had_a_odds:'客勝',
    hdc_h_odds:'讓球主勝',
    hdc_a_odds: '讓球客勝', 
    crs_odds_s0100: '波膽  1：0',
    crs_odds_s0200: '波膽  2：0',
    crs_odds_s0201: '波膽  2：1',
    crs_odds_s0300: '波膽  3：0',
    crs_odds_s0301: '波膽  3：1',
    crs_odds_s0302: '波膽  3：2',
    crs_odds_s0400: '波膽  4：0',
    crs_odds_s0401: '波膽  4：1',
    crs_odds_s0402: '波膽  4：2',
    crs_odds_s0500: '波膽  5：0',
    crs_odds_s0501: '波膽  5：1',
    crs_odds_s0502: '波膽  5：2',
    crs_odds_sm1mh: '波膽： 主其他',
    crs_odds_s0000: '波膽  0：0',
    crs_odds_s0101: '波膽  1：1',
    crs_odds_s0202: '波膽  2：2',
    crs_odds_s0303: '波膽  3：3',
    crs_odds_sm1md: '波膽： 和其他',
    crs_odds_s0001: '波膽  0：1',
    crs_odds_s0002: '波膽  0：2',
    crs_odds_s0102: '波膽  1：2',
    crs_odds_s0003: '波膽  0：3',
    crs_odds_s0103: '波膽  1：3',
    crs_odds_s0203: '波膽  2：3',
    crs_odds_s0004: '波膽  0：4',
    crs_odds_s0104: '波膽  1：4',
    crs_odds_s0204: '波膽  2：4',
    crs_odds_s0005: '波膽  0：5',
    crs_odds_s0105: '波膽  1：5',
    crs_odds_s0205: '波膽  2：5',
    crs_odds_sm1ma: '波膽： 客其他',
    hil_odds_l: '入球細',
    hil_odds_h: '入球大',
    chl_odds_l: '角球細',
    chl_odds_h: '角球大'
}
export interface Odd {
    homeWin:number,
    draw:number,
    awayWin:number
}
export interface OddState {
    odds:{
        [T:string]:Odd
    }
}

const initialState:OddState = {
    odds:{
        1:{
            homeWin:1.6,
            draw:4,
            awayWin:3
        },
        2:{
            homeWin:1.6,
            draw:2,
            awayWin:2
        },
        3:{
            homeWin:2.6,
            draw:4,
            awayWin:2
        },

    }
}

export const oddReducer = (state:OddState = initialState, action:any) => {
    return state
}