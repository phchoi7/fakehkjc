import { Dispatch } from "react";
import { Comment } from "./reducer";
export function addedMessage(matchId:string ,userID:number | null , content:string){
    return {
        type:'@@comment/ADD_MESSAGE' as '@@comment/ADD_MESSAGE',
        matchId,
        userID,
        content
    }
}
export function loadComments(comments: Comment[]) {
    return {
      type: '@@comment/LOAD_COMMENTS' as '@@comment/LOAD_COMMENTS',
      comments
    }
  }

export function fetchComments(matchId: string) {
    return async (dispatch: Dispatch<any>) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/comments/${matchId}`)
      const json = await res.json();
      console.log("the fetch comments json is " + json)
      
      dispatch(loadComments(json))
    }
  }

  export function addMessage(matchId:string ,userID:number | null , content:string) {
    return async (dispatch: Dispatch<any>) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/comments/${matchId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          matchId,userID, content
        })
      })
      const json = await res.json();
  
      if (json.result === 'ok') {
        dispatch(addedMessage(matchId, userID, content))
      }
  
    }
  }

export type AddMessageAction = ReturnType<typeof addedMessage>
export type LoadCommentsAction = ReturnType<typeof loadComments>


export type CommentsActions = AddMessageAction | LoadCommentsAction 