
import { CommentsActions } from "./action";
import produce from 'immer';

export interface Message {
    userID:number | null;
    content: string;
}
export interface Comment {
    matchId:string;
    message:Message[]
}

export interface CommentState {
    comments: Comment[];
}


const initialState:CommentState ={
    comments:[

    ]
}

export const commentReducer = (state:CommentState = initialState, action: CommentsActions):CommentState => {
    if (action.type === '@@comment/ADD_MESSAGE'){
        return produce(state,draftState => {
            let theComment = state.comments.find (c =>c.matchId === action.matchId)
            theComment?.message.push({
                userID:action.userID, content:action.content
            })
        })
        // let theComment = state.comments.find (c =>c.matchId === action.matchId)
        // if (theComment == null) {
        //     return state;
        // }
        // theComment ={
        //     ...theComment,
        //     message: theComment.message.concat([
        //         {
        //             username:action.username, content:action.content
        //         }
        //     ])
        // } 
        // return {
        //     ...state,
        //     comments: state.comments.map(comment => {
        //         if (comment.matchId === action.matchId && theComment != null){
        //             return theComment
        //         } else {
        //             return  comment
        //         }
        //     })
        // }
    } else if (action.type === '@@comment/LOAD_COMMENTS') {
        return {
          ...state,
          comments: action.comments
        }
      }
    
    return state;
}