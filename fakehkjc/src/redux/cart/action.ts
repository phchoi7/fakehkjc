import { Dispatch } from "redux"
import { RootState } from "../../store"
import { DraftBet } from "./reducer"

export function addDraftBet(draftBet: DraftBet) {
  return {
    type: '@@cart/ADD_DRAFT_BET' as '@@cart/ADD_DRAFT_BET',
    draftBet
  }
}

export function insertDraftBet(draft: DraftBet) {
  return async (dispatch: Dispatch<any>, getState: () => RootState) => {
    console.log(draft)
    await fetch(`${process.env.REACT_APP_BACKEND_URL}/bet-area/cart/${draft.end_user_id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": "Bearer " + getState().auth.token
      },
      body: JSON.stringify(draft)
    })
    dispatch(addDraftBet(draft))

  }
}

export function loadAllDraftBets(bets: DraftBet[]) {
  return {
    type: '@@cart/LOAD_ALL_DRAFT_BET' as '@@cart/LOAD_ALL_DRAFT_BET',
    bets
  }
}

export function selectAllDraftBets(user_id: number) {
  console.log("draft bet")
  return async (dispatch: Dispatch<any>) => {
    const result = await fetch(`${process.env.REACT_APP_BACKEND_URL}/bet-area/${user_id}`, {
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
    })
    const json = await result.json()
    if (json.result) {
      dispatch(loadAllDraftBets([]))
    } else {
      dispatch(loadAllDraftBets(json.map((record: DraftBet) => ({
        ...record
      }))))
    }

  }
}

export function removeDraftBet(betId: number) {
  return {
    type: '@@cart/REMOVE_DRAFT_BET' as '@@cart/REMOVE_DRAFT_BET',
    betId,
  }
}

export function deleteDraftBet(betId: number, userId: number) {
  return async (dispatch: Dispatch<any>) => {
    const body = {
      betId,
      userId
    }
    await fetch(`${process.env.REACT_APP_BACKEND_URL}/bet-area/cart/remove`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(body)
    })

    dispatch(removeDraftBet(betId))

  }
}

// update draft bet, 未有用住
// export function editDraftBet(betId: number) {
//   return {
//     type: "@@cart/EDIT_DRAFT_BET" as "@@cart/EDIT_DRAFT_BET",
//     betId
//   }
// }
// 
// export function updateDraftBet(betId: number, odds: number) {
//   return async (dispatch: Dispatch<any>) => {
//     await fetch(`${process.env.REACT_APP_BACKEND_URL}/bet-area/cart/edit/${betId}`, {
//       method: "PUT", 
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify(odds)
//     })
//   }
// }


// export function addDraftBet(draftBet: DraftBet) {
//   return {
//     type: '@@cart/ADD_DRAFT_BET' as '@@cart/ADD_DRAFT_BET',
//     draftBet
//   }
// }

// export function insertDraftBet(draft: DraftBet) {
//   return async (dispatch: Dispatch<any>) => {
//     console.log(draft)
//     await fetch(`${process.env.REACT_APP_BACKEND_URL}/bet-area/cart/${draft.end_user_id}`, {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify(draft)
//     })
//     // console.log(await res.json())
//     // console.log(draft)
//     dispatch(addDraftBet(draft))

//   }
// }

export type CartActions = ReturnType<typeof addDraftBet> | 
                          ReturnType<typeof removeDraftBet> | 
                          ReturnType<typeof loadAllDraftBets> 
                          //ReturnType<typeof editDraftBet> 
                          