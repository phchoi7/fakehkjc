import { AuthActions } from "../auth/action";
import { CartActions } from "./action";

export interface DraftBet {
    id: number | null;
    end_user_id: number;        // user_id: number;
    match_id: string;           // matchId: string;
    odds: number;               // odd: number;
    bet_type: string;           // betType: string;
    bet_type_content?: any    // betContent?: any;
    bet_content: string         // oddType: string;
    spent_coin: number    // amount: number;
    // user_id: number;
    // matchId: string;
    // oddType: string;
    // odd: number;
    // betType: string;
    // betContent?: any;
    // amount: number;
}
export interface CartState {
    draftBets: DraftBet[]
}


const initialState: CartState = {
    draftBets: []
}

export const cartReducer = (state: CartState = initialState, action: CartActions | AuthActions): CartState => {
        if (action.type === '@@cart/ADD_DRAFT_BET') {
            return {
                ...state,
                draftBets: state.draftBets.concat(action.draftBet)
            }
        } else if (action.type === '@@cart/REMOVE_DRAFT_BET') {
            console.log("remove")
            console.log({
                state, action})
            return {
                ...state,
                draftBets: state.draftBets.filter(bet => bet.id !== action.betId)
            }
        } else if (action.type === '@@cart/LOAD_ALL_DRAFT_BET') {
            return {
                ...state,
                draftBets: action.bets
            }
        } else if (action.type === '@@auth/LOGOUT_SUCCESS') {
            return initialState
        
        } else {
            return state;
        }

}