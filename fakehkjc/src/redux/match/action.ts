import { Dispatch } from "redux";
import { Match } from "./reducer";

export function loadMatchesForBet(matches: Match[]) {
    return {
        type: "@@match/LOAD_MATCHES_FOR_BET" as "@@match/LOAD_MATCHES_FOR_BET",
        matches
    }
}

export function fetchMatches() {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/matches`)
        // console.log(res)
        const json = await res.json();
        // console.log(json)

        dispatch(loadMatchesForBet(json.map((match: any) => ({
            ...match,
        }))))


    }
}


export type MatchActions = ReturnType<typeof loadMatchesForBet>