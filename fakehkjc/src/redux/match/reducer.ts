import { IObjectKeys } from '../../App'
import { MatchActions } from './action'
export const betTypeMap:any ={
    had_h_odds: '主客和',
    had_d_odds: '主客和',
    had_a_odds: '主客和',
    hdc_h_odds: '讓球',
    hdc_a_odds: '讓球',
    crs_odds_s0100: '波膽',
    crs_odds_s0200: '波膽',
    crs_odds_s0201: '波膽',
    crs_odds_s0300: '波膽',
    crs_odds_s0301: '波膽',
    crs_odds_s0302: '波膽',
    crs_odds_s0400: '波膽',
    crs_odds_s0401: '波膽',
    crs_odds_s0402: '波膽',
    crs_odds_s0500: '波膽',
    crs_odds_s0501: '波膽',
    crs_odds_s0502: '波膽',
    crs_odds_sm1mh: '波膽',
    crs_odds_s0000: '波膽',
    crs_odds_s0101: '波膽',
    crs_odds_s0202: '波膽',
    crs_odds_s0303: '波膽',
    crs_odds_sm1md: '波膽',
    crs_odds_s0001: '波膽',
    crs_odds_s0002: '波膽',
    crs_odds_s0102: '波膽',
    crs_odds_s0003: '波膽',
    crs_odds_s0103: '波膽',
    crs_odds_s0203: '波膽',
    crs_odds_s0004: '波膽',
    crs_odds_s0104: '波膽',
    crs_odds_s0204: '波膽',
    crs_odds_s0005: '波膽',
    crs_odds_s0105: '波膽',
    crs_odds_s0205: '波膽',
    // delete 其他?
    crs_odds_sm1ma: '波膽',
    hil_odds_l: '入球大細',
    hil_odds_h: '入球大細',
    chl_odds_l: '角球大細',
    chl_odds_h: '角球大細'
}


export const oddTypeMap: IObjectKeys = {
    had_h_odds:'主勝',
    had_d_odds:'和',
    had_a_odds:'客勝',
    hdc_h_odds:'讓球主勝',
    hdc_a_odds: '讓球客勝', 
    crs_odds_s0100: '1:0',
    crs_odds_s0200: '2:0',
    crs_odds_s0201: '2:1',
    crs_odds_s0300: '3:0',
    crs_odds_s0301: '3:1',
    crs_odds_s0302: '3:2',
    crs_odds_s0400: '4:0',
    crs_odds_s0401: '4:1',
    crs_odds_s0402: '4:2',
    crs_odds_s0500: '5:0',
    crs_odds_s0501: '5:1',
    crs_odds_s0502: '5:2',
    crs_odds_sm1mh: '主其他',
    crs_odds_s0000: '0:0',
    crs_odds_s0101: '1:1',
    crs_odds_s0202: '2:2',
    crs_odds_s0303: '3:3',
    crs_odds_sm1md: '和其他',
    crs_odds_s0001: '0:1',
    crs_odds_s0002: '0:2',
    crs_odds_s0102: '1:2',
    crs_odds_s0003: '0:3',
    crs_odds_s0103: '1:3',
    crs_odds_s0203: '2:3',
    crs_odds_s0004: '0:4',
    crs_odds_s0104: '1:4',
    crs_odds_s0204: '2:4',
    crs_odds_s0005: '0:5',
    crs_odds_s0105: '1:5',
    crs_odds_s0205: '2:5',
    crs_odds_sm1ma: '客其他',
    hil_odds_l: '入球細',
    hil_odds_h: '入球大',
    chl_odds_l: '角球細',
    chl_odds_h: '角球大'
}

export interface Match {
    jc_match_id: string,
    matchStatus: string,
    home_team: string,
    home_team_flag: string,
    home_half_match_score: number,
    home_full_match_score: number,
    away_team: string,
    away_team_flag: string,
    away_half_match_score: number,
    away_full_match_score: number,
    match_type: string,
    match_day: string,
    match_date: Date,
    had_h_odds: number,
    had_a_odds: number,
    had_d_odds: number,
    hdc_hg: string,
    hdc_h_odds: number,
    hdc_ag: number,
    hdc_a_odds: number,
    crs_odds_s0100: number,
    crs_odds_s0200: number,
    crs_odds_s0201: number,
    crs_odds_s0300: number,
    crs_odds_s0301: number,
    crs_odds_s0302: number,
    crs_odds_s0400: number,
    crs_odds_s0401: number,
    crs_odds_s0402: number,
    crs_odds_s0500: number,
    crs_odds_s0501: number,
    crs_odds_s0502: number,
    crs_odds_sm1mh: number,
    crs_odds_s0000: number,
    crs_odds_s0101: number,
    crs_odds_s0202: number,
    crs_odds_s0303: number,
    crs_odds_sm1md: number,
    crs_odds_s0001: number,
    crs_odds_s0002: number,
    crs_odds_s0102: number,
    crs_odds_s0003: number,
    crs_odds_s0103: number,
    crs_odds_s0203: number,
    crs_odds_s0004: number,
    crs_odds_s0104: number,
    crs_odds_s0204: number,
    crs_odds_s0005: number,
    crs_odds_s0105: number,
    crs_odds_s0205: number,
    crs_odds_sm1ma: number,
    hil_odds_line: string,
    hil_odds_l: number,
    hil_odds_h: number,
    chl_odds_line: string,
    chl_odds_l: number,
    chl_odds_h: number
}

export interface MatchState {
    matches: Match[]
}

const initialState: MatchState = {
    matches: []
}

export const matchReducer = (state: MatchState = initialState, action: MatchActions) => {
    if (action.type === "@@match/LOAD_MATCHES_FOR_BET") {
        return {
            ...state,
            matches: action.matches
        }
    }
    return state
}





// export interface Match {
//     id:number,
//     date:Date,
//     number:number,
//     league:string,
//     home:string,
//     away:string,
// }

// export interface MatchState {
//     matches:Match[]
// }

// const initialState: MatchState = {
//     matches: [{id:1,date:new Date(2020,0,19,9,10),number: 1,league:'英足盃',home:'車路士',away:'盧頓'},
//     {id:2,date:new Date(2020,0,19,9,45),number: 2,league:'意甲',home:'卡利亞里',away:'AC米蘭'},
//     {id:3,date:new Date(2020,0,19,9,45),number: 3,league:'中超',home:'廣洲恒大',away:'上海申花'},]
//   }


// export const matchReducer = (state: MatchState = initialState, action:any) : MatchState => {
//     return state;
// }