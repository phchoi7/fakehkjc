// import { Action } from "redux"
import { AuthActions } from "../auth/action"
import { FollowActions } from "./action"


export interface Followee {
    id: number | null,
    username: string | null,
    coin: number | null,
    no_of_game: number | null,
    no_of_win_game: number | null,
    no_of_game_COR: number | null,
    no_of_win_game_COR: number | null,
    no_of_game_CRS: number | null,
    no_of_win_game_CRS: number | null,
    no_of_game_HAD: number | null,
    no_of_win_game_HAD: number | null,
    no_of_game_HDC: number | null,
    no_of_win_game_HDC: number | null,
    no_of_game_HIL: number | null,
    no_of_win_game_HIL: number | null,
}


export interface FolloweeState {
    followee: Followee[]
}


const initialState: FolloweeState = {
    followee: []
}

export const FolloweeReducer = (state: FolloweeState = initialState, action: FollowActions | AuthActions) => {
    if (action.type === "@@followee/GET_INFO") {
        return {
            ...state,
            followee: action.followee
        }
    }
    if (action.type === "@@followee/ADD_FOLLOWEE") {
        return {
            ...state,
            followee: state.followee.concat(action.followee)
        }
    }
    if (action.type === "@@followee/DELETE_FOLLOWEE") {
        return {
            ...state,
            followee: state.followee.filter(follow => follow.username !==action.username)
        }
    }
    if (action.type === "@@auth/LOGOUT_SUCCESS") {
        return initialState
    }
    return state
}
