import { Dispatch } from "redux"
import { Followee } from "./reducer"

export function followeeInfo(followee: any) {
    return {
        type: "@@followee/GET_INFO" as "@@followee/GET_INFO",
        followee
    }
}

export function addFollowee(followee: any) {
    return {
        type: "@@followee/ADD_FOLLOWEE" as "@@followee/ADD_FOLLOWEE",
        followee
    }
}

export function clearFollowee(username: string | null) {
    return {
        type: "@@followee/DELETE_FOLLOWEE" as "@@followee/DELETE_FOLLOWEE",
        username
    }
}


export function fetchFolloweeInfo(user_id: string) {
    console.log("followee info")
    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/followee/${user_id}`, {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                }
            })
            const json = await res.json()
            console.log (json)

            dispatch(followeeInfo(json.map((followee: any) => ({
                ...followee,
            }))))

        } catch (e) {
            console.log(e)
        }

    }
}

export function insertFollowee(followee_Info: Followee) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/ranking/followee`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                end_user_id: followee_Info.id,
                followee_username: followee_Info.username,
            })
        })
        console.log (res)

        dispatch(addFollowee(followee_Info))
    }
}

export function deleteFollowee(id: number | null, username: string | null) {
    return async (dispatch: Dispatch<any>) => {
        await fetch(`${process.env.REACT_APP_BACKEND_URL}/ranking/followee/delete`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                end_user_id: id,
                followee_username: username,
            })
        })
        dispatch(clearFollowee(username))
    }
}

export type FollowActions = ReturnType<typeof followeeInfo> | 
                            ReturnType<typeof addFollowee>  |
                            ReturnType<typeof clearFollowee>