import { Dispatch } from "redux"

export function storeUserInfo(info: any) {
    return {
        type: "@@user/STORE_INFO" as "@@user/STORE_INFO",
        info
    }
}

export function getBetRecord(record: any) {
    return {
        type: "@@user/GET_BET_RECORD" as "@@user/GET_BET_RECORD",
        record
    }
}

export function registerStatus(msg: string) {
    return {
        type: "@@user/REGISTER_STATUS" as "@@user/REGISTER_STATUS",
        msg
    }
}

export function registerAc(email: string, username: string, password: string) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/register`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({email, username, password})
        })

        const json = await res.json()
        console.log(json)
        if (json.msg) {
            dispatch(registerStatus(json.msg))
        } else {
            dispatch(registerStatus("註冊成功，請立即登入，祝你贏多啲！"))
        }

    }
}

export function fetchBetRecord(user_id: string) {
    console.log("bet record")

    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch (`${process.env.REACT_APP_BACKEND_URL}/bet-records/${user_id}`, {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                }
            })
            const json = await res.json()
            console.log(json)
            if (!json.result) {
                dispatch(getBetRecord(json.map((betRecord: any) => ({
                    ...betRecord,
                }))))
            }
        } catch (e) {
            console.log(e)
        }

    }
}


export type UserActions = ReturnType<typeof storeUserInfo> | ReturnType<typeof getBetRecord> |
                          ReturnType<typeof registerStatus>