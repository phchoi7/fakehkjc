// import { Action } from "redux"
import { AuthActions } from "../auth/action"
// import { AuthState } from "../auth/reducer"
import { UserActions } from "./action"

export interface UserState {
    id: number | null,
    username: string | null,
    coin: number | null,
    no_of_game: number | null,
    no_of_win_game: number | null,
    no_of_game_COR: number | null,
    no_of_win_game_COR: number | null,
    no_of_game_CRS: number | null,
    no_of_win_game_CRS: number | null,
    no_of_game_HAD: number | null,
    no_of_win_game_HAD: number | null,
    no_of_game_HDC: number | null,
    no_of_win_game_HDC: number | null,
    no_of_game_HIL: number | null,
    no_of_win_game_HIL: number | null,
    message: string | null
}

const initialState: UserState = {
    id: null,
    username: null,
    coin: null,
    no_of_game: null,
    no_of_win_game: null,
    no_of_game_COR: null,
    no_of_win_game_COR: null,
    no_of_game_CRS: null,
    no_of_win_game_CRS: null,
    no_of_game_HAD: null,
    no_of_win_game_HAD: null,
    no_of_game_HDC: null,
    no_of_win_game_HDC: null,
    no_of_game_HIL: null,
    no_of_win_game_HIL: null,
    message: null
}

export const UserReducer = (state: UserState = initialState, action: UserActions | AuthActions) => {
    if (action.type === "@@user/STORE_INFO") {
        return {
            ...state,
            id: action.info.id,
            username: action.info.username,
            coin: action.info.coin,
            no_of_game: action.info.no_of_game,
            no_of_win_game: action.info.no_of_win_game,
            no_of_game_COR: action.info.no_of_game_COR,
            no_of_win_game_COR: action.info.no_of_win_game_COR,
            no_of_game_CRS: action.info.no_of_game_CRS,
            no_of_win_game_CRS: action.info.no_of_win_game_CRS,
            no_of_game_HAD: action.info.no_of_game_HAD,
            no_of_win_game_HAD: action.info.no_of_win_game_HAD,
            no_of_game_HDC: action.info.no_of_game_HDC,
            no_of_win_game_HDC: action.info.no_of_win_game_HDC,
            no_of_game_HIL: action.info.no_of_game_HIL,
            no_of_win_game_HIL: action.info.no_of_win_game_HIL,
        }
    } else if (action.type ===  "@@auth/LOGOUT_SUCCESS") {
        return {
            state: initialState
        }
    } else if (action.type === "@@user/REGISTER_STATUS") {
        return {
            message: action.msg
        }
    } else {
        return state
    }
}

export interface EachBetRecord {
    home_team: string,
    // 備用
    // home_team_flag: string,
    away_team: string,
    // match_away_team_flag: string,
    home_full_match_score: number,
    away_full_match_score: number,
    match_id: string,
    match_type: string,
    match_day: string,
    match_date: string,
    spent_coin: number,
    gain_coin: number,
    bet_type: string,
    bet_type_content: string | null,
    bet_content: string,
    odds: number,
    bet_result_type: string,
    created_at: Date,
}

export interface BetRecordState {
    bets: EachBetRecord[]
}

const initialBetRecordState = {
    bets: []
}

export const UserBetReducer = (state: BetRecordState = initialBetRecordState, action: UserActions | AuthActions) => {
    if (action.type === "@@user/GET_BET_RECORD") {
        return {
            ...state,
            bets: state.bets.concat(action.record)
        }
    } 
    if (action.type === '@@auth/LOGOUT_SUCCESS') {
        return initialState
    }
    return state

}
