import { push } from "connected-react-router"
import { Dispatch } from "redux"
import { storeUserInfo } from "../user/action"

export function loginSuccess(token: string, id: number) {
    return {
        type: "@@auth/LOGIN_SUCCESS" as "@@auth/LOGIN_SUCCESS",
        token,
        id
    }
}

export function loginFailed(message: string) {
    return {
        type: "@@auth/LOGIN_FAILED" as "@@auth/LOGIN_FAILED",
        message
    }
}

export function logoutSuccess() {
    return {
        type: "@@auth/LOGOUT_SUCCESS" as "@@auth/LOGOUT_SUCCESS"
    }
}


export type AuthActions = ReturnType<typeof loginSuccess> | ReturnType<typeof loginFailed> | ReturnType<typeof logoutSuccess>

export function login(email: string, password: string) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ email, password })
        })
        const json = await res.json()
        console.log("auth action")
        console.log(json)
        // console.log("json: ", json)

        if (json.token) {
            localStorage.setItem("token", json.token)
            dispatch(loginSuccess(json.token, json.id))
            dispatch(push('/profile/stat'))

        } else {
            console.log("json: ", json)
            dispatch(loginFailed(json.result))
        }
    }
}

export function logout(){
    return async (dispatch: Dispatch<any>) => {
        dispatch(logoutSuccess())
        dispatch(push('/'))
        localStorage.removeItem("token")
    }
}


export function checkLogin(token: string | null) {
    return async (dispatch: Dispatch<any>) => {
        if (token == null) {
            console.log("no token")
            dispatch(logoutSuccess())
            return
        }

        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/current-user`, {
            method: "GET",
            headers: {
                "Authorization": "Bearer " + token
            },
        })

        const json = await res.json()
        // console.log("auth action:")
        // console.log(json)

        if (json.username) {
            dispatch(loginSuccess(token, json.id))
            dispatch(storeUserInfo(json))
        } else {
            dispatch(logoutSuccess())
        }
    }
}


export function loginFacebook(accessToken: string){
    console.log("7")
    console.log(accessToken)
    return async(dispatch: Dispatch<any>) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login/facebook`, {
            method:'POST',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({accessToken})
        })
        console.log("8")
        const result = await res.json();
        console.log("9")
        console.log(result)
        if (res.status!==200) {
            dispatch(loginFailed(result.msg));
        } else {
            localStorage.setItem('token', result.token);
            dispatch(loginSuccess(result.token, result.id))
            dispatch(push("/profile/stat"));
        }
    }
}