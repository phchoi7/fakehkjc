// import { UserState } from "../user/reducer"
import { AuthActions } from "./action"

export interface AuthState {
    isAuthenticated: boolean | null,
    user_id: number | null,
    username: string | null,
    token: string | null,
    message: string | null
}

const initialState: AuthState = {
    isAuthenticated: null,
    user_id: null,
    username: null,
    token: null,
    message: null,
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
    if (action.type === "@@auth/LOGIN_SUCCESS") {
        return {
            ...state, 
            isAuthenticated: true,
            token: action.token,
            user_id: action.id,
            message: null
        }
    } else if (action.type === "@@auth/LOGIN_FAILED") {
        return {
            ...state, 
            isAuthenticated: false,
            message: action.message
        }
    } else if (action.type === "@@auth/LOGOUT_SUCCESS") {
        return {
            ...state, 
            isAuthenticated: null,
            username: null,
            token: null,
            user_id: null,
        }
    }

    return state
}