// import { Bet } from '../bet/reducer'
import { AuthActions } from '../auth/action'
import { TipsActions } from './action'

export interface Tips {
    tip_provider_id: number,
    username: string,
    match_id: string,
    bet_type: string,
    bet_type_content: string,
    bet_content: string,
    home_team: string,
    away_team: string,
    match_type: string,
    match_date: string,
    match_day: string,
    odds: number
}
export interface TipsProvider {
    id: number,
    username: string,
    coin: number
    betRecord:string[]
    bet: Tips[]
}

const initialState: paidTipState = { 
    paidTips: []
}

export interface Payment {
    customer_id: number | null
    tips_provider: string,
    match_id: string,
}
export interface paidTipState {
    paidTips: Tips[] //買咗邊場+買咗邊個+買咗咩tips
}

export const paidTipsReducer = (state: paidTipState = initialState, action: TipsActions | AuthActions) => {
    if (action.type === '@@tips/LOAD_PAID_TIPS_BY_MATCH') {
        return {
            ...state,
            paidTips: action.paidTips
        }
    }
    if (action.type === '@@tips/BUY_PAID_TIPS_BY_MATCH') {
        return {
            ...state,
            paidTips: state.paidTips.concat(action.paidTips)
        }
    }
    if (action.type === '@@auth/LOGOUT_SUCCESS') {
        return initialState
    }


    return state
}


