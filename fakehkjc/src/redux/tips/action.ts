import { Dispatch } from "redux";
// import { paidTipState } from "./reducer";
import { Payment } from "./reducer"

export function loadPaidTipsByMatch(paidTips: []) {
  return {
    type: '@@tips/LOAD_PAID_TIPS_BY_MATCH' as '@@tips/LOAD_PAID_TIPS_BY_MATCH',
    paidTips
  }
}

export function buyPaidTipsByMatch(paidTips: []) {
  return {
    type: '@@tips/BUY_PAID_TIPS_BY_MATCH' as '@@tips/BUY_PAID_TIPS_BY_MATCH',
    paidTips
  }
}
export function fetchPaidTips(user_id: number | null) {
  return async (dispatch: Dispatch<any>) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/tips/paidTips/${user_id}`, {
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }
    })
    const json = await res.json();
    console.log("thunk", json)

    dispatch(loadPaidTipsByMatch(json))
  }
}

export function sendTips(payment: Payment) {
  return async (dispatch: Dispatch<any>) => {
    await fetch(`${process.env.REACT_APP_BACKEND_URL}/tips/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        customer_id: payment.customer_id,
        tips_provider: payment.tips_provider,
        match_id: payment.match_id,
        paidCoin: 100

      })
    })
    console.log('sent Tips')
    dispatch(fetchPaidTips(payment.customer_id))
    // 做法一： server return 返一支 bet，然後 add 入 redux store
    // 做法二： 重新 dispatch loadAllDraftBets()

  }

}




export type TipsActions = ReturnType<typeof loadPaidTipsByMatch> | ReturnType<typeof buyPaidTipsByMatch>
