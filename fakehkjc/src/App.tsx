// import { push, replace } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Route, Switch } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  // NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
// import { Container, Row, Col } from 'reactstrap';
import './App.css';
import { BetAreaPage } from './Pages/BetAreaPage';
import { BetPage } from './Pages/BetPage';
import { RankingPage } from './Pages/RankingPage';
import { HomePage } from './Pages/HomePage';
import { LoginPage } from './Pages/LoginPage';

import { ProfileFailPage, ProfilePage } from './Pages/ProfilePage';
import { Results } from './Pages/ResultPage';
import { checkLogin, logout } from './redux/auth/action';
// import { fetchMatches } from './redux/match/action';
import { RootState } from './store';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import SportsSoccerIcon from '@material-ui/icons/SportsSoccer';
import BallotIcon from '@material-ui/icons/Ballot';
import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber';
import GroupIcon from '@material-ui/icons/Group';
import PersonIcon from '@material-ui/icons/Person';
import Badge from '@material-ui/core/Badge';
// import { Grommet } from 'grommet';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { RegisterPage } from './Pages/RegisterPage';
// import { BetRecord } from './Components/ProfileContent';
import { Icon } from 'semantic-ui-react'
import AppTopBar from './Components/AppTopBar';

export interface IObjectKeys {
  [key: string]: string;
}

export const weekday: IObjectKeys = {
  MON: "一",
  TUE: "二",
  WED: "三",
  THU: "四",
  FRI: "五",
  SAT: "六",
  SUN: "日"
}

export const betResultType: IObjectKeys = {
  win: "贏",
  draw: "和",
  lose: "輸"
}

export const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

export const leagueEngToChi: IObjectKeys = {
  premierLeague: "英格蘭超級聯賽",
  laLiga: "西班牙甲組聯賽",
  serieA: "意大利甲組聯賽",
  bundesliga: "德國甲組聯賽",
  ligue: "法國甲組聯賽"
}

function App(props: any) {
  const [isOpen, setIsOpen] = useState(false);
  const numberOfBets = useSelector((state: RootState) => {
    if (state.cart.draftBets) {
      return state.cart.draftBets.length
    }
  })
  const toggle = () => setIsOpen(!isOpen);
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const userName = useSelector((state:RootState)=> state.user.username)
//  const userCoins = useSelector((state:RootState)=> state.user.coin)

  const dispatch = useDispatch()
  const [fetchAgain, setFetchAgain] = useState(false)
  const {
    // buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const Modaltoggle = () => setModal(!modal);

  const useStyles = makeStyles({
    root: {
      position: 'fixed',
      bottom: 0,
      left: '20%',
      margin: '0 0 0 -20%',
      width: '100%',
      padding:'5px',
    },
  });
  const classes = useStyles();
  const pathname = window.location.pathname; // in case user visits the path directly. The BottomNavBar is able to follow suit.
  const [value, setValue] = React.useState(pathname);
  const handleChange = (event: any, newValue: any) => {
    setValue(newValue);
  };


  useEffect(() => {
    dispatch(checkLogin(localStorage.getItem('token')))
  })


  return (
    
    <div className="App">
      <AppTopBar />
      <Navbar className="nav-bar" color="light" light expand="md">
        <NavbarBrand href="/">貼士多</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <Link to="/" className="nav-link nav-tab">主頁</Link>
            </NavItem>
            <NavItem>
              <Link to="/bet-page" className="nav-link nav-tab" onClick={() => setFetchAgain(!fetchAgain)}>即時投注</Link>
            </NavItem>
            <NavItem>
              <Link to="/match-result" className="nav-link nav-tab" onClick={() => setFetchAgain(!fetchAgain)}>賽事結果</Link>
            </NavItem>
            <NavItem>
              {isAuthenticated &&
                <Link to="/bet-area" className="nav-link nav-tab">
                  <Badge className="bet-badge" badgeContent={numberOfBets} color="error">
                    投注區
                  </Badge>
                </Link>}
            </NavItem>
            <NavItem>
              <Link to="/ranking" className="nav-link" onClick={() => setFetchAgain(!fetchAgain)}>社區排名</Link>
            </NavItem>
            {!isAuthenticated && <NavItem>
              <Link to="/login" className="nav-link" >註冊/登入</Link>
            </NavItem>}
            {isAuthenticated && <NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  <Icon name='user circle' />
                  {userName}
              </DropdownToggle>
                <DropdownMenu className="nav-drop" right>
                  <DropdownItem className="navdrop-item" tag={Link} to="/profile/stat">
                    概覽
                  </DropdownItem>
                  <DropdownItem className="navdrop-item" tag={Link} to="/profile/record">
                    投注紀錄
                  </DropdownItem>
                  <DropdownItem className="navdrop-item" tag={Link} to="/profile/tips">
                    已買Tips
                  </DropdownItem>
                  <DropdownItem className="navdrop-item" tag={Link} to="/profile/follow">
                    跟隨紀錄
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem className="navdrop-item">
                    設定
                  </DropdownItem >
                  <DropdownItem className="navdrop-item" onClick={() => { dispatch(logout()) }}>
                    登出
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </NavItem>}
          </Nav>
        </Collapse>
      </Navbar>

      <Modal isOpen={modal} toggle={Modaltoggle} className={className}>
        <ModalHeader toggle={Modaltoggle}>Modal title</ModalHeader>
        <ModalBody>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={Modaltoggle}>Do Something</Button>{' '}
          <Button color="secondary" onClick={Modaltoggle}>Cancel</Button>
        </ModalFooter>
      </Modal>

      
            <div className="main">
              <Switch>
                <Route path="/" exact>
                  <HomePage />
                </Route>
                <Route path="/ranking" exact>
                  <RankingPage />
                </Route>
                <Route path="/bet-page/:match?/:id?">
                  <BetPage />
                </Route>
                <Route path="/bet-page/match/:id?" >
                  <BetPage />
                </Route>
                <Route path="/match-result/:league?" >
                  <Results />
                </Route>
                {isAuthenticated ?
                  <Route path="/bet-area" >
                    <BetAreaPage />
                  </Route> :
                  <Route path="/bet-area">
                    <ProfileFailPage />
                  </Route>}

                <Route path="/login" >
                  <LoginPage />
                </Route>
                <Route path="/register" >
                  <RegisterPage />
                </Route>

                <Route path="/auth">
                  <ProfileFailPage />
                </Route>

                {isAuthenticated ?
                  <Route path="/profile/stat">
                    <ProfilePage />
                  </Route>
                  :
                  <Route path="/profile">
                    <ProfileFailPage />
                  </Route>}
              </Switch>
            </div>
          
      <div className="bottom-nav">
        <BottomNavigation

          value={value}
          onChange={handleChange}
          showLabels
          className={classes.root }
          
        >
          <BottomNavigationAction className="bottom-nav-action" label="即時投注" icon={<SportsSoccerIcon />} component={Link} to='/bet-page' />
          <BottomNavigationAction className="bottom-nav-action" label="賽事結果" icon={<BallotIcon />} component={Link} to='/match-result' />
          <BottomNavigationAction className="bottom-nav-action" label="投注區" icon={<Badge badgeContent={numberOfBets} color="error"><ConfirmationNumberIcon /></Badge>} component={Link} to='/bet-area' />
          <BottomNavigationAction className="bottom-nav-action" label="社區排名" icon={<GroupIcon />} component={Link} to='/ranking' />
          <BottomNavigationAction className="bottom-nav-action" label="用戶專區" icon={<PersonIcon />} component={Link} to='/profile/stat' />
        </BottomNavigation>
      </div>
    </div >

  );
}

export default App;