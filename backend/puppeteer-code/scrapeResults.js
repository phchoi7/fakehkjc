// 要分開inProgressResults.json同埋readyResults.json
const fetch = require("node-fetch")
const fs = require("fs")
const jsonfile = require("jsonfile")
const puppeteer = require('puppeteer')

console.log("start")

async function scrapeAllMatchesResult() {
    let todayMatchesResults = await fetch("https://bet.hkjc.com/football/getJSON.aspx?jsontype=results.aspx")
    let todayMatchesResultsJson = await todayMatchesResults.json()

    // scrape data into in-progress json, so the ready json file will not be affected 
    let readJson = await jsonfile.readFile("inProgressResults.json", "utf8")
    readJson = todayMatchesResultsJson 
    await jsonfile.writeFile("inProgressResults.json", readJson, { spaces: 2 })

    // amended the ready json file
    await jsonfile.writeFile("readyResults.json", todayMatchesResultsJson , { spaces: 2 })

    console.log("done")
}

scrapeAllMatchesResult()


