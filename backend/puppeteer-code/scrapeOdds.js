// 要分開inProgressOdds.json同埋readyOdds.json

const fetch = require("node-fetch")
const fs = require("fs")
const jsonfile = require("jsonfile")
const puppeteer = require('puppeteer')

console.log("start")

async function scrapeAllMatchesOdd() {
    let todayMatches = await fetch("https://bet.hkjc.com/football/getJSON.aspx?jsontype=index.aspx")
    let todayMatchesJson = await todayMatches.json()

    let cleanJson = await jsonfile.readFile("inProgressOdds.json", "utf8")
    cleanJson = []
    await jsonfile.writeFile("inProgressOdds.json", cleanJson, { spaces: 2 })

    for (let i = 0; i < todayMatchesJson.length; i++) {
        // let array = []
        console.log(i)
        const browser = await puppeteer.launch({
            headless: false
        });
        const page = await browser.newPage();
        await page.setDefaultNavigationTimeout(3000000);
        let readJson = await jsonfile.readFile("inProgressOdds.json", "utf8")

        await page.goto('https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_allodds.aspx&matchid=' + todayMatchesJson[i]["matchID"]);
        await page.waitForSelector("pre")
        let eachOdd = await page.$eval("body", async () => {
            let results = JSON.parse(document.querySelector("pre").innerText)
            return results.filter(result => result.definedPools.length > 0)
        })

        // scrape data into in-progress json, so the ready json file will not be affected 
        readJson.push(eachOdd)
        await jsonfile.writeFile("inProgressOdds.json", readJson, { spaces: 2 })

        await page.waitForTimeout(2000)
        await browser.close()
    }

    // amended the ready json file
    let readJson = await jsonfile.readFile("inProgressOdds.json", "utf8")
    await jsonfile.writeFile("readyOdds.json", readJson, { spaces: 2 })


    console.log("done")
}

scrapeAllMatchesOdd()
