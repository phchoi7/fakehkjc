import express from 'express'
import { isLoggedIn } from './guard';
import { tipsController } from './main'

export const tipsRoute = express.Router();

tipsRoute.get('/:matchId', tipsController.loadTipsProviderByMatch)
tipsRoute.get('/:userid', tipsController.loadNoResultTips)
tipsRoute.post('/', isLoggedIn, tipsController.buyTips)

tipsRoute.get('/paidTips/:userid', isLoggedIn, tipsController.loadPaidTips)
