import * as Knex from "knex";
import xlsx from 'xlsx'
import bcryptjs from 'bcryptjs'

interface IEnd_user {
    username: string,
    password: string,
    email: string,
    phone_no: string,
    coin: number,
    no_of_game: number,
    no_of_win_game: number,
    no_of_game_HAD: number,
    no_of_win_game_HAD: number,
    no_of_game_HDC: number,
    no_of_win_game_HDC: number,
    no_of_game_CRS: number,
    no_of_win_game_CRS: number,
    no_of_game_HIL: number,
    no_of_win_game_HIL: number,
    no_of_game_COR: number,
    no_of_win_game_COR: number,
}

// enum Bet_content {
//    主,
//    和,
//    客,
//    主其他,
//    客其他,
//    和其他,
//    角球細,
//    角球大,
//    入球細,
//    入球大,
//    讓球主勝,
//    讓球客勝,

// }

enum Bet_result_type {
    win,
    draw,
    lose
}
export interface IBet {
    end_user: string,
    match: string,
    spent_coin: number,
    gain_coin: number,
    bet_type: string,
    bet_type_content: string,
    bet_content: string,
    odds: number,
    bet_result_type: Bet_result_type
}

interface IMatch {
    jc_match_id: string,

    home_team: string
    home_team_flag: string,
    home_half_match_score: number,
    home_full_match_score: number,

    away_team: string,
    away_team_flag: string,
    away_half_match_score: number,
    away_full_match_score: number
    matchStatus: string,
    match_type: string,
    match_day: string,
    match_date: string,
    //日子及時間, 馬會json: 2021-01-18+08:00
    had_h_odds: number,
    had_a_odds: number,
    had_d_odds: number,
    hdc_hg: string,
    hdc_h_odds: number,
    hdc_ag: string,
    hdc_a_odds: number,
    crs_odds_s0100: number,
    crs_odds_s0200: number,
    crs_odds_s0201: number,
    crs_odds_s0300: number,
    crs_odds_s0301: number,
    crs_odds_s0302: number,
    crs_odds_s0400: number,
    crs_odds_s0401: number,
    crs_odds_s0402: number,
    crs_odds_s0500: number,
    crs_odds_s0501: number,
    crs_odds_s0502: number,
    crs_odds_sm1mh: number,
    crs_odds_s0000: number,
    crs_odds_s0101: number,
    crs_odds_s0202: number,
    crs_odds_s0303: number,
    crs_odds_sm1md: number,
    crs_odds_s0001: number,
    crs_odds_s0002: number,
    crs_odds_s0102: number,
    crs_odds_s0003: number,
    crs_odds_s0103: number,
    crs_odds_s0203: number,
    crs_odds_s0004: number,
    crs_odds_s0104: number,
    crs_odds_s0204: number,
    crs_odds_s0005: number,
    crs_odds_s0105: number,
    crs_odds_s0205: number,
    crs_odds_sm1ma: number,
    hil_odds_line: string,
    hil_odds_l: number,
    hil_odds_h: number,
    chl_odds_line: string,
    chl_odds_l: number,
    chl_odds_h: number,
}

interface IBadge_end_user {
    end_user: string,
    badge: string
}

interface IForum {
    end_user: string,
    match: string,
    content: string
}

interface IPayment {
    customer: string,
    tip_provider: string,
    match: string,
    paid_coin: number
}

interface IFollowership {
    follower_end_user: string,
    followee_end_user: string
}
let workbook = xlsx.readFile('./seeds/fakehkjc-seed.xlsx')
let end_users: IEnd_user[] = xlsx.utils.sheet_to_json(workbook.Sheets.end_user)
let bets: IBet[] = xlsx.utils.sheet_to_json(workbook.Sheets.bet)
let matches: IMatch[] = xlsx.utils.sheet_to_json(workbook.Sheets.match)
let payments: IPayment[] = xlsx.utils.sheet_to_json(workbook.Sheets.payment)
let followerships: IFollowership[] = xlsx.utils.sheet_to_json(workbook.Sheets.followership)
let badge_end_users: IBadge_end_user[] = xlsx.utils.sheet_to_json(workbook.Sheets.badge_end_user)
let badges = xlsx.utils.sheet_to_json(workbook.Sheets.badge)
let forums: IForum[] = xlsx.utils.sheet_to_json(workbook.Sheets.forum)
export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("forum").del();
    await knex("followership").del();
    await knex("badge_end_user").del();
    await knex("badge").del();
    await knex("draft_bet").del();
    await knex("payment").del();
    await knex("bet").del();
    await knex("match").del();
    await knex("end_user").del();
    //  end_user
    const end_user_data = [];
    for (const end_user of end_users) {
        const { password, ...others } = end_user
        end_user_data.push(
            {
                password: await bcryptjs.hash(String(password), 10),
                ...others,
            }
        )
    }

    const end_user_info: Array<{ id: number, username: string }> = await knex("end_user").insert(end_user_data).returning(["id", "username"])
    //  match
    const end_user_mapping = new Map();
    for (const row of end_user_info) {
        end_user_mapping.set(row.username, row.id);
    }
    const match_info: Array<{ jc_match_id: number, home_team: string, away_team: string }> = await knex("match").insert(matches).returning(["jc_match_id", "home_team", "away_team"])
    //  bet
    const match_mapping = new Map();
    for (const row of match_info) {
        let match_team = `${row.home_team} vs ${row.away_team}`
        match_mapping.set(match_team, row.jc_match_id);
    }
    const bet_data: any[] = [];
    for (const bet of bets) {
        const { end_user, match, ...others } = bet
        bet_data.push(
            {
                end_user_id: end_user_mapping.get(end_user),
                match_id: match_mapping.get(match),
                ...others
            }
        )
    }
    // console.log(bet_data)
    await knex("bet").insert(bet_data);
    //  badge
    const badge_info: Array<{ id: number, title: string }> = await knex("badge").insert(badges).returning(["id", "title"])
    //  badge_end_user
    const badge_mapping = new Map();
    for (const row of badge_info) {
        badge_mapping.set(row.title, row.id);
    }
    const badge_data: any[] = [];
    for (const badgeName of badge_end_users) {
        const { end_user, badge } = badgeName
        badge_data.push(
            {
                end_user_id: end_user_mapping.get(end_user),
                badge_id: badge_mapping.get(badge),
            }
        )
    }
    await knex("badge_end_user").insert(badge_data)
    //forum
    const forum_data: any[] = [];
    for (const forum of forums) {
        console.log (forum)
        const { end_user, match, ...others } = forum
        forum_data.push(
            {
                end_user_id: end_user_mapping.get(end_user),
                match_id: match_mapping.get(match),
                ...others
            }
        )
    }
    await knex("forum").insert(forum_data)
    //payment
    const payment_data = []
    for (const payment of payments) {
        const { customer, tip_provider, match, ...others } = payment
        payment_data.push({
            customer_id: end_user_mapping.get(customer),
            tip_provider_id: end_user_mapping.get(tip_provider),
            match_id: match_mapping.get(match),
            ...others
        })
    }
    await knex("payment").insert(payment_data)
    //followership
    const followership_data = []
    for (const follower of followerships) {
        const { follower_end_user, followee_end_user } = follower
        followership_data.push({
            followee_end_user_id: end_user_mapping.get(followee_end_user),
            follower_end_user_id: end_user_mapping.get(follower_end_user)
        }
        )
    }
    await knex("followership").insert(followership_data)
}