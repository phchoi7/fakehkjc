import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';
import { UserService } from "./services/UsersService"
// import { userService } from './main';
// import { User } from './services/model';

import Knex from 'knex'
const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])

const permit = new Bearer({
    query: "access_token"
})

const userService = new UserService(knex)

export async function isLoggedIn(req: express.Request, res: express.Response, next: express.NextFunction) {
    //cms
    try {
        const token = permit.check(req);
        if (!token) {
            //console.log("guard 24: " + req["user"])
            return res.status(401).json({ msg: "Permission Denied" });
        }
        const payload = jwtSimple.decode(token, jwt.jwtSecret);
        // const user = await knex.select('username').from('end_user').where('id', payload.id);
        // const user = await knex.select('username').from('end_user').where('id', payload.id);
        const user = await userService.checkToken(payload)
        
        if (user[0]) {
            req["user"] = user[0];
            //console.log("guard 34: " + req["user"])
            return next();
        } else {
            //console.log("guard: no permission")
            return res.status(401).json({ msg: "Permission Denied" });
        }
    } catch (e) {
        return res.status(401).json({ msg: "Permission Denied" });
    }
}