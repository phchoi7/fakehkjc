import type Knex from "knex";

export class CommentService {
  public constructor(private knex: Knex) {

  }

  public selectComments = async (matchId: string) => {
    try {
        return await this.knex('forum')
            .select("*")
            .innerJoin('end_user', 'end_user.id', 'forum.end_user_id')
            .andWhere("match_id", matchId)
    } catch (e) {
        console.log(e)
        return e
    }
  }

  public insertComments = async (end_user_id:string,match_id:string,content: string) => {
    try {
        console.log(end_user_id,match_id,content)
        return await this.knex.insert({
            end_user_id, match_id,content
          }).into('forum')
    } catch (e) {
        console.log(e)
        return e
    }





  }

}