import Knex from "knex"
import { IObjectKeys } from "./BetService"
// import { oddTypeMap } from "../../fakehkjc/src/redux/match/reducer"


export const oddTypeMap: IObjectKeys = {
    had_h_odds:'主勝',
    had_d_odds:'和',
    had_a_odds:'客勝',
    hdc_h_odds:'讓球主勝',
    hdc_a_odds: '讓球客勝', 
    crs_odds_s0100: '1:0',
    crs_odds_s0200: '2:0',
    crs_odds_s0201: '2:1',
    crs_odds_s0300: '3:0',
    crs_odds_s0301: '3:1',
    crs_odds_s0302: '3:2',
    crs_odds_s0400: '4:0',
    crs_odds_s0401: '4:1',
    crs_odds_s0402: '4:2',
    crs_odds_s0500: '5:0',
    crs_odds_s0501: '5:1',
    crs_odds_s0502: '5:2',
    crs_odds_sm1mh: '主其他',
    crs_odds_s0000: '0:0',
    crs_odds_s0101: '1:1',
    crs_odds_s0202: '2:2',
    crs_odds_s0303: '3:3',
    crs_odds_sm1md: '和其他',
    crs_odds_s0001: '0:1',
    crs_odds_s0002: '0:2',
    crs_odds_s0102: '1:2',
    crs_odds_s0003: '0:3',
    crs_odds_s0103: '1:3',
    crs_odds_s0203: '2:3',
    crs_odds_s0004: '0:4',
    crs_odds_s0104: '1:4',
    crs_odds_s0204: '2:4',
    crs_odds_s0005: '0:5',
    crs_odds_s0105: '1:5',
    crs_odds_s0205: '2:5',
    crs_odds_sm1ma: '客其他',
    hil_odds_l: '入球細',
    hil_odds_h: '入球大',
    chl_odds_l: '角球細',
    chl_odds_h: '角球大'
}


export class CartService {
    constructor (private knex: Knex) {

    }

    public insertDraft = async (draftBet: any) => {
        console.log("inserting draft")
        try {
            await this.knex("draft_bet").insert({
                end_user_id: draftBet.end_user_id,
                match_id: draftBet.match_id,
                odds: draftBet.odds,
                bet_type: draftBet.bet_type,
                bet_type_content: draftBet.bet_type_content? draftBet.bet_type_content : null,
                bet_content: oddTypeMap[draftBet.bet_content],
                spent_coin: draftBet.spent_coin,
            })
            return {result: "inserted"}
        } catch (e) {
            console.log(e)
            return e
        }
    }

    public selectDraft = async (userID: string) => {
        try {
            let result = await this.knex.select("*")
                                        .from("draft_bet")
                                        .where("end_user_id", parseInt(userID))
            return result
        } catch (e) {
            console.log(e)
            return e
        }
    }

    public deleteDraftBet = async (userId: number, betId: number) => {
        console.log("server bet id: " + userId)
        console.log("server bet id: " + betId)
        try {
            return await this.knex("draft_bet")
                        .where("id", betId)
                        .andWhere("end_user_id", userId)
                        .del()
        } catch (e) {
            console.log(e)
            return (e)
        }
    }

    public updateDraftBet = async (betId: string, odds: number) => {
        try {
            await this.knex("draft_bet")
                                    .update({
                                        odds: odds
                                    })
                                    .where("id", betId)
        } catch (e) {
            return (e)
        }
    }
}