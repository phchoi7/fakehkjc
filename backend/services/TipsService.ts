import Knex from 'knex'
import moment from 'moment'

const oneMonthAgo = (moment().subtract(1, 'month')).toISOString()
export class TipsService {
    constructor(private knex: Knex) { }

    public getTipsProviderByMatch = async (matchId: string) => {
        try {
            //Alex Version
            // let topUsersArray = await this.knex('bet')
            //     .select("bet.bet_result_type", "end_user.username")
            //     .count("*")
            //     .innerJoin("end_user", "bet.end_user_id", "end_user.id")
            //     .innerJoin("bet AS current_bet", (where) => {
            //         where.andOn("current_bet.end_user_id", "end_user.id")
            //         where.andOn("current_bet.match_id", matchId)
            //     })
            //     .where("bet.bet_result_type", "win")
            //     .andWhere("updated_at", ">", oneMonthAgo)
            //     .groupBy('bet.bet_result_type', 'end_user.username')
            //     .orderBy('count', 'desc')

            // let topUsersUsername = []
            // for (let topUser of topUsersArray) {
            //     topUsersUsername.push(topUser.username)
            // }

            //get users who win the most in the past month
            let topUsersArray = await this.knex('bet')
                .select("bet.bet_result_type", "end_user.username")
                .count("*")
                .innerJoin("end_user", "bet.end_user_id", "end_user.id")
                .where("bet.bet_result_type", "win")
                .andWhere("bet.updated_at", ">", oneMonthAgo)
                .groupBy('bet.bet_result_type', 'end_user.username')
                .orderBy('count', 'desc')

            let topUsersUsername = []
            for (let topUser of topUsersArray) {
                topUsersUsername.push(topUser.username)
            }

            console.log("topUsersUsername " + topUsersUsername)

            //filter users who did not bet on the specific match
            let haveBetUsersArray = await this.knex('bet')
                .select("end_user.username")
                .innerJoin("end_user", "end_user.id", "bet.end_user_id")
                .whereIn("end_user.username", topUsersUsername)
                .andWhere("bet.match_id", matchId)
                .groupBy("end_user.username")

            let haveBetUsers = []
            for (let haveBetUser of haveBetUsersArray) {
                haveBetUsers.push(haveBetUser.username)
            }
            console.log("haveBetUsers " + haveBetUsers)
            //get their past bet result
            let top10UsersResult = await this.knex('bet')
                .select('end_user.username', 'bet.bet_result_type', 'end_user.coin', 'end_user.id')
                .count("*")
                .innerJoin("end_user", "bet.end_user_id", "end_user.id")
                .whereIn("end_user.username", haveBetUsers)
                .andWhere("updated_at" > oneMonthAgo)
                .andWhereNot("bet.bet_result_type", null)
                .groupBy('bet.bet_result_type', 'end_user.username', 'end_user.coin', 'end_user.id')
                .orderBy('count', 'desc')


            let data: {
                id: number
                username: string,
                coin: number
                betRecord: string[]
            }[] = []

            for (let result of top10UsersResult) {
                let row = data.find(d => d.username == result.username)
                if (!row) {
                    row = {
                        id: result.id as number,
                        username: result.username as string,
                        coin: result.coin as number,
                        betRecord: [],

                    }
                    data.push(row);
                }
                row.betRecord.push(result.bet_result_type as string)
            }

            console.log("data", data)
            return data

        } catch (e) {
            console.log(e)
            return e
        }
    }
    
    public noResultBetByUser = async (id: number) => {
        try {
            await this.knex('bet')
                .select("bet.bet_type", "bet.bet_type_content", "bet.odds", "bet.bet_content", "end_user.username")
                .innerJoin("end_user", "end_user.id", "bet.end_user_id")
                .where("end_user.id", id)
        } catch (e) {
            console.log(e)
            return e
        }
    }

            //get their bet on this match
            
    public buyTipsByUser = async (customer_id: number, tip_provider: string, match_id: string, paidCoin: number) => {
        try {
            console.log(customer_id, tip_provider, match_id, paidCoin)
            let tip_provider_id = await this.knex('end_user')
                .select('id')
                .where('end_user.username', tip_provider)

            //make payment record
            await this.knex('payment')
                .insert({
                    customer_id: customer_id,
                    tip_provider_id: tip_provider_id[0].id,
                    match_id: match_id + "",
                    paid_coin: paidCoin
                })
            return ({ result: "payment has been done" })
        } catch (e) {
            console.log(e)
            return e
        }
    }


    public getPaidTipsByUser = async (userId: number) => {
        try {
      
            let result: {}[] = await this.knex('payment')
                .select("bet.*", "end_user.username", "end_user.coin",
                 "match.home_team", "match.away_team", "match.match_date", "match.match_day", "match.match_type")
                .innerJoin('bet', function(){
                    this.on('bet.end_user_id','=',"payment.tip_provider_id")
                    this.andOn('bet.match_id','=','payment.match_id')
                })
                .innerJoin('end_user', 'end_user.id',"payment.tip_provider_id")
                .innerJoin('match','match.jc_match_id','payment.match_id')
                .where("payment.customer_id", userId)
                .orderBy('bet.created_at','desc')
            return result
        } catch (e) {
            console.log(e)
            return e
        }
    }
}