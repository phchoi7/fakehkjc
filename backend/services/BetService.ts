import type Knex from "knex"
import moment from "moment"
// import { oddTypeMap } from "../../fakehkjc/src/redux/match/reducer"

export interface IObjectKeys {
    [key: string]: string;
}

const typeMap: IObjectKeys = {
    主客和: "HAD",
    波膽: "CRS",
    角球大細: "COR",
    讓球: "HDC",
    入球大細: "HIL"
}
export interface Match {
    jc_match_id: string,
    matchStatus: string,
    home_team: string,
    home_team_flag: string,
    home_half_match_score: number,
    home_full_match_score: number,
    away_team: string,
    away_team_flag: string,
    away_half_match_score: number,
    away_full_match_score: number,
    match_type: string,
    match_day: string,
    match_date: Date,
    had_h_odds: number,
    had_a_odds: number,
    had_d_odds: number,
    hdc_hg: string,
    hdc_h_odds: number,
    hdc_ag: number,
    hdc_a_odds: number,
    crs_odds_s0100: number,
    crs_odds_s0200: number,
    crs_odds_s0201: number,
    crs_odds_s0300: number,
    crs_odds_s0301: number,
    crs_odds_s0302: number,
    crs_odds_s0400: number,
    crs_odds_s0401: number,
    crs_odds_s0402: number,
    crs_odds_s0500: number,
    crs_odds_s0501: number,
    crs_odds_s0502: number,
    crs_odds_sm1mh: number,
    crs_odds_s0000: number,
    crs_odds_s0101: number,
    crs_odds_s0202: number,
    crs_odds_s0303: number,
    crs_odds_sm1md: number,
    crs_odds_s0001: number,
    crs_odds_s0002: number,
    crs_odds_s0102: number,
    crs_odds_s0003: number,
    crs_odds_s0103: number,
    crs_odds_s0203: number,
    crs_odds_s0004: number,
    crs_odds_s0104: number,
    crs_odds_s0204: number,
    crs_odds_s0005: number,
    crs_odds_s0105: number,
    crs_odds_s0205: number,
    crs_odds_sm1ma: number,
    hil_odds_line: string,
    hil_odds_l: number,
    hil_odds_h: number,
    chl_odds_line: string,
    chl_odds_l: number,
    chl_odds_h: number
}
export class BetService {
    public constructor(private knex: Knex) {
    }

    public matchingOdds = async (user_id: string) => {
        // console.log(id)
        interface draftArray {
            id: number,
            end_user_id: number,
            match_id: string,
            spent_odds: number,
            odds: number,
            bet_type: string,
            bet_type_content: string,
            bet_content: string
        }
        try {
            let result = {
                match: [] as Match[],
                dateFail: [] as draftArray[],
                oddFail: [] as draftArray[],
                oddPass: [] as draftArray[],
                betTypeContentFail: [] as draftArray[]
            }

            let drafts = await this.knex.select("*")
                .from("draft_bet")
                .where("end_user_id", parseInt(user_id))

            for (let draft of drafts) {
                let match = await this.knex.select("*").from("match")
                    .where("jc_match_id", draft.match_id)

                // if (result['match'].find(match => match.jc_match_id == match.jc_match_id) == undefined) {
                //     result["match"].push(match as any)}

                result['match'].push(match as any)

                if (moment(new Date()).format("YYYY-MM-DD HH:mm") > moment(match[0].match_date).format("YYYY-MM-DD HH:mm")) {
                    result["dateFail"].push(draft)
                } else {
                    if (draft.bet_content == "角球細") {
                        if (draft.odds == match[0].chl_odds_l && draft.bet_type_content == match[0].chl_odds_line) {
                            result["oddPass"].push(draft)
                        } else if (draft.bet_type_content != match[0].chl_odds_line) {
                            result["betTypeContentFail"].push(draft)
                        } else if (draft.odds != match[0].chl_odds_l) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "角球大") {
                        if (draft.odds == match[0].chl_odds_h && draft.bet_type_content == match[0].chl_odds_line) {
                            result["oddPass"].push(draft)
                        } else if (draft.bet_type_content != match[0].chl_odds_line) {
                            result["betTypeContentFail"].push(draft)
                        } else if (draft.odds != match[0].chl_odds_h) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "主勝") {
                        if (draft.odds == match[0].had_h_odds) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].had_h_odds) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "和") {
                        if (draft.odds == match[0].had_d_odds) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].had_d_odds) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "客勝") {
                        if (draft.odds == match[0].had_a_odds) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].had_a_odds) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "入球細") {
                        if (draft.odds == match[0].hil_odds_l && draft.bet_type_content == match[0].hil_odds_line) {
                            result["oddPass"].push(draft)
                        } else if (draft.bet_type_content != match[0].hil_odds_line) {
                            result["betTypeContentFail"].push(draft)
                        } else if (draft.odds != match[0].hil_odds_l) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "入球大") {
                        if (draft.odds == match[0].hil_odds_h && draft.bet_type_content == match[0].hil_odds_line) {
                            result["oddPass"].push(draft)
                        } else if (draft.bet_type_content != match[0].hil_odds_line) {
                            result["betTypeContentFail"].push(draft)
                        } else if (draft.odds != match[0].hil_odds_h) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "讓球客勝") {
                        if (draft.odds == match[0].hdc_a_odds && draft.bet_type_content == match[0].hdc_ag) {
                            result["oddPass"].push(draft)
                        } else if (draft.bet_type_content != match[0].hdc_ag) {
                            result["betTypeContentFail"].push(draft)
                        } else if (draft.odds != match[0].hdc_a_odds) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "讓球主勝") {
                        if (draft.odds == match[0].hdc_h_odds && draft.bet_type_content == match[0].hdc_hg) {
                            result["oddPass"].push(draft)
                        } else if (draft.bet_type_content != match[0].hdc_hg) {
                            result["betTypeContentFail"].push(draft)
                        } else if (draft.odds != match[0].hdc_h_odds) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "1:0") {
                        if (draft.odds == match[0].crs_odds_s0100) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0100) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "2:0") {
                        if (draft.odds == match[0].crs_odds_s0200) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0200) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "2:1") {
                        if (draft.odds == match[0].crs_odds_s0201) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0201) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "3:0") {
                        if (draft.odds == match[0].crs_odds_s0300) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0300) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "3:1") {
                        if (draft.odds == match[0].crs_odds_s0301) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0301) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "3:2") {
                        if (draft.odds == match[0].crs_odds_s0302) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0302) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "4:0") {
                        if (draft.odds == match[0].crs_odds_s0400) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0400) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "4:1") {
                        if (draft.odds == match[0].crs_odds_s0401) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0401) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "4:2") {
                        if (draft.odds == match[0].crs_odds_s0402) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0402) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "5:0") {
                        if (draft.odds == match[0].crs_odds_s0500) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0500) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "5:1") {
                        if (draft.odds == match[0].crs_odds_s0501) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0501) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "5:2") {
                        if (draft.odds == match[0].crs_odds_s0502) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0502) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "主其他") {
                        if (draft.odds == match[0].crs_odds_sm1mh) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_sm1mh) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "0:0") {
                        if (draft.odds == match[0].crs_odds_s0000) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0000) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "1:1") {
                        if (draft.odds == match[0].crs_odds_s0101) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0101) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "2:2") {
                        if (draft.odds == match[0].crs_odds_s0202) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0202) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "3:3") {
                        if (draft.odds == match[0].crs_odds_s0303) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0303) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "和其他") {
                        if (draft.odds == match[0].crs_odds_sm1md) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_sm1md) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "0:1") {
                        if (draft.odds == match[0].crs_odds_s0001) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0001) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "0:2") {
                        if (draft.odds == match[0].crs_odds_s0002) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0002) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "1:2") {
                        if (draft.odds == match[0].crs_odds_s0102) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0102) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "0:3") {
                        if (draft.odds == match[0].crs_odds_s0003) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0003) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "1:3") {
                        if (draft.odds == match[0].crs_odds_s0103) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0103) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "2:3") {
                        if (draft.odds == match[0].crs_odds_s0203) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0203) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "0:4") {
                        if (draft.odds == match[0].crs_odds_s0104) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0104) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "1:4") {
                        if (draft.odds == match[0].crs_odds_s0104) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0104) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "2:4") {
                        if (draft.odds == match[0].crs_odds_s0204) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0204) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "0:5") {
                        if (draft.odds == match[0].crs_odds_s0005) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0005) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "1:5") {
                        if (draft.odds == match[0].crs_odds_s0105) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0105) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "2:5") {
                        if (draft.odds == match[0].crs_odds_s0205) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_s0205) {
                            result["oddFail"].push(draft)
                        }
                    } else if (draft.bet_content == "客其他") {
                        if (draft.odds == match[0].crs_odds_sm1ma) {
                            result["oddPass"].push(draft)
                        } else if (draft.odds != match[0].crs_odds_sm1ma) {
                            result["oddFail"].push(draft)
                        }
                    }
                }
            }

            if (result.dateFail.length > 0 || result.oddFail.length > 0) {
                return result
            } else {
                for (let draft of drafts) {
                    await this.knex("bet").insert({
                        end_user_id: draft.end_user_id,
                        match_id: draft.match_id,
                        spent_coin: draft.spent_coin,
                        bet_type: draft.bet_type,
                        bet_type_content: draft.bet_type_content,
                        bet_content: draft.bet_content,
                        odds: draft.odds
                    })

                    await this.knex("draft_bet")
                        .where("id", draft.id)
                        .del()

                    // 要扣user錢 + 加場數!!
                    await this.knex("end_user").where("id", parseInt(user_id))
                        .decrement("coin", draft.spent_coin)
                        .increment("no_of_game", 1)
                        .increment(`no_of_game_${typeMap[draft.bet_type]}`, 1)
                }
                return result
            }


        } catch (e) {
            console.log(e)
            return e
        }
    }
}