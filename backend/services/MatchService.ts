import type Knex from "knex"
import moment from 'moment'
import "moment-timezone"
// import { IBet } from '../seeds/initial-seed'

export class MatchService {
    public constructor(private knex: Knex) {

    }

    public selectMatches = async () => {
        return (await this.knex.select("*")
            .from('match')
            .orderBy("match_date", "desc")
            .where("matchStatus", "Defined")
        )
    }

    public selectMatchesResult = async () => {
        return (await this.knex.select("*")
            .from("match")
            .where("matchStatus", "ResultIn")
            .orderBy("match_date", "desc")
        )
    }

    public insertMatches = async (matchJson: any) => {
        // console.log("service")
        // console.log(matchJson)
        for (let match of matchJson) {
            await this.knex("match")
                .insert({
                    jc_match_id: match[0]["matchID"],
                    matchStatus: match[0]["matchStatus"],

                    home_team: match[0]["homeTeam"]["teamNameCH"],
                    home_team_flag: match[0]["homeTeam"]["teamNameCH"] + ".png",
                    away_team: match[0]["awayTeam"]["teamNameCH"],
                    away_team_flag: match[0]["awayTeam"]["teamNameCH"] + ".png",
                    match_type: match[0]['league']["leagueNameCH"],
                    match_day: match[0]["matchDay"],
                    match_date: match[0]["matchTime"].slice(0, 19).replace("T", " "),
                    // match_date: (match[0]["matchTime"].slice(0,19).replace("T", " ")).format("YYYY-MM-DD hh:mm:ss"),

                    had_h_odds: parseFloat(match[0]["hadodds"]["H"].slice(4)),
                    had_a_odds: parseFloat(match[0]["hadodds"]["A"].slice(4)),
                    had_d_odds: parseFloat(match[0]["hadodds"]["D"].slice(4)),

                    // hdc_hg: (match[0]["hdcodds"] ? match[0]["hdcodds"]["HG"] : null),
                    // hdc_h_odds: (match[0]["hdcodds"] ? parseFloat(match[0]["hdcodds"]["H"].slice(4)) : null),
                    // hdc_ag: (match[0]["hdcodds"] ? match[0]["hdcodds"]["AG"] : null),
                    // hdc_a_odds: (match[0]["hdcodds"] ? parseFloat(match[0]["hdcodds"]["A"].slice(4)) : null),

                    hdc_hg: (match[0]["hdcodds"] ? match[0]["hdcodds"]["HG"] : null),
                    hdc_h_odds: (match[0]["hdcodds"] ? parseFloat(match[0]["hdcodds"]["H"].slice(4)) : null),
                    hdc_ag: (match[0]["hdcodds"] ? match[0]["hdcodds"]["AG"] : null),
                    hdc_a_odds: (match[0]["hdcodds"] ? parseFloat(match[0]["hdcodds"]["A"].slice(4)) : null),

                    crs_odds_s0100: parseFloat(match[0]["crsodds"]["S0100"].slice(4)),
                    crs_odds_s0200: parseFloat(match[0]["crsodds"]["S0200"].slice(4)),
                    crs_odds_s0201: parseFloat(match[0]["crsodds"]["S0201"].slice(4)),
                    crs_odds_s0300: parseFloat(match[0]["crsodds"]["S0300"].slice(4)),
                    crs_odds_s0301: parseFloat(match[0]["crsodds"]["S0301"].slice(4)),
                    crs_odds_s0302: parseFloat(match[0]["crsodds"]["S0302"].slice(4)),
                    crs_odds_s0400: parseFloat(match[0]["crsodds"]["S0400"].slice(4)),
                    crs_odds_s0401: parseFloat(match[0]["crsodds"]["S0401"].slice(4)),
                    crs_odds_s0402: parseFloat(match[0]["crsodds"]["S0402"].slice(4)),
                    crs_odds_s0500: parseFloat(match[0]["crsodds"]["S0500"].slice(4)),
                    crs_odds_s0501: parseFloat(match[0]["crsodds"]["S0501"].slice(4)),
                    crs_odds_s0502: parseFloat(match[0]["crsodds"]["S0502"].slice(4)),
                    crs_odds_sm1mh: parseFloat(match[0]["crsodds"]["SM1MH"].slice(4)),
                    crs_odds_s0000: parseFloat(match[0]["crsodds"]["S0000"].slice(4)),
                    crs_odds_s0101: parseFloat(match[0]["crsodds"]["S0101"].slice(4)),
                    crs_odds_s0202: parseFloat(match[0]["crsodds"]["S0202"].slice(4)),
                    crs_odds_s0303: parseFloat(match[0]["crsodds"]["S0303"].slice(4)),
                    crs_odds_sm1md: parseFloat(match[0]["crsodds"]["SM1MD"].slice(4)),
                    crs_odds_s0001: parseFloat(match[0]["crsodds"]["S0001"].slice(4)),
                    crs_odds_s0002: parseFloat(match[0]["crsodds"]["S0002"].slice(4)),
                    crs_odds_s0102: parseFloat(match[0]["crsodds"]["S0102"].slice(4)),
                    crs_odds_s0003: parseFloat(match[0]["crsodds"]["S0003"].slice(4)),
                    crs_odds_s0103: parseFloat(match[0]["crsodds"]["S0103"].slice(4)),
                    crs_odds_s0203: parseFloat(match[0]["crsodds"]["S0203"].slice(4)),
                    crs_odds_s0004: parseFloat(match[0]["crsodds"]["S0004"].slice(4)),
                    crs_odds_s0104: parseFloat(match[0]["crsodds"]["S0104"].slice(4)),
                    crs_odds_s0204: parseFloat(match[0]["crsodds"]["S0204"].slice(4)),
                    crs_odds_s0005: parseFloat(match[0]["crsodds"]["S0005"].slice(4)),
                    crs_odds_s0105: parseFloat(match[0]["crsodds"]["S0105"].slice(4)),
                    crs_odds_s0205: parseFloat(match[0]["crsodds"]["S0205"].slice(4)),
                    crs_odds_sm1ma: parseFloat(match[0]["crsodds"]["SM1MA"].slice(4)),

                    hil_odds_line: match[0]["hilodds"]["LINELIST"].find((line: any) => line["LINENUM"] == 1)["LINE"],
                    hil_odds_l: parseFloat(match[0]["hilodds"]["LINELIST"].find((line: any) => line["LINENUM"] == 1)["L"].slice(4)),
                    hil_odds_h: parseFloat(match[0]["hilodds"]["LINELIST"].find((line: any) => line["LINENUM"] == 1)["H"].slice(4)),

                    // chl_odds_line: (match[0]["chlodds"] ? match[0]["chlodds"]["LINELIST"][0]["LINE"] : null),
                    // chl_odds_l: (match[0]["chlodds"] ? parseFloat(match[0]["chlodds"]["LINELIST"][0]["L"].slice(4)) : null),
                    // chl_odds_h: (match[0]["chlodds"] ? parseFloat(match[0]["chlodds"]["LINELIST"][0]["H"].slice(4)) : null),
                    chl_odds_line: (match[0]["chlodds"] ? match[0]["chlodds"]["LINELIST"].find((line: any) => line["LINENUM"] == 1)["LINE"] : null),
                    chl_odds_l: (match[0]["chlodds"] ? parseFloat(match[0]["chlodds"]["LINELIST"].find((line: any) => line["LINENUM"] == 1)["L"].slice(4)) : null),
                    chl_odds_h: (match[0]["chlodds"] ? parseFloat(match[0]["chlodds"]["LINELIST"].find((line: any) => line["LINENUM"] == 1)["H"].slice(4)) : null),
                })
                .onConflict("jc_match_id")
                .merge()
        }
        return ({ result: "inserted jockey club odds json into database" })
    }

    public insertResults = async (resultJson: any) => {
        // console.log("service")
        // console.log(matchJson)
        for (let twoStatus of resultJson) {
            for (let status of twoStatus["matches"]) {
                await this.knex("match")
                    .insert({
                        jc_match_id: status["matchID"],
                        matchStatus: status["matchStatus"],
                        home_team: status["homeTeam"]["teamNameCH"],
                        home_team_flag: status["homeTeam"]["teamNameCH"] + ".png",
                        away_team: status["awayTeam"]["teamNameCH"],
                        away_team_flag: status["awayTeam"]["teamNameCH"] + ".png",
                        home_half_match_score: status["accumulatedscore"][0]["home"],
                        home_full_match_score: (status["accumulatedscore"][1] ? status["accumulatedscore"][1]["home"] : null),
                        away_half_match_score: status["accumulatedscore"][0]["away"],
                        away_full_match_score: (status["accumulatedscore"][1] ? status["accumulatedscore"][1]["away"] : null),
                        match_type: status['league']["leagueNameCH"],
                        match_day: status["matchDay"],
                        match_date: moment(status["matchDate"].replace("+", " ")).format("YYYY-MM-DD hh:mm"),
                    })
                    .onConflict("jc_match_id")
                    .merge()
            }
        }
        return ({ result: "inserted jockey club results json into database" })
    }
    public insertBetResult = async (resultJson: any) => {
        let bet_without_result = await this.knex("bet").select('*').where('bet_result_type', null)
        for (let twoStatus of resultJson) {
            for (let status of twoStatus["matches"]) {
                let home_score = parseInt(status["accumulatedscore"][1] ? status["accumulatedscore"][1]["home"] : null)
                let away_score = parseInt(status["accumulatedscore"][1] ? status["accumulatedscore"][1]["away"] : null)

                //bet_type result 
                let matchResult = {}
                if (home_score !== null && away_score !== null) {
                    matchResult['match_id'] = status["matchID"]
                    matchResult['波膽'] = home_score + ":" + away_score
                    matchResult['入球大細'] = home_score + away_score
                    matchResult['角球大細'] = status["cornerresult"] ? parseInt(status["cornerresult"]) : null;
                    matchResult['讓球'] = home_score - away_score;
                    if (home_score > away_score) {
                        matchResult['主客和'] = "主"
                    } else if (home_score === away_score) {
                        matchResult['主客和'] = "和"
                    } else {
                        matchResult['主客和'] = "客"
                    }

                    console.log(matchResult)
                }


                for (let i = 0; i < bet_without_result.length; i++) {
                    if (bet_without_result[i].match_id == matchResult["match_id"]) {
                        let handicap_result = []

                        if (bet_without_result[i].bet_type === "讓球") {
                            let handicap_first_Odd = parseFloat(bet_without_result[i].bet_type_content.split("/")[0])
                            let handicap_second_Odd = parseFloat(bet_without_result[i].bet_type_content.split("/")[1])

                            console.log('first Odd', parseFloat(bet_without_result[i].bet_type_content.split("/")[0]))
                            console.log('second Odd', parseFloat(bet_without_result[i].bet_type_content.split("/")[1]))
                            if ((bet_without_result[i].bet_content == "讓球主勝" && matchResult["讓球"] + handicap_first_Odd > 0) ||
                                (bet_without_result[i].bet_content == "讓球客勝" && matchResult["讓球"] - handicap_first_Odd < 0)
                            ) {
                                handicap_result.push('win')
                            } else if
                                ((bet_without_result[i].bet_content == "讓球主勝" && matchResult["讓球"] + handicap_first_Odd == 0) ||
                                (bet_without_result[i].bet_content == "讓球客勝" && matchResult["讓球"] - handicap_first_Odd == 0)
                            ) {
                                handicap_result.push('draw')
                            } else {
                                handicap_result.push('lose')
                            }

                            if (
                                (bet_without_result[i].bet_content == "讓球主勝" && matchResult["讓球"] + handicap_second_Odd > 0) ||
                                (bet_without_result[i].bet_content == "讓球客勝" && matchResult["讓球"] - handicap_second_Odd < 0)) {
                                handicap_result.push('win')
                            } else if
                                (
                                (bet_without_result[i].bet_content == "讓球主勝" && matchResult["讓球"] + handicap_second_Odd == 0) ||
                                (bet_without_result[i].bet_content == "讓球客勝" && matchResult["讓球"] - handicap_second_Odd == 0)) {
                                handicap_result.push('draw')
                            } else {
                                handicap_result.push('lose')
                            }



                            console.log(handicap_result)
                        }

                        let bet_median: string[] = [];
                        if (bet_without_result[i].bet_type === "角球大細" || bet_without_result[i].bet_type === "入球大細") {
                            bet_median.push(bet_without_result[i].bet_type_content.split('/')[0])
                            // console.log(bet_median)
                            // console.log('float',parseFloat(bet_median[0]))
                        }

                        console.log("match_id", matchResult['match_id'], "userBet", bet_without_result[i].bet_content, "match result", matchResult[bet_without_result[i].bet_type])
                        if ((bet_without_result[i].bet_content == matchResult[bet_without_result[i].bet_type]) || //tackle 主客和，波膽(without 其他)
                            (bet_without_result[i].bet_content == "主其他" && home_score > away_score && home_score + away_score > 7) ||
                            (bet_without_result[i].bet_content == "和其他" && home_score == away_score && home_score + away_score > 6) ||
                            (bet_without_result[i].bet_content == "客其他" && home_score < away_score && home_score + away_score > 7) ||
                            (bet_without_result[i].bet_content == "角球大" && matchResult["角球大細"] > parseFloat(bet_median[0])) ||
                            (bet_without_result[i].bet_content == "角球細" && matchResult["角球大細"] < parseFloat(bet_median[0])) ||
                            (bet_without_result[i].bet_content == "入球大" && matchResult["入球大細"] > parseFloat(bet_median[0])) ||
                            (bet_without_result[i].bet_content == "入球細" && matchResult["入球大細"] < parseFloat(bet_median[0])) ||
                            (handicap_result[0] == "win" && handicap_result[1] == "win")
                        ) {

                            console.log("win")
                            await this.knex("bet")
                                .innerJoin("end_user", "end_user.id", "bet.end_user_id")
                                .update({
                                    bet_result_type: "win",
                                    gain_coin: bet_without_result[i].spent_coin * bet_without_result[i].odds,
                                    // coin: +  bet_without_result[i].spent_coin * bet_without_result[i].odds
                                })
                                .where("bet.id", bet_without_result[i].id)

                            await this.knex("end_user")
                                //.innerJoin("bet", "end_user.id", "bet.end_user_id")
                                .increment("coin", (bet_without_result[i].spent_coin * bet_without_result[i].odds))
                                .where("id", bet_without_result[i].end_user_id)

                        } else if ((handicap_result[0] == "draw" && handicap_result[1] == "win") ||
                            (handicap_result[0] == "lose" && handicap_result[1] == "draw") ||
                            (handicap_result[0] == "win" && handicap_result[1] == "draw") ||
                            (handicap_result[0] == "draw" && handicap_result[1] == "lose")) {
                            console.log("draw")
                            await this.knex("bet")
                                .innerJoin("end_user", "end_user.id", "bet.end_user_id")
                                .update({
                                    bet_result_type: "draw",
                                    gain_coin: bet_without_result[i].spent_coin * 0.5 * bet_without_result[i].odds,
                                    // coin: + bet_without_result[i].spent_coin * 0.5 * bet_without_result[i].odds
                                })
                                .where("bet.id", bet_without_result[i].id)

                            await this.knex("end_user")
                                .increment("coin", (bet_without_result[i].spent_coin * 0.5 * bet_without_result[i].odds))
                                .where("id", bet_without_result[i].end_user_id)


                        } else if (handicap_result[0] == "draw" && handicap_result[1] == "draw") {
                            await this.knex("bet")
                                .innerJoin("end_user", "end_user.id", "bet.end_user_id")
                                .update({
                                    bet_result_type: "draw",
                                    gain_coin: bet_without_result[i].spent_coin,
                                    // coin: + bet_without_result[i].spent_coin
                                })
                                .where("bet.id", bet_without_result[i].id)

                            await this.knex("end_user")
                                .increment("coin", bet_without_result[i].spent_coin)
                                .where("id", bet_without_result[i].end_user_id)

                        } else {
                            console.log("lose")
                            await this.knex("bet")
                                .innerJoin("end_user", "end_user.id", "bet.end_user_id")
                                .update({
                                    bet_result_type: "lose",
                                    // coin: - bet_without_result[i].spent_coin
                                })
                                .where("bet.id", bet_without_result[i].id)

                            await this.knex("end_user")
                                .decrement("coin", bet_without_result[i].spent_coin)
                                .where("id", bet_without_result[i].end_user_id)
                        }


                    }
                }
            }
        }
        return ({ result: "Bet Result has been insert" })
    }
}

// async function matchResult(homeScore: number, awayScore: number) {
//     let result = 'draw'
//     if (home_score > away_score) {
//         let result = 'home'
//         return result
//     } else if (home_score < away_score) {
//         let result = 'away'
//         return result
//     }
//     return result


// }


// function checkResult(home_score: number, away_score: number) {
//     if (home_score == away_score) {
//         const match_result = 'draw';
//         return match_result
//     } else if (home_score > away_score) {
//         const match_result = 'home_win'
//         return match_result
//     } else {
//         const match_result = 'away_win'
//         return match_result
//     }
// }
//     switch (checkResult(home_score,away_score)){
//         case "home_win":
//     }

