import type Knex from "knex"
import bcryptjs from "bcryptjs"
import jwtSimple from "jwt-simple"
import jwt from '../jwt'

export class UserService {
    public constructor(private knex: Knex) {

    }

    public selectUser = async (email: string, password: string) => {
        const users = await this.knex.select('*').from('end_user')
            .where('email', email)
        // console.log(users)
        if (users.length <= 0 || !(await bcryptjs.compare(password, users[0].password))) {
            return { result: '登入電郵或密碼錯誤' }
        }
        return {
            id: users[0].id,
            token: jwtSimple.encode({
                id: users[0].id
            }, jwt.jwtSecret)
        }
    }

    public selectUserByEmail = async (email: string) => {
        return await this.knex.select("*").from("end_user").where("email", email)
    }

    public selectUserByName = async (name: string) => {
        return await this.knex.select("*").from("end_user").where("username", name)
    }

    public createUserFB = async (email: string, name: string) => {
        return await this.knex("end_user").insert({
            email: email,
            username: name
        }).returning(["id", "username"])
    }

    public createUser = async (email: string, username: string, password: string) => {
        return await this.knex("end_user").insert({
            email,
            username,
            password: await bcryptjs.hash(password, 10)
        }).returning(["id", "username"])
    }

    public checkToken = async (payload: any) => {
        // console.log(payload)
        return await this.knex.select('*').from('end_user').where('id', payload.id)
    }


    public selectUserBet = async (id: string) => {
        // console.log(id)
        try {
            const betRecord = await this.knex.select("bet.*", "match.home_team", "match.away_team", "match.home_full_match_score",
                "match.away_full_match_score", "match.match_type", "match.match_day",
                "match.match_date")
                .from("bet")
                .innerJoin("end_user", "bet.end_user_id", "end_user.id")
                .innerJoin("match", "bet.match_id", "match.jc_match_id")
                .where("bet.end_user_id", parseInt(id))
                .orderBy("updated_at", "desc")
            if (betRecord.length <= 0) {
                return { result: "暫無投注紀錄。" }
            }
            return {
                betRecord
            }
        } catch (error) {
            console.log(error)
            return { result: "暫無投注紀錄。"}
        }
    }

    public selectUserBetMatch = async (id: string) => {
        // console.log(id)
        try {
            const betRecord = await this.knex.select("end_user.username","match.home_team", "match.away_team", "match.home_full_match_score",
                "match.away_full_match_score", "match.match_type", "match.match_day",
                "match.match_date","bet.match_id")
                .from("bet")
                .innerJoin("end_user", "bet.end_user_id", "end_user.id")
                .innerJoin("match", "bet.match_id", "match.jc_match_id")
                .where("bet.end_user_id", parseInt(id))
                .andWhere('bet.bet_result_type', null)
            if (betRecord.length <= 0) {
                return { result: "暫無投注紀錄。" }
            }
            return {
                betRecord
            }
        } catch (error) {
            console.log(error)
            return { result: "暫無投注紀錄。"}
        }
    }
    public selectUserFollowee = async (id: string) => {
        console.log(id)
        try {
            const followRecord = await this.knex
                .select("end_user.username", 'end_user.id', 'end_user.coin', 
                        "end_user.no_of_game", "end_user.no_of_win_game",
                        "end_user.no_of_game_HAD", "end_user.no_of_win_game_HAD", 
                        "end_user.no_of_game_HDC", "end_user.no_of_win_game_HDC", 
                        "end_user.no_of_game_CRS", "end_user.no_of_win_game_CRS",
                        "end_user.no_of_game_HIL", "end_user.no_of_win_game_HIL", 
                        "end_user.no_of_game_COR","end_user.no_of_win_game_COR")
                .from("end_user")
                .innerJoin("followership", "end_user.id", "followership.followee_end_user_id")
                // .innerJoin("followership", "end_user.id", "followership.followee_end_user_id")
                .where("followership.follower_end_user_id", parseInt(id))

            // console.log(followRecord)
            if (followRecord.length <= 0) {
                return { result: "暫無跟隨紀錄。" }
            }
            return {
                followRecord
            }
        } catch (error) {
            return { result: "暫無跟隨紀錄。"}
        }
    }

    public selectFollowedBy = async (id: string) => {
        console.log(id)
        try {
            const following = await this.knex("followership").count("followee_end_user_id").where("followee_end_user_id", parseInt(id))
            return following
        } catch (e) {
            return { result: "暫無跟隨紀錄。"}
        }
    }

    // registration and login
    // public registerNewUser = async (username: string, password: string, email: string) => {
    //     let boolean = await this.checkExistingUserEmail(email);

    //     if (!boolean) {
    //         return (await this.knex
    //             .insert({
    //                 name: username,
    //                 password,
    //                 email
    //             })
    //             .into('user')
    //             .returning('id'))
    //     } else {
    //         return false;
    //     }
    // }





}