import Knex from 'knex'

export class RankingService {
    constructor(private knex: Knex) { }

    public getRanking = async () => {
        try {
            let result:{}[] = await this.knex('end_user')
                        .orderByRaw("(end_user.no_of_win_game::decimal) / (end_user.no_of_game::decimal) desc")
                        .andWhereNot('end_user.no_of_win_game', 0)
                        .andWhereNot('end_user.no_of_game', 0)
                        .limit(20)
            return result
        } catch(e) {
            console.log(e)
            return e
        }
    }   
    
    public postFollowee = async (follower_id: number, followee: string) => {
        try { 
            let followee_id = await this.knex("end_user")
                                .select("id")
                                .where("end_user.username",followee)
    
            await this.knex("followership")
            .insert({
                follower_end_user_id: follower_id,
                followee_end_user_id: followee_id[0].id
            })
        return ({result: "Followership has been inserted"})
        
        } catch (e) {
            console.log(e)
            return (e)
        }
    }

    public removeFollowee = async (follower_id: number, followee: string) => {
        try { 
            let followee_id = await this.knex("end_user")
                                .select("id")
                                .where("end_user.username",followee)
    
            await this.knex("followership")
            .where("follower_end_user_id", follower_id)
            .andWhere("followee_end_user_id",followee_id[0].id)
            .del()

        return ({result: "Followership has been deleted"})
        
        } catch (e) {
            console.log(e)
            return (e)
        }
    }
}
