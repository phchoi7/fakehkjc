import express from 'express'
import { Request, Response } from 'express'


const stripe = require("stripe")
    ("sk_test_51IJZHlH76ZNxrR3AvVoDjqgi1mfNFsxbo6wllxI1QyUHqsBhtmZPTYUAAzbyj0ZeWk4UKUxFeWrBBZEo1ksniEbV00aR3a9vEa")

export const stripeRoute = express.Router();


stripeRoute.post('/checkout', async (req: Request, res: Response) => {

    let { amount, id } = req.body
    try {
        const payment = await stripe.paymentIntents.create({
            amount,
            currency: "HKD",
            description: "tips",
            payment_method: id,
            confirm: true
        })
        console.log("Payment", payment)
        res.json({
            message: 'Payment Successful',
            success: true
        })

    } catch (error) {
        console.error('Error:', error)
        res.json({
            message: "Payment Failed",
            success: false
        })
    }

}
)

// rankingRoute.post('/', rankingController.buyTips)

// rankingRoute.get('/paidTips/:userid/:matchid', rankingController.loadPaidTips)