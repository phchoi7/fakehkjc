// caution: using phil's method need to change package.json
const jsonfile = require('jsonfile')
const fetch = require("node-fetch")
const fs = require("fs")
const puppeteer = require('puppeteer')

// scrape jockey club json first (puppeteer code here))
fetchResult ()
// async function scrapeAllMatchesResult() {
//     console.log("start scrapping results")
//     let todayMatchesResults = await fetch("https://bet.hkjc.com/football/getJSON.aspx?jsontype=results.aspx")
//     let todayMatchesResultsJson = await todayMatchesResults.json()
// 
//     // scrape data into in-progress json, so the ready json file will not be affected 
//     let readJson = await jsonfile.readFile("inProgressResults.json", "utf8")
//     readJson = todayMatchesResultsJson 
//     await jsonfile.writeFile("inProgressResults.json", readJson, { spaces: 2 })
//     
//     // amended the ready json file
//     await jsonfile.writeFile("readyResults.json", todayMatchesResultsJson , { spaces: 2 })
//     console.log("done")
// }



async function fetchResult (){
    // await scrapeAllMatchesResult()
    await fetchResultsJson()
    return
}



// insert match for results into database
async function fetchResultsJson() {
    console.log('fetchJCJson.js - fetchResultsJon(), start')
    // let json = await jsonfile.readFile(`readyResults.json`, "utf8")  //real
    let json = await jsonfile.readFile(`readyResults.json`, "utf8")     //for test only
    console.log(json.length)
    let result = await fetch(`http://localhost:8080/posting-results`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    })

    console.log(await result.json())
    console.log('fetchJCJson.js - fetchResultsJon(), end')
    return
}

// async function insertBetResult() {
//     await fetch(`http://localhost:8080/insert-bet-result`)
// }





