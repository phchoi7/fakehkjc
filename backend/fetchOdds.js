// caution: using phil's method need to change package.json
const jsonfile = require('jsonfile')
const fetch = require("node-fetch")
const fs = require("fs")
const puppeteer = require('puppeteer')

fetchOdds ()
async function fetchOdds () {
    // await scrapeAllMatchesOdd()
    await fetchMatchesJson()
    return
}
// scrape jockey club json first (puppeteer code here))
// async function scrapeAllMatchesOdd() {
//     console.log("start scraping")
//     let todayMatches = await fetch("https://bet.hkjc.com/football/getJSON.aspx?jsontype=index.aspx")
//     let todayMatchesJson = await todayMatches.json()
// 
//     let cleanJson = await jsonfile.readFile("inProgressOdds.json", "utf8")
//     cleanJson = []
//     await jsonfile.writeFile("inProgressOdds.json", cleanJson, { spaces: 2 })
// 
//     for (let i = 0; i < todayMatchesJson.length; i++) {
//         // let array = []
//         console.log(i)
//         const browser = await puppeteer.launch({
//             headless: true,
//             args: [
//                 "--no-sandbox",
//                 "--disable-setuid-sandbox"
//             ]
//         });
//         const page = await browser.newPage();
//         await page.setDefaultNavigationTimeout(3000000);
//         let readJson = await jsonfile.readFile("inProgressOdds.json", "utf8")
// 
//         await page.goto('https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_allodds.aspx&matchid=' + todayMatchesJson[i]["matchID"]);
//         await page.waitForSelector("pre")
//         let eachOdd = await page.$eval("body", async () => {
//             let results = JSON.parse(document.querySelector("pre").innerText)
//             return results.filter(result => result.definedPools.length > 0)
//         })
// 
//         // scrape data into in-progress json, so the ready json file will not be affected 
//         readJson.push(eachOdd)
//         await jsonfile.writeFile("inProgressOdds.json", readJson, { spaces: 2 })
// 
//         await page.waitForTimeout(2000)
//         await browser.close()
//     }
// 
//     // amended the ready json file
//     let readJson = await jsonfile.readFile("inProgressOdds.json", "utf8")
//     await jsonfile.writeFile("readyOdds.json", readJson, { spaces: 2 })
// 
//     console.log("done")
// }





// insert match for bet into database
async function fetchMatchesJson() {
    console.log('fetchJCJson.js - fetchMatchJson(), start')
    let json = await jsonfile.readFile(`readyOdds.json`, "utf8")
    console.log(json.length)
    let result = await fetch(`http://localhost:8080/posting-matches`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    })

    console.log(await result.json())
    console.log('fetchJCJson.js - fetchJson(), end')
    return
}











// async function fetchMatch(array: any) {
//     console.log('1216')
//     let result = await fetch('http://127.0.0.1:8000/matches', {
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify(array)
//     })
//     console.log(await result.json())
//     console.log("1224")
// }

// fetchMatch(matches)



// app.post('/matches', async (req, res) => {
//     // console.log(req.body[1][0]["hadodds"]["A"].slice(4))
//     // console.log(parseFloat(req.body[1][0]["hadodds"]["A"].slice(4)))
//     for (let match of req.body) {
//         await knex("match")
//             .insert({
//                 jc_match_id: match[0]["matchID"],
//                 matchStatus: match[0]["matchStatus"],

//                 home_team: match[0]["homeTeam"]["teamNameCH"],
//                 home_team_flag: match[0]["homeTeam"]["teamNameCH"] + ".png",
//                 away_team: match[0]["awayTeam"]["teamNameCH"],
//                 away_team_flag: match[0]["awayTeam"]["teamNameCH"] + ".png",
//                 match_type: match[0]['league']["leagueNameCH"],
//                 match_day: match[0]["matchDay"],
//                 match_date: moment(match[0]["matchDate"].replace("+", " ")).format("YYYY-MM-DD hh:mm"),

//                 had_h_odds: parseFloat(match[0]["hadodds"]["H"].slice(4)),
//                 had_a_odds: parseFloat(match[0]["hadodds"]["A"].slice(4)),
//                 had_d_odds: parseFloat(match[0]["hadodds"]["D"].slice(4)),

//                 hdc_hg: (match[0]["hdcodds"] ? match[0]["hdcodds"]["HG"] : null),
//                 hdc_h_odds: (match[0]["hdcodds"] ? parseInt(match[0]["hdcodds"]["H"].slice(4)) : null),
//                 hdc_ag: (match[0]["hdcodds"] ? match[0]["hdcodds"]["AG"] : null),
//                 hdc_a_odds: (match[0]["hdcodds"] ? parseInt(match[0]["hdcodds"]["A"].slice(4)) : null),

//                 crs_odds_s0100: parseInt(match[0]["crsodds"]["S0100"].slice(4)),
//                 crs_odds_s0200: parseInt(match[0]["crsodds"]["S0200"].slice(4)),
//                 crs_odds_s0201: parseInt(match[0]["crsodds"]["S0201"].slice(4)),
//                 crs_odds_s0300: parseInt(match[0]["crsodds"]["S0300"].slice(4)),
//                 crs_odds_s0301: parseInt(match[0]["crsodds"]["S0301"].slice(4)),
//                 crs_odds_s0302: parseInt(match[0]["crsodds"]["S0302"].slice(4)),
//                 crs_odds_s0400: parseInt(match[0]["crsodds"]["S0400"].slice(4)),
//                 crs_odds_s0401: parseInt(match[0]["crsodds"]["S0401"].slice(4)),
//                 crs_odds_s0402: parseInt(match[0]["crsodds"]["S0402"].slice(4)),
//                 crs_odds_s0500: parseInt(match[0]["crsodds"]["S0500"].slice(4)),
//                 crs_odds_s0501: parseInt(match[0]["crsodds"]["S0501"].slice(4)),
//                 crs_odds_s0502: parseInt(match[0]["crsodds"]["S0502"].slice(4)),
//                 crs_odds_sm1mh: parseInt(match[0]["crsodds"]["SM1MH"].slice(4)),
//                 crs_odds_s0000: parseInt(match[0]["crsodds"]["S0000"].slice(4)),
//                 crs_odds_s0101: parseInt(match[0]["crsodds"]["S0101"].slice(4)),
//                 crs_odds_s0202: parseInt(match[0]["crsodds"]["S0202"].slice(4)),
//                 crs_odds_s0303: parseInt(match[0]["crsodds"]["S0303"].slice(4)),
//                 crs_odds_sm1md: parseInt(match[0]["crsodds"]["SM1MD"].slice(4)),
//                 crs_odds_s0001: parseInt(match[0]["crsodds"]["S0001"].slice(4)),
//                 crs_odds_s0002: parseInt(match[0]["crsodds"]["S0002"].slice(4)),
//                 crs_odds_s0102: parseInt(match[0]["crsodds"]["S0102"].slice(4)),
//                 crs_odds_s0003: parseInt(match[0]["crsodds"]["S0003"].slice(4)),
//                 crs_odds_s0103: parseInt(match[0]["crsodds"]["S0103"].slice(4)),
//                 crs_odds_s0203: parseInt(match[0]["crsodds"]["S0203"].slice(4)),
//                 crs_odds_s0004: parseInt(match[0]["crsodds"]["S0004"].slice(4)),
//                 crs_odds_s0104: parseInt(match[0]["crsodds"]["S0104"].slice(4)),
//                 crs_odds_s0204: parseInt(match[0]["crsodds"]["S0204"].slice(4)),
//                 crs_odds_s0005: parseInt(match[0]["crsodds"]["S0005"].slice(4)),
//                 crs_odds_s0105: parseInt(match[0]["crsodds"]["S0105"].slice(4)),
//                 crs_odds_s0205: parseInt(match[0]["crsodds"]["S0205"].slice(4)),
//                 crs_odds_sm1ma: parseInt(match[0]["crsodds"]["SM1MA"].slice(4)),

//                 hil_odds_line: match[0]["hilodds"]["LINELIST"][0]["LINE"],
//                 hil_odds_l: parseInt(match[0]["hilodds"]["LINELIST"][0]["L"].slice(4)),
//                 hil_odds_h: parseInt(match[0]["hilodds"]["LINELIST"][0]["H"].slice(4)),

//                 chl_odds_line: (match[0]["chlodds"] ? match[0]["chlodds"]["LINELIST"][0]["LINE"] : null),
//                 chl_odds_l: (match[0]["chlodds"] ? parseInt(match[0]["chlodds"]["LINELIST"][0]["L"].slice(4)) : null),
//                 chl_odds_h: (match[0]["chlodds"] ? parseInt(match[0]["chlodds"]["LINELIST"][0]["H"].slice(4)) : null),
//             })
//             .onConflict("jc_match_id")
//             .merge()
//     }
//     res.json({ "result": "inserted match" })
// })
