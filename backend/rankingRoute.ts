import express from 'express'
import { isLoggedIn } from './guard';
import { rankingController } from './main'

export const rankingRoute = express.Router();

rankingRoute.get('/', rankingController.loadRanking)
rankingRoute.post('/followee', isLoggedIn, rankingController.insertFollowee)
rankingRoute.delete('/followee/delete', isLoggedIn, rankingController.deleteFollowee)

// rankingRoute.post('/', rankingController.buyTips)

// rankingRoute.get('/paidTips/:userid/:matchid', rankingController.loadPaidTips)