import express from 'express'
import Knex from 'knex'
import bodyParser from "body-parser"
import cors from 'cors'
import { RankingService } from './services/RankingService'
import { RankingController } from './controllers/RankingController'
import { TipsService } from './services/TipsService'
import { TipsController } from './controllers/TipsController'
import { MatchController } from "./controllers/MatchController"
import { MatchService } from "./services/MatchService"
import moment from 'moment'
// import bcryptjs from "bcryptjs"
// import jwtSimple from "jwt-simple"
// import jwt from './jwt'
import { UserController } from './controllers/UsersController'
import { UserService } from './services/UsersService'
// import { isLoggedIn } from './guard'
import { CommentController } from './controllers/CommentController'
import { CommentService } from './services/CommentService'
import { CartController } from './controllers/CartController'
import { CartService } from './services/CartService'
import { BetController } from './controllers/BetController'
import { BetService } from './services/BetService'

const knexConfig = require('./knexfile.ts')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])

const app = express()

app.use(bodyParser.json({ limit: "50mb" }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use((req, res, next) => {
    console.log('[' + moment().format('yyyy-MM-DD HH:mm:ss') + '] ' + req.method + ' ' + req.path);
    next();
})

const matchService = new MatchService(knex)
const matchController = new MatchController(matchService)

const userService = new UserService(knex)
const userController = new UserController(userService)

const commentService = new CommentService(knex)
const commentController = new CommentController(commentService)

const cartService = new CartService(knex)
const cartController = new CartController(cartService)

const betService = new BetService(knex)
const betController = new BetController(betService)


app.use(cors({
    origin: [process.env.REACT_HOST!]
}))

const rankingService = new RankingService(knex);
export const rankingController = new RankingController(rankingService)

const tipsService = new TipsService(knex);
export const tipsController = new TipsController(tipsService)



import { tipsRoute } from './tipsRoute'
import { rankingRoute } from './rankingRoute'
import { stripeRoute } from './stripeRoute'
import { isLoggedIn } from './guard'

app.use('/tips', tipsRoute)
app.use('/ranking', rankingRoute)
app.use('/', stripeRoute)


// add matches to database from fetchJCJson.js
app.post('/posting-matches', matchController.postMatches)

// add matches results to database from fetchJCJson.js
app.post('/posting-results', matchController.postResults)

//insert bet result according to the match result from DATABASE
// app.post('insert-bet-result', matchController.betResult)

// select all ______要改做for bet page______  matches for match reducer
app.get('/matches',  matchController.getMatches)

// select match result for result page
app.get("/matches-result", matchController.getMatchesResult)


// login
app.post('/login', userController.getUser)

// check localStorage
app.get('/current-user', isLoggedIn, (req, res) => {
    //console.log("main: " + req["user"])
    res.json(req['user'])
})

// comment
app.get('/comments/:matchId', commentController.getComments)
app.post('/comments/:matchId', isLoggedIn, commentController.postComment)


// get user bet
app.get('/bet-records/:userID', isLoggedIn, userController.getUserBet)
app.get('/bet/:userID', userController.getUserBetMatch)

// get user followee
app.get('/followee/:userID', isLoggedIn, userController.getUserFollowee)
app.get('/following/:userID', isLoggedIn, userController.getFollowedBy)

// 購物車
app.post('/bet-area/cart/:userID', isLoggedIn, cartController.postDraft)
app.get('/bet-area/:userID', isLoggedIn, cartController.getDraft)
app.delete("/bet-area/cart/remove", isLoggedIn, cartController.removeDraft)
// app.put('/bet-area/cart/edit/:betID', cartController.putDraft)

// 真正落注
app.get('/validating-odds/:userID', isLoggedIn, betController.validatingOdds)

// fb login
app.post("/login/facebook", userController.loginFacebook)

// register
app.post("/register", userController.register)



const port = 8080
app.listen(port, () => {
    console.log('Listening on port ' + port)
})