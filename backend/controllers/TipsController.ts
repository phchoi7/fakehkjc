import { TipsService } from '../services/TipsService'
import { Request, Response } from 'express'

export class TipsController {
    constructor(private tipsService: TipsService){}

    public loadTipsProviderByMatch = async (req: Request, res: Response) => {
        try {
            console.log ('running controller loadTipsProvider')
            let matchId:string = req.params.matchId
            let result: {}[] = await this.tipsService.getTipsProviderByMatch(matchId)
            res.json(result)
            console.log(result)
        } catch (e) {
            console.log ('running controller loadTipsProvider fail')
            res.status(500).json({ message: "internal server error" })
        }
    }

   
    public loadPaidTips = async (req: Request, res: Response) => {
        try {
            console.log ("runngincontroleler")
            let userId = req.params.userid
            let result = await this.tipsService.getPaidTipsByUser(parseInt(userId))
            res.json(result)
        } catch (e) {
            console.log (e)
            res.status(500).json({ message: "internal server error" })
        } 
    }


    public buyTips = async (req: Request, res: Response) => {
        try {
            let result = await this.tipsService.buyTipsByUser(req.body.customer_id, req.body.tips_provider, req.body.match_id, req.body.paidCoin)
            res.json(result)
        } catch (e) {
            console.log (e)
            res.status(500).json({ message: "internal server error buy tips" })
        }
    }

    public loadNoResultTips = async (req: Request, res: Response) => {
        try {
            let result = await this.tipsService.noResultBetByUser(req.body.id)
            res.json(result)
        } catch (e) {
            console.log (e)
            res.status(500).json({ message: "internal server error buy tips" })
        }
    }
}