import type express from 'express'
import type {CommentService} from '../services/CommentService'


export class CommentController {
  constructor(private CommentService: CommentService) {

  }
  


  public getComments = async (req: express.Request, res: express.Response) => {

    try {
      let matchId:string = req.params.matchId
        let comment = await this.CommentService.selectComments(matchId);
        res.json(comment);
        console.log("the get comments is " + comment)
      } catch (e) {
        console.error(e);
        res.status(500).json({'error': 'internal_server_error'})
      }
}

public postComment = async (req: express.Request, res: express.Response) => {

    try {
        let match_id: string = req.body.matchId
        let end_user_id: string = req.body.userID
        let content:string = req.body.content
        let comment = await this.CommentService.insertComments(end_user_id,match_id,content)

        res.json(comment);
        console.log("the id " + req.body.matchId)
        console.log("the date" + req.body.created_at)
        console.log("the userid" + req.body.userID)
        console.log("the content " + req.body.content)
        console.log("the comment " + comment)
      } catch (e) {
        console.error(e);
        res.status(500).json({'error': 'internal_server_error'})
      }
}


}