import { RankingService } from '../services/RankingService'
import { Request, Response } from 'express'

export class RankingController {
    constructor(private rankingService: RankingService) { }

    public loadRanking = async (req: Request, res: Response) => {
        try {
            let result: {}[] = await this.rankingService.getRanking()
            res.json(result)
        } catch (e) {
            res.status(500).json({ message: "internal server error" })
        }
    }

    public insertFollowee = async (req: Request, res: Response) => {
        try {
            console.log('req.body', req.body)
            let result: {}[] = await this.rankingService.postFollowee(req.body.end_user_id, req.body.followee_username)
            res.json(result)
        } catch (e) {
            console.log(e)
            return e
        }
    }

    public deleteFollowee = async (req: Request, res: Response) => {
        try {
            let result: {}[] = await this.rankingService.removeFollowee(req.body.end_user_id, req.body.followee_username)
            res.json(result)
        } catch (e) {
            console.log(e)
            return e
        }
    }
}