import type express from 'express'
import type { MatchService } from '../services/MatchService'


export class MatchController {
    public constructor(private matchService: MatchService) {

    }

    // select for bet page的比賽 (match status = defined)
    public getMatches = async (req: express.Request, res: express.Response) => {
        let result = await this.matchService.selectMatches()
        res.json(result)
    }

    // select for result page的比賽 (match status = resultIn)
    public getMatchesResult = async (req: express.Request, res: express.Response) => {
        let result = await this.matchService.selectMatchesResult()
        res.json(result)
    }

    // for scrape完odds data後, 入落database到
    public postMatches = async (req: express.Request, res: express.Response) => {
        let result = await this.matchService.insertMatches(req.body)
        res.json(result)
    }
    // public betResult = async (req: express.Request, res: express.Response) => {
    //     await this.matchService.insertMatches(req.body)
    //     res.json({"result":"Bet result has been updated"})
    // }

    // for scrape完results data後, 入落database到
    public postResults = async (req: express.Request, res: express.Response) => {
        let result = await this.matchService.insertResults(req.body)
        await this.matchService.insertBetResult(req.body)
        res.json(result)
    }

    // public getMatchesForBet = async (req: express.Request, res: express.Response) => {
    //     await this.matchService.selectMatches()
    // }
}