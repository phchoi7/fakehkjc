import type express from 'express'
import type { BetService } from '../services/BetService';

export class BetController {
    constructor(private betService: BetService) {

    }

    public validatingOdds = async (req: express.Request, res: express.Response) => {
        // console.log(req.body.email, req.body.password)

        let result = await this.betService.matchingOdds(req.params.userID)
        res.json(result)
        // if (serviceResult.result) {
        //     res.json(serviceResult)
        // } else if (serviceResult.token) {
        //     res.json(serviceResult)
        // }

    }
}
