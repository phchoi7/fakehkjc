import { CartService } from '../services/CartService'
import type express from 'express'

export class CartController {
    constructor(private cartService: CartService) {}

    public postDraft = async (req: express.Request, res: express.Response) => {
        console.log("posting draft bet")
        await this.cartService.insertDraft(req.body)
        res.json({result: 'posted draft bet'})
    }

    public getDraft = async (req: express.Request, res: express.Response) => {
        console.log("getting draft bet")
        const result = await this.cartService.selectDraft(req.params.userID)
        if (result.length > 0) {
            res.json(result)
        } else {
            res.json({result: "未有暫存注項"})
        }
    }

    public removeDraft = async (req: express.Request, res: express.Response) => {
        console.log("removing draft bet")
        await this.cartService.deleteDraftBet(req.body.userId, req.body.betId)
        res.json({result: "removed"})
    }

    public putDraft = async (req: express.Request, res: express.Response) => {
        console.log("editing draft bet")
        await this.cartService.updateDraftBet(req.params.betID, req.body.odds)
        res.json({result: "edited draft bet"})
    }
}