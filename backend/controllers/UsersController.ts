import type express from 'express'
import type { UserService } from '../services/UsersService';
import jwtSimple from "jwt-simple"
import jwt from '../jwt'
import fetch from "cross-fetch"


export class UserController {
    constructor(private userService: UserService) {

    }

    public getUser = async (req: express.Request, res: express.Response) => {
        // console.log(req.body.email, req.body.password)
        let serviceResult = await this.userService.selectUser(req.body.email, req.body.password)
        if (serviceResult.result) {
            res.json(serviceResult)
        } else if (serviceResult.token) {
            res.json(serviceResult)
        }

    }

    public getUserBet = async (req: express.Request, res: express.Response) => {
        // console.log("params " + req.params.userID)
        let bets = await this.userService.selectUserBet(req.params.userID)
        if (bets && bets.result) {
            res.json(bets.result)
        } else if (bets && bets.betRecord) {
            res.json(bets.betRecord!)
        }
    }

    public getUserBetMatch = async (req: express.Request, res: express.Response) => {
        // console.log("params " + req.params.userID)
        let bets = await this.userService.selectUserBetMatch(req.params.userID)
        if (bets && bets.result) {
            res.json(bets.result)
        } else if (bets && bets.betRecord) {
            res.json(bets.betRecord!)
        }
    }

    public getUserFollowee = async (req: express.Request, res: express.Response) => {
        console.log("params " + req.params.userID)
        let followee = await this.userService.selectUserFollowee(req.params.userID)
        if (followee && followee.result) {
            res.json(followee.result)
        } else if (followee && followee.followRecord) {
            res.json(followee.followRecord!)
        }
    }

    public getFollowedBy = async (req: express.Request, res: express.Response) => {
        let length = await this.userService.selectFollowedBy(req.params.userID)
        res.json(length)
    }

    public loginFacebook = async (req: express.Request, res: express.Response) => {
        try {
            console.log("1")
            if (!req.body.accessToken) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }
            console.log("2")
            const { accessToken } = req.body;
            const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
            console.log("3")
            const result = await fetchResponse.json();
            console.log("4")
            console.log(result)
            if (result.error) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }
            
            let user = (await this.userService.selectUserByEmail(result.email))[0];

            console.log("5")
            console.log(user)
            // Create a new user if the user does not exist
            if (!user) {
                user = (await this.userService.createUserFB(result.email, result.name))[0];
            }
            const payload = {
                id: user.id,
                username: user.username
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                id: payload.id,
                token: token
            });
            console.log("7")
        } catch (e) {
            res.status(500).json({ msg: e.toString() })
        }
    }


    public register = async (req: express.Request, res: express.Response) => {
        console.log(req.body)
        let duplicateEmail = (await this.userService.selectUserByEmail(req.body.email))[0];
        let duplicateName = (await this.userService.selectUserByName(req.body.username))[0];
        // console.log(user)

        // console.log(duplicateEmail)
        // console.log(duplicateName)

        if (duplicateEmail && duplicateName) {
            res.json({msg: "電郵已被註冊。"})
        } else if (duplicateEmail) {
            res.json({msg: "電郵已被註冊。"})
        } else if (duplicateName) {
            res.json({msg: "名稱已被註冊。"})
        } else if (req.body.username.length > 12) {
            res.json({msg: "名稱太長。"})
        } else if (req.body.password.length < 8) {
            res.json({msg: "密碼必須有至少有8個字元。"})
        } else if (req.body.password.length > 50) {
            res.json({msg: "密碼太長。"})
        } else if (!duplicateName && !duplicateEmail) {
            res.json(await this.userService.createUser(req.body.email, req.body.username, req.body.password))
        }

    }
}
