import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("draft_bet", table => {
        table.dropColumn("bet_type_content")
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("draft_bet", table => {
        table.string("bet_type_content")
    })

}

