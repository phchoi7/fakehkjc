import * as Knex from "knex";

// remarks 
// 馬會json有matchStatus: 
// "Defined"  應該係未開波
// "ResultIn" 係完左場
// 所以喺match到拎受注賽事時要select "Defined", 
// 拎賽事結果就要select "ResultIn"

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("match", table => {
        table.string("matchStatus")
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("match", table => {
        table.dropColumn("matchStatus")
    })
}

