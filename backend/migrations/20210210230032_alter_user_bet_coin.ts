import * as Knex from "knex";
// 收user同bet table的coin/spent coin 做decimal

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("bet", table => {
        table.decimal("gain_coin", 9, 2)
    })

    await knex.schema.alterTable("end_user", table => {
        table.decimal("coin", 9, 2).defaultTo(50000)
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("end_user", table => {
        table.dropColumn("coin")
    })

    await knex.schema.alterTable("bet", table => {
        table.dropColumn("gain_coin")
    })
}

