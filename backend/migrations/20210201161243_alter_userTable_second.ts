import * as Knex from "knex";

// remarks 
// 紀錄每個user喺每種bet type嘅落注數目, for profile page計勝率用


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("end_user", table => {
        table.integer('coin').defaultTo(20000) // default 20,000
        table.integer('no_of_game').defaultTo(0) // default 0
        table.integer('no_of_win_game').defaultTo(0) // default 0
        table.integer('no_of_game_HAD').defaultTo(0) // default 0
        table.integer('no_of_win_game_HAD').defaultTo(0) // default 0, 主客和
        table.integer('no_of_game_HDC').defaultTo(0) // default 0
        table.integer('no_of_win_game_HDC').defaultTo(0) // default 0, 讓球盤
        table.integer('no_of_game_CRS').defaultTo(0) // default 0
        table.integer('no_of_win_game_CRS').defaultTo(0) // default 0, 波膽
        table.integer('no_of_game_HIL').defaultTo(0) // default 0
        table.integer('no_of_win_game_HIL').defaultTo(0) // default 0, 入球大細
        table.integer('no_of_game_COR').defaultTo(0) // default 0
        table.integer('no_of_win_game_COR').defaultTo(0) // default 0, 角球總數
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("end_user", table => {
        table.dropColumns("no_of_win_game_COR", 'no_of_game_COR', 'no_of_win_game_HIL',
            'no_of_game_HIL', 'no_of_win_game_CRS', 'no_of_game_CRS',
            'no_of_win_game_HDC', 'no_of_game_HDC', 'no_of_win_game_HAD',
            'no_of_game_HAD', 'no_of_win_game', 'no_of_game', "coin")
    })

}

