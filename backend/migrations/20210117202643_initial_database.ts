import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('end_user', table => {
        table.increments();
        table.string('username', 20);
        table.string('password', 60);
        table.string('email', 50);
        table.integer('phone_no', 20);
        table.integer('coin').notNullable() // default 20,000
        table.integer('no_of_game').notNullable() // default 0
        table.integer('no_of_win_game').notNullable() // default 0
        table.integer('no_of_win_game_HAD').notNullable() // default 0, 主客和
        table.integer('no_of_win_game_HDC').notNullable() // default 0, 讓球盤
        table.integer('no_of_win_game_CRS').notNullable() // default 0, 波膽
        table.integer('no_of_win_game_HIL').notNullable() // default 0, 入球大細
        table.integer('no_of_win_game_COR').notNullable() // default 0, 角球總數
        table.timestamps(false, true);
    })

    await knex.schema.createTable("followership", table => {
        table.integer('follower_end_user_id').notNullable() // follow別人的人
        table.foreign("follower_end_user_id").references("end_user.id")
        table.integer('followee_end_user_id').notNullable(); // 被follow的人
        table.foreign("followee_end_user_id").references("end_user.id")
        table.timestamps(false, true);
    })

    await knex.schema.createTable("badge", table => {
        table.increments();
        table.string('title');
        table.string('condition')
    })

    await knex.schema.createTable("badge_end_user", table => {
        table.increments();
        table.integer('end_user_id')
        table.foreign("end_user_id").references("end_user.id")
        table.integer('badge_id');
        table.foreign("badge_id").references("badge.id")
        table.timestamps(false, true);
    })

    /////////////////////////////////////////////  included in table match
    // await knex.schema.createTable("match_type", table => {
    //     table.string('match_type') //比賽性質(e.g. 聯賽/盃賽etc)
    // })

    ///////////////////////////////////////////  included in table match
    // await knex.schema.createTable("football_team", table => {
    //     table.increments();
    //     table.string("team_name")
    //     table.string("flag_path")
    // })

    await knex.schema.createTable("match", table => {
        //////////////////////////////////////// 用馬會matchID做primary key
        // table.increments();
        table.string("jc_match_id").notNullable().primary() // 馬會json的matchID

        // table.integer('home_team').notNullable() 
        // table.foreign("home_team").references("football_team.id")
        table.string("home_team")
        table.string("home_team_flag")
        table.integer("home_half_match_score")
        table.integer('home_full_match_score'); 
        // table.integer('away_team').notNullable()
        // table.foreign("away_team").references("football_team.id") 
        table.string("away_team")
        table.string("away_team_flag")
        table.integer("away_half_match_score")
        table.integer('away_full_match_score'); 
        ///////////////////////////////////
        table.string("match_type").notNullable()
        table.string('match_day').notNullable() //星期幾
        table.dateTime("match_date").notNullable() //日子及時間, 馬會json: 2021-01-18+08:00
        table.decimal("had_h_odds", 9, 2)
        table.decimal("had_a_odds", 9, 2)
        table.decimal("had_d_odds", 9, 2)
        
        table.string("hdc_hg")
        table.decimal("hdc_h_odds", 9, 2)
        table.string("hdc_ag")
        table.decimal("hdc_a_odds", 9, 2)
        table.decimal("crs_odds_s0100", 9, 2)
        table.decimal("crs_odds_s0200", 9, 2)
        table.decimal("crs_odds_s0201", 9, 2)
        table.decimal("crs_odds_s0300", 9, 2)
        table.decimal("crs_odds_s0301", 9, 2)
        table.decimal("crs_odds_s0302", 9, 2)
        table.decimal("crs_odds_s0400", 9, 2)
        table.decimal("crs_odds_s0401", 9, 2)
        table.decimal("crs_odds_s0402", 9, 2)
        table.decimal("crs_odds_s0500", 9, 2)
        table.decimal("crs_odds_s0501", 9, 2)
        table.decimal("crs_odds_s0502", 9, 2)
        table.decimal("crs_odds_sm1mh", 9, 2)

        table.decimal("crs_odds_s0000", 9, 2)
        table.decimal("crs_odds_s0101", 9, 2)
        table.decimal("crs_odds_s0202", 9, 2)
        table.decimal("crs_odds_s0303", 9, 2)
        table.decimal("crs_odds_sm1md", 9, 2)

        table.decimal("crs_odds_s0001", 9, 2)
        table.decimal("crs_odds_s0002", 9, 2)
        table.decimal("crs_odds_s0102", 9, 2)
        table.decimal("crs_odds_s0003", 9, 2)
        table.decimal("crs_odds_s0103", 9, 2)
        table.decimal("crs_odds_s0203", 9, 2)
        table.decimal("crs_odds_s0004", 9, 2)
        table.decimal("crs_odds_s0104", 9, 2)
        table.decimal("crs_odds_s0204", 9, 2)
        table.decimal("crs_odds_s0005", 9, 2)
        table.decimal("crs_odds_s0105", 9, 2)
        table.decimal("crs_odds_s0205", 9, 2)
        table.decimal("crs_odds_sm1ma", 9, 2)
        table.string("hil_odds_line") // 只輸入一個入球大細盤入database
        table.decimal("hil_odds_l", 9, 2)
        table.decimal("hil_odds_h", 9, 2)
        table.string("chl_odds_line") // 只輸入一個角球大細盤入database
        table.decimal("chl_odds_l", 9, 2)
        table.decimal("chl_odds_h", 9, 2)
        table.timestamps(false, true);
    })

    await knex.schema.createTable("forum", table => {
        table.increments();
        table.integer("end_user_id").notNullable()
        table.foreign("end_user_id").references("end_user.id")
        table.string("match_id").notNullable()
        table.foreign("match_id").references("match.jc_match_id")
        table.string("content", 1000).notNullable()
        table.timestamps(false, true);
    })

    await knex.schema.createTable("draft_bet", table => {
        table.increments();
        table.integer("end_user_id").notNullable()
        table.foreign("end_user_id").references("end_user.id")
        table.string("match_id").notNullable()
        table.foreign("match_id").references("match.jc_match_id")
        table.integer("spent_coin").notNullable()
        table.string("bet_type").notNullable() //5大投注種類
        table.string("bet_type_content") //盤口
        table.decimal("odds", 9, 2).notNullable()
        table.timestamps(false, true);
    })

    await knex.schema.createTable("bet", table => {
        table.increments();
        table.integer("end_user_id").notNullable()
        table.foreign("end_user_id").references("end_user.id")
        table.string("match_id").notNullable()
        table.foreign("match_id").references("match.jc_match_id")
        table.integer("spent_coin").notNullable()
        table.integer("gain_coin")
        table.string("bet_type").notNullable() //5大投注種類        = Christian 的 betType
        table.string("bet_type_content") //盤口                     = Christian 的 betContent
        table.string("bet_content") //買咗咩？ 主/客/和/大/細？      = Christian 的 oddType
        table.decimal("odds", 9, 2).notNullable()
        table.string("bet_result_type") // 贏 or 和 or 輸
        table.timestamps(false, true);
    })

    await knex.schema.createTable("payment", table => {
        table.increments();
        table.integer("customer_id").notNullable()
        table.foreign("customer_id").references("end_user.id")
        table.integer("tip_provider_id").notNullable()
        table.foreign("tip_provider_id").references("end_user.id")
        table.string("match_id").notNullable()
        table.foreign("match_id").references("match.jc_match_id")
        table.integer("paid_coin").notNullable() // default $1000
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('payment');
    await knex.schema.dropTable('bet');
    await knex.schema.dropTable('draft_bet');
    await knex.schema.dropTable('forum');
    await knex.schema.dropTable('match');
    // await knex.schema.dropTable('football_team');
    // await knex.schema.dropTable('match_type');
    await knex.schema.dropTable('badge_end_user');
    await knex.schema.dropTable('badge');
    await knex.schema.dropTable('followership');
    await knex.schema.dropTable('end_user');
}

