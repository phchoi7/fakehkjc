import * as Knex from "knex";
// 收user同bet table的coin/spent coin 做decimal

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("end_user", table => {
        table.dropColumn("coin")
    })

    await knex.schema.alterTable("bet", table => {
        table.dropColumn("gain_coin")
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("bet", table => {
        table.integer("gain_coin")
    })

    await knex.schema.alterTable("end_user", table => {
        table.integer("coin").defaultTo(20000)
    })
}

